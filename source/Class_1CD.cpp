//---------------------------------------------------------------------------

#include <vcl.h>
#include <vector>
#include <IOUtils.hpp>
#pragma hdrstop

#include "Class_1CD.h"
#include "Base64.h"
#ifndef getcfname
#include "CRC32.h"
#endif
#include "Common.h"
//---------------------------------------------------------------------------
//#pragma package(smart_init)
//---------------------------------------------------------------------------

#define live_cash 5 // ����� ����� ������������ ������ � �������

//---------------------------------------------------------------------------
char field::buf[2048 + 16];
char field::null_index[4096];
bool field::null_index_initialized = false;

const char SIG_CON[8] = {'1', 'C', 'D', 'B', 'M', 'S', 'V', '8'};
const char SIG_OBJ[8] = {'1', 'C', 'D', 'B', 'O', 'B', 'V', '8'};
const char SIG_MOXCEL[7] = {'M', 'O', 'X', 'C', 'E', 'L', 0};
const char SIG_SKD[8] = {0, 0, 0, 0, 1, 0, 0, 0};
const char SIG_TABDESCR[4] = {'{', 0, '\"', 0};

const unsigned char SIG_ZIP[16] = {0x53,0x4b,0x6f,0xf4,0x88,0x8d,0xc1,0x4e,0xa0,0xd5,0xeb,0xb6,0xbd,0xa0,0xa7,0x0d};

const char NUM_TEST_TEMPLATE[256] = {
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0,
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0,
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0,
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0,
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0,
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0,
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0,
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0,
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0,
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
};

const char DATE1_TEST_TEMPLATE[256] = {
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0,
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0,
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0,
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
};

const char DATE3_TEST_TEMPLATE[256] = {
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0,
	1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
};

const char DATE4_TEST_TEMPLATE[256] = {
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0,
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0,
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0,
	1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
};

const char DATE5_TEST_TEMPLATE[256] = {
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0,
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0,
	1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
};

const char DATE67_TEST_TEMPLATE[256] = {
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0,
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0,
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0,
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0,
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0,
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
};


v8object* v8object::first = NULL;
v8object* v8object::last = NULL;

memblock* memblock::first = NULL;
memblock* memblock::last = NULL;
unsigned int memblock::count = 0;
unsigned int memblock::maxcount = 0x40000; // ��������! ()
//unsigned int memblock::maxcount = 0x10000; // 256 �������� ()
//unsigned int memblock::maxcount = 0x8000; // 128 �������� ()
memblock** memblock::memblocks = NULL;
unsigned int memblock::numblocks = 0;
unsigned int memblock::array_numblocks = 0;
unsigned int memblock::delta = 128;


#ifndef getcfname
#ifndef console
extern TMultiReadExclusiveWriteSynchronizer* tr_syn;
#endif
#endif

bool field::showGUID = true;
bool field::showGUIDasMS = false;

//********************************************************
// �������


//---------------------------------------------------------------------------
tree* __fastcall get_treeFromV8file(v8file* f, MessageRegistrator* err)
{
	TBytesStream* sb;
	TEncoding *enc;
	TBytes bytes;
	int offset;
	tree* rt;

	sb = new TBytesStream(bytes);
	f->SaveToStream(sb);

	enc = NULL;
	offset = TEncoding::GetBufferEncoding(sb->Bytes, enc);
	if(offset == 0)
	{
		if(err) err->AddError("������ ����������� ��������� ����� ����������",
			"����", f->GetFullName());
		delete sb;
		return NULL;
	}
	bytes = TEncoding::Convert(enc, TEncoding::Unicode, sb->Bytes, offset, sb->Size-offset);

	rt = parse_1Ctext(String((wchar_t*)&bytes[0], bytes.Length / 2), err, f->GetFullName());
	delete sb;
	return rt;
}

//---------------------------------------------------------------------------
String __fastcall toXML(String in)
{
	return TStringBuilder(in).Replace(L"&", L"&amp;")->Replace(L"<", L"&lt;")->Replace(L">", L"&gt;")->Replace(L"'", L"&apos;")->Replace(L"\"", "&quot;")->ToString();
}

//---------------------------------------------------------------------------
unsigned char __fastcall from_hex_digit(wchar_t digit)
{
	if(digit >= L'0' && digit <= L'9') return digit - L'0';
	if(digit >= L'a' && digit <= L'f') return digit - L'a' + 10;
	if(digit >= L'A' && digit <= L'F') return digit - L'A' + 10;
	return 0;
}


//********************************************************
// ����� memblock

//---------------------------------------------------------------------------
__fastcall memblock::memblock(TFileStream* fs, unsigned int _numblock, bool for_write, bool read)
{
	numblock = _numblock;
	if(count >= maxcount) delete first; // ���� ���������� ������������ ������ ��������� ������������, ������� ���������, � �������� ���� ���������
	count++;
	prev = last;
	next = NULL;
	if(last) last->next = this;
	else first = this;
	last = this;

	buf = new char[0x1000];
	if(for_write)
	{
		unsigned int fnumblocks = fs->Size >> 12;
		if(fnumblocks <= numblock)
		{
			memset(buf, 0, 0x1000);
			fs->Seek((__int64)numblock << 12, (TSeekOrigin)soFromBeginning);
			fs->WriteBuffer(buf, 0x1000);
			fs->Seek(12i64, (TSeekOrigin)soFromBeginning);
			fs->WriteBuffer(&numblock, 4);
		}
		else
		{
			if(read)
			{
				fs->Seek((__int64)numblock << 12, (TSeekOrigin)soFromBeginning);
				fs->ReadBuffer(buf, 0x1000);
			}
			else memset(buf, 0, 0x1000);
		}
	}
	else
	{
		fs->Seek((__int64)numblock << 12, (TSeekOrigin)soFromBeginning);
		fs->ReadBuffer(buf, 0x1000);
	}

	is_changed = for_write;
	file = fs;

	// ������������ ���� � � ������� ������
	memblocks[numblock] = this;
}

//---------------------------------------------------------------------------
__fastcall memblock::~memblock()
{
	if(is_changed) write();

	// ������� ���� �� �������...
	if(prev) prev->next = next;
	else first = next;
	if(next) next->prev = prev;
	else last = prev;

	// ������� ���� �� ������� ������
	memblocks[numblock] = NULL;

	count--;
	delete[] buf;
}

//---------------------------------------------------------------------------
char* __fastcall memblock::getblock(bool for_write)
{
	lastdataget = GetTickCount();
	// ������� ���� �� �������� ��������� �������...
	if(prev) prev->next = next;
	else first = next;
	if(next) next->prev = prev;
	else last = prev;
	// ... � ���������� ���� � ����� �������
	prev = last;
	next = NULL;
	if(last) last->next = this;
	else first = this;
	last = this;

	if(for_write) is_changed = true;

	return buf;
}

//---------------------------------------------------------------------------
void __fastcall memblock::garbage()
{
	unsigned int curt = GetTickCount();
	while(first)
	{
		if(curt - first->lastdataget > live_cash * 60 * 1000) delete first;
		else break;
	}
}

//---------------------------------------------------------------------------
char* __fastcall memblock::getblock(TFileStream* fs, unsigned int _numblock)
{
	if(_numblock >= numblocks) return NULL;
	if(!memblocks[_numblock]) new memblock(fs, _numblock, false, true);
	return memblocks[_numblock]->getblock(false);
}

//---------------------------------------------------------------------------
char* __fastcall memblock::getblock_for_write(TFileStream* fs, unsigned int _numblock, bool read)
{
	if(_numblock > numblocks) return NULL;
	if(_numblock == numblocks) add_block();
	if(!memblocks[_numblock]) new memblock(fs, _numblock, true, read);
	else memblocks[_numblock]->is_changed = true;
	return memblocks[_numblock]->getblock(true);
}

//---------------------------------------------------------------------------
void __fastcall memblock::create_memblocks(unsigned int _numblocks)
{
	numblocks = _numblocks;
	array_numblocks = (numblocks / delta + 1) * delta;
	memblocks = new memblock*[array_numblocks];
	memset(memblocks, 0, array_numblocks * 4); // 4 = sizeof(memblock*)
}

//---------------------------------------------------------------------------
void __fastcall memblock::delete_memblocks()
{
	while(first) delete first;
	delete[] memblocks;
	numblocks = 0;
	array_numblocks = 0;
}

//---------------------------------------------------------------------------
void __fastcall memblock::add_block()
{
	unsigned int i;
	memblock** mb;

	if(numblocks < array_numblocks) memblocks[numblocks++] = NULL;
	else
	{
		mb = new memblock*[array_numblocks + delta];
		for(i = 0; i < array_numblocks; i++) mb[i] = memblocks[i];
		for(i = array_numblocks; i < array_numblocks + delta; i++) mb[i] = NULL;
		array_numblocks += delta;
		delete[] memblocks;
		memblocks = mb;
	}
}

//---------------------------------------------------------------------------
unsigned int __fastcall memblock::get_numblocks()
{
	return numblocks;
}

//---------------------------------------------------------------------------
void __fastcall memblock::flush()
{
	memblock* cur;
	for(cur = first; cur; cur = cur->next) if(cur->is_changed) cur->write();
}

//---------------------------------------------------------------------------
void __fastcall memblock::write()
{
	if(!is_changed) return;
	file->Seek((__int64)numblock << 12, (TSeekOrigin)soFromBeginning);
	file->WriteBuffer(buf, 0x1000);
	is_changed = false;
}

//---------------------------------------------------------------------------

//***************************************************************************
// ����� v8object

//---------------------------------------------------------------------------
void __fastcall v8object::garbage()
{
	unsigned int curt = GetTickCount();
	v8object* ob = first;

	while(ob)
	{
		if(!ob->lockinmemory) if(ob->data) if(curt - ob->lastdataget > live_cash * 60 * 1000)
		{
			delete[] ob->data;
			ob->data = NULL;
		}
		ob = ob->next;
	}
}

//---------------------------------------------------------------------------
void __fastcall v8object::set_lockinmemory(bool _lock)
{
	lockinmemory = _lock;
	lastdataget = GetTickCount();
}

//---------------------------------------------------------------------------
void __fastcall v8object::init()
{
	len = 0;
	version.version_1 = 0;
	version.version_2 = 0;
	version_rec.version_1 = 0;
	version_rec.version_2 = 0;
	new_version_recorded = false;
	type = 0;
	numblocks = 0;
	real_numblocks = 0;
	blocks = NULL;
	block = -1;
	data = NULL;
	lockinmemory = false;
}

//---------------------------------------------------------------------------
void __fastcall v8object::init(T_1CD* _base, int blockNum)
{
	base = _base;
	err = base->err;
	lockinmemory = false;

	prev = last;
	next = NULL;
	if(last) last->next = this;
	else first = this;
	last = this;

	v8ob* t = new v8ob;
	base->getblock(t, blockNum);
	if(memcmp(&(t->sig), SIG_OBJ, 8) != 0)
	{
		delete t;
		init();
		if(err) err->AddError("������ ��������� ������� �� �����. ���� �� �������� ��������.",
			"����", tohex(blockNum));
		return;
	}

	len = t->len;
	version.version_1 = t->version.version_1;
	version.version_2 = t->version.version_2;
	version_rec.version_1 = version.version_1 + 1;
	version_rec.version_2 = 0;
	new_version_recorded = false;
	type = t->type;
	block = blockNum;
	real_numblocks = 0;
	data = NULL;

	//if(type == 0)
	if(block == 1)
	{
		if(len) numblocks = (len - 1) / 0x400 + 1;
		else numblocks = 0;

		// � ������� ��������� ������ � ������� blocks ����� ���� ������ ������, ��� numblocks
		// numblocks - ���-�� ������ � ��������� �������
		// ���������� real_numblocks - numblocks ����� ����������� �������, �� �� �������� ������
		while(t->blocks[real_numblocks]) real_numblocks++;
		if(real_numblocks)
		{
			blocks = new unsigned int[real_numblocks];
			memcpy(blocks, t->blocks, real_numblocks * 4);
		}
		else blocks = NULL;
	}
	else
	{
//		if(len) numblocks = (((len - 1) / 0x1000 + 1) - 1) / 0x3ff + 1;
		if(len) numblocks = (len - 1) / 0x3ff000 + 1;
		else numblocks = 0;
//		if(numblocks != type) if(err) err->AddError("���������� ������, ����������� �� ����� ������� �� ��������� � ����������� ������, ��������� � ���������",
//			"����� �����", tohex(blockNum),
//			"�����", len,
//			"����������� ���-�� ������", numblocks,
//			"���-�� ������", type);
		if(numblocks)
		{
			blocks = new unsigned int[numblocks];
			memcpy(blocks, t->blocks, numblocks * 4);
		}
		else blocks = NULL;
	}

	delete t;

	#ifdef _DEBUG
	if(err) err->AddDebugMessage("������ ������", msInfo,
		"����� �����", tohex(blockNum),
		"�����", len,
		"������ ������", String(version.version_1) + ":" + version.version_2,
		"������ �����", type);
	#endif

}

//---------------------------------------------------------------------------
__fastcall v8object::v8object(T_1CD* _base, int blockNum)
{
	init(_base, blockNum);
}

__fastcall v8object::v8object(T_1CD* _base)
{
	int blockNum;
	char* b;

	blockNum = _base->get_free_block();
	b = _base->getblock_for_write(blockNum, false);
	memset(b, 0, 0x1000);
	memcpy(((v8ob*)b)->sig, SIG_OBJ, 8);
	init(_base, blockNum);
}


//---------------------------------------------------------------------------
__fastcall v8object::~v8object()
{
	delete[] blocks;
	delete[] data;

	if(prev) prev->next = next;
	else first = next;
	if(next) next->prev = prev;
	else last = prev;
}

//---------------------------------------------------------------------------
char* __fastcall v8object::getdata()
{
	char* tt;
	objtab* b;
	unsigned int i, l;
	unsigned int curlen = 0;

	lastdataget = GetTickCount();
	if(!len) return NULL;
	if(data) return data;

	if(type == 0)
	{
		l = len * 4;
		data = new char[l];
		tt = data;
		i = 0;
		while(l > 0x1000)
		{
			base->getblock(tt, blocks[i++]);
			tt += 0x1000;
			l -= 0x1000;
		}
		base->getblock(tt, blocks[i], l);
	}
	else
	{
		l = len;
		data = new char[l];
		tt = data;
		for(i = 0; i < numblocks; i++)
		{
			b = (objtab*)base->getblock(blocks[i]);
			for(int j = 0; j < b->numblocks; j++)
			{
				curlen = l > 0x1000 ? 0x1000 : l;
				base->getblock(tt, b->blocks[j], curlen);
				if(l <= curlen) break;
				l -= curlen;
				tt += 0x1000;
			}
			if(l <= curlen) break;
		}
	}

	return data;
}

//---------------------------------------------------------------------------
char* __fastcall v8object::getdata(void* buf, unsigned int _start, unsigned int _length)
{
	unsigned int curblock;
	unsigned int curoffblock;
	char* _buf;
	char* _bu;
	unsigned int curlen;

	objtab* b;
	unsigned int curobjblock;
	unsigned int curoffobjblock;

	lastdataget = GetTickCount();

	if(data) memcpy(buf, data + _start, _length);
	else
	{
		if(type == 0)
		{
			if(_start + _length > len * 4)
			{
				if(err) err->AddError("������� ������ ������ �� ��������� �������",
					"����� ����� �������", tohex(block),
					"����� �������", len * 4,
					"������ �������� ������", _start,
					"����� �������� ������", _length);
				return NULL;
			}

			curblock = _start >> 12;
			_buf = (char*)buf;
			curoffblock = _start - (curblock << 12);
			curlen = min(0x1000 - curoffblock, _length);

			while(_length)
			{
				_bu = base->getblock(blocks[curblock++]);
				if(!_bu) return NULL;
				memcpy(_buf, _bu + curoffblock, curlen);
				_buf += curlen;
				_length -= curlen;
				curoffblock = 0;
				curlen = min(0x1000, _length);
			}

		}
		else
		{
			if(_start + _length > len)
			{
				if(err) err->AddError("������� ������ ������ �� ��������� �������",
					"����� ����� �������", tohex(block),
					"����� �������", len,
					"������ �������� ������", _start,
					"����� �������� ������", _length);
				return NULL;
			}

			curblock = _start >> 12;
			_buf = (char*)buf;
			curoffblock = _start - (curblock << 12);
			curlen = min(0x1000 - curoffblock, _length);

			curobjblock = curblock / 1023;
			curoffobjblock = curblock - curobjblock * 1023;

			b = (objtab*) base->getblock(blocks[curobjblock++]);
			if(!b) return NULL;
			while(_length)
			{
				_bu = base->getblock(b->blocks[curoffobjblock++]);
				if(!_bu) return NULL;
				memcpy(_buf, _bu + curoffblock, curlen);
				_buf += curlen;
				_length -= curlen;
				curoffblock = 0;
				curlen = min(0x1000, _length);
				if(_length > 0) if(curoffobjblock >= 1023)
				{
					curoffobjblock = 0;
					b = (objtab*) base->getblock(blocks[curobjblock++]);
				}
			}

		}

	}
	return (char*)buf;
}

//---------------------------------------------------------------------------
unsigned int __fastcall v8object::getlen()
{
	if(type == 0) return len * 4;
	else return len;
}

//---------------------------------------------------------------------------
void __fastcall v8object::savetofile(String _filename)
{
	unsigned int i, j, k;
	TFileStream* fs = new TFileStream(_filename, fmCreate);
	char* buf = new char[0x1000];
	unsigned int l = getlen();
	k = l;
	for(i = 0; i < l; i += 0x1000U)
	{
		j = min(k, 0x1000U);
		getdata(buf, i, j);
		fs->Write(buf, j);
		k -= 0x1000U;
	}
	delete fs;
	delete[] buf;
}

//---------------------------------------------------------------------------
__int64 __fastcall v8object::get_fileoffset(unsigned int offset)
{
	unsigned int _start = offset;
	objtab* b;
	unsigned int curblock;
	unsigned int curoffblock;
	unsigned int curobjblock;
	unsigned int curoffobjblock;

	if(type == 0)
	{
		curblock = _start >> 12;
		curoffblock = _start - (curblock << 12);
		return (((unsigned __int64)(blocks[curblock])) << 12) + curoffblock;
	}
	else
	{

		curblock = _start >> 12;
		curoffblock = _start - (curblock << 12);

		curobjblock = curblock / 1023;
		curoffobjblock = curblock - curobjblock * 1023;

		b = (objtab*) base->getblock(blocks[curobjblock]);

		return (((unsigned __int64)(b->blocks[curoffobjblock])) << 12) + curoffblock;
	}

}

//---------------------------------------------------------------------------
bool __fastcall v8object::setdata(void* buf, unsigned int _start, unsigned int _length)
{
	unsigned int curblock;
	unsigned int curoffblock;
	char* _buf;
	unsigned int curlen;

	objtab* b;
	unsigned int curobjblock;
	unsigned int curoffobjblock;

	if(base->get_readonly())
	{
		if(err) err->AddError(L"������� ������ � ������ � ������ \"������ ������\"",
			L"����� ����� �������", tohex(block));
		return false;
	}

	if(block == 1)
//	if(type == 0)
	{
		if(err) err->AddError(L"������� ������ ������ � ������ ��������� ������",
			L"����� ����� �������", tohex(block));
		return false;
	}

	lastdataget = GetTickCount();

	delete[] data;
	data = NULL;
	if(_start + _length > len) set_len(_start + _length);

	curblock = _start >> 12;
	_buf = (char*)buf;
	curoffblock = _start - (curblock << 12);
	curlen = min(0x1000 - curoffblock, _length);

	curobjblock = curblock / 1023;
	curoffobjblock = curblock - curobjblock * 1023;

	b = (objtab*) base->getblock(blocks[curobjblock++]);
	while(_length)
	{
		memcpy((char*)(base->getblock_for_write(b->blocks[curoffobjblock++], curlen != 0x1000)) + curoffblock, _buf, curlen);
		_buf += curlen;
		_length -= curlen;
		curoffblock = 0;
		curlen = min(0x1000, _length);
		if(_length > 0) if(curoffobjblock >= 1023)
		{
			curoffobjblock = 0;
			b = (objtab*) base->getblock(blocks[curobjblock++]);
		}
	}

	write_new_version();
	return true;
}

//---------------------------------------------------------------------------
bool __fastcall v8object::setdata(void* _buf, unsigned int _length)
{
	char* tt;
	objtab* b;
	unsigned int i, l;
	unsigned int curlen = 0;
	char* buf;

	if(base->get_readonly())
	{
		if(err) err->AddError(L"������� ������ � ������ � ������ \"������ ������\"",
			L"����� ����� �������", tohex(block));
		return false;
	}

	if(block == 1)
//	if(type == 0)
	{
		if(err) err->AddError(L"������� ������ ������ � ������ ��������� ������",
			L"����� ����� �������", tohex(block));
		return false;
	}

	delete[] data;
	data = NULL;
	set_len(_length);

	buf = (char*)_buf;

	l = len;
	for(i = 0; i < numblocks; i++)
	{
		b = (objtab*)base->getblock(blocks[i]);
		for(int j = 0; j < b->numblocks; j++)
		{
			curlen = l > 0x1000 ? 0x1000 : l;
			tt = base->getblock_for_write(b->blocks[j], false);
			memcpy(tt, buf, curlen);
			buf += 0x1000;
			if(l <= curlen) break;
			l -= curlen;
		}
		if(l <= curlen) break;
	}

	write_new_version();
	return true;
}

//---------------------------------------------------------------------------
bool __fastcall v8object::setdata(TStream* stream)
{
	char* tt;
	objtab* b;
	unsigned int i, l;
	unsigned int curlen = 0;
	unsigned int _length;

	if(base->get_readonly())
	{
		if(err) err->AddError(L"������� ������ � ������ � ������ \"������ ������\"",
			L"����� ����� �������", tohex(block));
		return false;
	}

	//if(type == 0)
	if(block == 1)
	{
		if(err) err->AddError(L"������� ������ ������ � ������ ��������� ������",
			L"����� ����� �������", tohex(block));
		return false;
	}

	delete[] data;
	data = NULL;
	_length = stream->Size;
	set_len(_length);

	stream->Seek(0, soFromBeginning);
	l = len;
	for(i = 0; i < numblocks; i++)
	{
		b = (objtab*)base->getblock(blocks[i]);
		for(int j = 0; j < b->numblocks; j++)
		{
			curlen = l > 0x1000 ? 0x1000 : l;
			tt = base->getblock_for_write(b->blocks[j], false);
			stream->ReadBuffer(tt, curlen);
			if(l <= curlen) break;
			l -= curlen;
		}
		if(l <= curlen) break;
	}

	write_new_version();
	return true;
}

//---------------------------------------------------------------------------
bool __fastcall v8object::setdata(TStream* stream, unsigned int _start, unsigned int _length)
{
	unsigned int curblock;
	unsigned int curoffblock;
	unsigned int curlen;

	objtab* b;
	unsigned int curobjblock;
	unsigned int curoffobjblock;

	if(base->get_readonly())
	{
		if(err) err->AddError(L"������� ������ � ������ � ������ \"������ ������\"",
			L"����� ����� �������", tohex(block));
		return false;
	}

	if(block == 1)
//	if(type == 0)
	{
		if(err) err->AddError(L"������� ������ ������ � ������ ��������� ������",
			L"����� ����� �������", tohex(block));
		return false;
	}

	lastdataget = GetTickCount();

	delete[] data;
	data = NULL;
	if(_start + _length > len) set_len(_start + _length);

	curblock = _start >> 12;
	curoffblock = _start - (curblock << 12);
	curlen = min(0x1000 - curoffblock, _length);

	curobjblock = curblock / 1023;
	curoffobjblock = curblock - curobjblock * 1023;

	b = (objtab*) base->getblock(blocks[curobjblock++]);
	while(_length)
	{
		stream->ReadBuffer((char*)(base->getblock_for_write(b->blocks[curoffobjblock++], curlen != 0x1000)) + curoffblock, curlen);
		_length -= curlen;
		curoffblock = 0;
		curlen = min(0x1000, _length);
		if(_length > 0) if(curoffobjblock >= 1023)
		{
			curoffobjblock = 0;
			b = (objtab*) base->getblock(blocks[curobjblock++]);
		}
	}


	write_new_version();
	return true;

}

//---------------------------------------------------------------------------
void __fastcall v8object::set_len(unsigned int _len)
{
	int num_data_blocks;
	unsigned int num_blocks;
//	unsigned int cur_data_blocks;
	int cur_data_blocks;
	unsigned int bl;
	unsigned int i;
	v8ob* b;
	objtab* ot;

	if(len == _len) return;

	if(block == 1)
//	if(!type)
	{
		// ������� ��������� ������
		if(err) err->AddError(L"������� ��������� ����� � ������� ��������� ������");
		return;
	}

	delete[] data;
	data = NULL;

	b = (v8ob*)base->getblock_for_write(block, true);
	b->len = _len;

	num_data_blocks = ((unsigned __int64)_len + 0xfff) >> 12;
	num_blocks = (num_data_blocks + 1022) / 1023;
	cur_data_blocks = ((unsigned __int64)len + 0xfff) >> 12;

	if(numblocks != num_blocks)
	{
		delete[] blocks;
		if(num_blocks) blocks = new unsigned int[num_blocks];
		else blocks = NULL;
	}

	if(_len > len)
	{
		// ���������� ����� �������
		//ot = (objtab*)base->getblock_for_write(b->blocks[numblocks - 1], true);
		if(numblocks) ot = (objtab*)base->getblock_for_write(b->blocks[numblocks - 1], true);
//		for(cur_data_blocks++; cur_data_blocks < num_data_blocks; cur_data_blocks++)
		for(; cur_data_blocks < num_data_blocks; cur_data_blocks++)
		{
			i = cur_data_blocks % 1023;
			if(i == 0)
			{
				bl = base->get_free_block();
				b->blocks[numblocks++] = bl;
				ot = (objtab*)base->getblock_for_write(bl, false);
				ot->numblocks = 0;
			}
			bl = base->get_free_block();
			base->getblock_for_write(bl, false); // �������� ���� ��� ������, �� ������, ���� ���� ����� � ����� �����
			ot->blocks[i] = bl;
			ot->numblocks = i + 1;
		}
	}
	else
	{
		// ���������� ����� �������
		ot = (objtab*)base->getblock_for_write(b->blocks[numblocks - 1], true);
		for(cur_data_blocks--; cur_data_blocks >= num_data_blocks; cur_data_blocks--)
		{
			i = cur_data_blocks % 1023;
			base->set_block_as_free(ot->blocks[i]);
			ot->blocks[i] = 0;
			ot->numblocks = i;
			if(i == 0)
			{
				base->set_block_as_free(b->blocks[--numblocks]);
				b->blocks[numblocks] = 0;
				if(numblocks) ot = (objtab*)base->getblock_for_write(b->blocks[numblocks - 1], true);
			}
		}

	}

	len = _len;
	if(b->type == 0) if(len) b->type = 1;
	memcpy(blocks, b->blocks, numblocks * 4);

	write_new_version();
}

//---------------------------------------------------------------------------
void __fastcall v8object::set_block_as_free(unsigned int block_number)
{
	unsigned int i, j, k;
	unsigned int* b;
	v8ob* ob;

	if(type)
	{
		// ������� ��������� ������
		if(err) err->AddError(L"������� ��������� ���������� ����� � �������, �� ���������� �������� ��������� ������",
			L"���� �������", block);
		return;
	}

	j = len >> 10; // length / 1024
	i = len & 0x3ff; // length % 1024

	ob = (v8ob*)base->getblock_for_write(block, true);

	if(real_numblocks > j)
	{
		len++;
		ob->len = len;
		b = (unsigned int*)base->getblock_for_write(blocks[j], true);
		b[i] = block_number;
		if(numblocks <= j) numblocks = j + 1;
	}
	else
	{
		ob->blocks[real_numblocks] = block_number;
		delete[] blocks;
		real_numblocks++;
		blocks = new unsigned int[real_numblocks];
		memcpy(blocks, ob->blocks, real_numblocks * 4);
	}

}

//---------------------------------------------------------------------------
unsigned int __fastcall v8object::get_free_block()
{
	unsigned int i, j, k;
	unsigned int* b;
	v8ob* ob;

	if(type)
	{
		// ������� ��������� ������
		if(err) err->AddError(L"������� ��������� ���������� ����� � �������, �� ���������� �������� ��������� ������",
			L"���� �������", block);
		return 0;
	}

	if(len)
	{
		len--;
		j = len >> 10; // length / 1024
		i = len & 0x3ff; // length % 1024
		b = (unsigned int*)base->getblock_for_write(blocks[j], true);
		k = b[i];
		b[i] = 0;
		ob = (v8ob*)base->getblock_for_write(block, true);
		ob->len = len;
		return k;
	}
	else
	{
		i = memblock::get_numblocks();
		base->getblock_for_write(i, false);
		return i;
	}

}

//---------------------------------------------------------------------------
void __fastcall v8object::get_version_rec_and_increase(_version* ver)
{
	ver->version_1 = version_rec.version_1;
	ver->version_2 = version_rec.version_2;

	version_rec.version_2++;
}

//---------------------------------------------------------------------------
void __fastcall v8object::get_version(_version* ver)
{
	ver->version_1 = version.version_1;
	ver->version_2 = version.version_2;
}

//---------------------------------------------------------------------------
void __fastcall v8object::write_new_version()
{
	_version new_ver;
	if(new_version_recorded) return;

	new_ver.version_1 = version.version_1 + 1;
	new_ver.version_2 = version.version_2;
	memcpy(base->getblock_for_write(block, true) + 12, &new_ver, 8);
	new_version_recorded = true;
}

//---------------------------------------------------------------------------
v8object* __fastcall v8object::get_first()
{
	return first;
}

//---------------------------------------------------------------------------
v8object* __fastcall v8object::get_last()
{
	return last;
}

//---------------------------------------------------------------------------
v8object* __fastcall v8object::get_next()
{
	return next;
}

//---------------------------------------------------------------------------
unsigned int __fastcall v8object::get_block_number()
{
	return block;
}

//---------------------------------------------------------------------------

//********************************************************
// ����� index

//---------------------------------------------------------------------------
__fastcall index::index(table* _base)
{
	tbase = _base;
	err = tbase->err;

	is_primary = false;
	num_records = 0;
	records = NULL;
	start = 0;
	rootblock = 0;
	length = 0;
	recordsindex_complete = false;
}

//---------------------------------------------------------------------------
__fastcall index::~index()
{
	delete[] records;
}

//---------------------------------------------------------------------------
String __fastcall index::getname()
{
	return name;
}

//---------------------------------------------------------------------------
bool __fastcall index::get_is_primary()
{
	return is_primary;
}

//---------------------------------------------------------------------------
int __fastcall index::get_num_records()
{
	return num_records;
}

//---------------------------------------------------------------------------
index_record* __fastcall index::get_records()
{
	return records;
}

//---------------------------------------------------------------------------
unsigned int __fastcall index::get_numrecords()
{
	if(!start) return 0;
	if(!recordsindex_complete) create_recordsindex();
	return recordsindex.Length;
}

//---------------------------------------------------------------------------
unsigned int __fastcall index::get_numrec(unsigned int num_record)
{
	if(!start) return 0;
	if(!recordsindex_complete) create_recordsindex();
	return recordsindex[num_record];
}

//---------------------------------------------------------------------------
void __fastcall index::create_recordsindex()
{
	char* buf;
	char* rbuf;
	int curlen;
	bool is_leaf;
	unsigned int curblock;
	unsigned int curindex;
	unsigned int mask;
	int rlen;
	int i;
	v8object* file_index;

	if(!start) return;

	String readindex(L"������ ������� ");
	err->Status(readindex);

	buf = new char[0x1000];

	file_index = tbase->file_index;
	file_index->getdata(buf, start, 8);

	rootblock = *(unsigned int*)buf;
	length = *(short int*)(buf + 4);

	curblock = rootblock;

	file_index->getdata(buf, curblock, 0x1000);
	curlen = *(short int*)(buf + 2);
	if(curlen)
	{
		recordsindex.Length = tbase->file_data->getlen() / tbase->recordlen;
		is_leaf = buf[0] & 0x2;
		while(!is_leaf)
		{
			curblock = *(unsigned int*)(buf + 16 + length);
			curblock = 	reverse_byte_order(curblock);
			file_index->getdata(buf, curblock, 0x1000);
			is_leaf = buf[0] & 0x2;
		}

		curindex = 0;
		while(true)
		{
			curlen = *(short int*)(buf + 2);
			curblock = *(unsigned int*)(buf + 8);
			mask = *(unsigned int*)(buf + 14);
			rlen = *(short int*)(buf + 28);
			rbuf = buf + 30;
			for(i = 0; i < curlen; i++)
			{
				recordsindex[curindex++] = *(unsigned int*)rbuf & mask;
				rbuf += rlen;
				if(curindex % 10000 == 0) err->Status(readindex + curindex);
			}
			if(curblock == 0xffffffff) break;
			file_index->getdata(buf, curblock, 0x1000);
		}
		recordsindex.Length = curindex;
	}

	recordsindex_complete = true;
	delete[] buf;
	tbase->log_numrecords = recordsindex.Length;
	err->Status(L"");
}

//---------------------------------------------------------------------------
#ifndef PublicRelease
void __fastcall index::dump_recursive(v8object* file_index, TFileStream* f, int level, unsigned int curblock)
{
	unsigned char bf[3];
	unsigned char b;
	char* buf;
	char* rbuf;
	char* ibuf;
	bool is_leaf;
	unsigned int curlen;
	unsigned int i, j;
	unsigned int lastnumrec;
	AnsiString s;

	unsigned int blockx;
	unsigned int numrecx;

	unsigned int numrecmask;
	unsigned int leftmask;
	unsigned int rightmask;
	unsigned int numrecbits;
	unsigned int leftbits;
	//unsigned int rightbits;
	unsigned int recbytes;
	__int64 indrec;
	char* curindex;
	unsigned int numrec;
	unsigned int left;
	unsigned int right;
	unsigned int previous_right;

	leaf_page_header* lph;
	branch_page_header* bph;

	buf = new char[0x1000];
	lph = (leaf_page_header*)buf;
	bph = (branch_page_header*)buf;
	bf[2] = ' ';
	file_index->getdata(buf, curblock, 0x1000);
	curlen = bph->number_indexes;
	if(curlen)
	{
		is_leaf = bph->flags & indexpage_is_leaf;
		if(is_leaf)
		{
			curindex = new char[length]; //
			memset(curindex, 0, length);

			numrecmask = lph->numrecmask;
			leftmask = lph->leftmask;
			rightmask = lph->rightmask;
			numrecbits = lph->numrecbits;
			leftbits = lph->leftbits;
			//rightbits = lph->rightbits;
			recbytes = lph->recbytes;
			rbuf = buf + 30;
			ibuf = buf + 0x1000;
			previous_right = length;

			s = "=";
			s += level;
			s += " leaf, curblock ";
			s += curblock == 0xffffffff ? -1 : (curblock >> 12);
			s += ", count ";
			s += curlen;
			s += ", free ";
			s += lph->freebytes;
			s += ", rec ";
			s += numrecbits;
			s += ", left ";
			s += leftbits;
			s += ", right ";
			s += lph->rightbits;
			s += ", bytes ";
			s += recbytes;
			s += "\r\n";
			f->Write(s.c_str(), s.Length());

			for(i = 0; i < curlen; i++)
			{
				indrec = *(__int64*)rbuf;
				numrec = indrec & numrecmask;
				indrec >>= numrecbits;
				left =  indrec & leftmask;
				indrec >>= leftbits;
				right =  indrec & rightmask;
				//indrec >>= rightbits;
				rbuf += recbytes;
				j = length - left - right;
				ibuf -= j;
				memcpy(curindex + left, ibuf, j);
				if(right > previous_right) memset(curindex + length - right, 0, right - previous_right);
				previous_right = right;

				s = "  -";
				s += level;
				s += ": ";
				f->Write(s.c_str(), s.Length());
				for(j = 0; j < length; j++)
				{
					b = curindex[j];
					b >>= 4;
					b += '0';
					if(b > '9') b += 'a' - '9' - 1;
					bf[0] = b;
					b = curindex[j];
					b &= 0xf;
					b += '0';
					if(b > '9') b += 'a' - '9' - 1;
					bf[1] = b;
					f->Write(bf, 3);
				}
				s = ": ";
				s += numrec;
				s += "\r\n";
				f->Write(s.c_str(), s.Length());
			}
			delete[] curindex;

		}
		else
		{
			s = "*";
			s += level;
			s += " branch, curblock ";
			s += curblock == 0xffffffff ? -1 : (curblock >> 12);
			s += ", count ";
			s += curlen;
			s += "\r\n";
			f->Write(s.c_str(), s.Length());

			rbuf = buf + 12;
			for(i = 0; i < curlen; i++)
			{
				curindex = rbuf;
				rbuf += length;
				numrecx = reverse_byte_order(*(unsigned int*)rbuf);
				rbuf += 4;
				blockx = reverse_byte_order(*(unsigned int*)rbuf);
				rbuf += 4;

				dump_recursive(file_index, f, level + 1, blockx);

				s = "  +";
				s += level;
				s += ": ";
				f->Write(s.c_str(), s.Length());
				for(j = 0; j < length; j++)
				{
					b = curindex[j];
					b >>= 4;
					b += '0';
					if(b > '9') b += 'a' - '9' - 1;
					bf[0] = b;
					b = curindex[j];
					b &= 0xf;
					b += '0';
					if(b > '9') b += 'a' - '9' - 1;
					bf[1] = b;
					f->Write(bf, 3);
				}
				s = ": ";
				s += numrecx;
				s += "\r\n";
				f->Write(s.c_str(), s.Length());

			}

		}
	}

	delete[] buf;
}
#endif //#ifdef PublicRelease

//---------------------------------------------------------------------------
unsigned int __fastcall index::get_rootblock()
{
	char buf[8];

	if(!start) return 0;

	if(rootblock == 0)
	{
		tbase->file_index->getdata(buf, start, 8);
		rootblock = *(unsigned int*)buf;
		length = *(short int*)(buf + 4);
	}
	return rootblock;
}

//---------------------------------------------------------------------------
unsigned int __fastcall index::get_length()
{
	char buf[8];

	if(!start) return 0;

	if(rootblock == 0)
	{
		tbase->file_index->getdata(buf, start, 8);
		rootblock = *(unsigned int*)buf;
		length = *(short int*)(buf + 4);
	}
	return length;
}

//---------------------------------------------------------------------------
#ifndef PublicRelease
void __fastcall index::dump(String _filename)
{
	TFileStream* f;
	v8object* file_index;
	char buf[8];
	AnsiString s;

	f = new TFileStream(_filename, fmCreate);

	if(!start)
	{
		delete f;
		return;
	}

	file_index = tbase->file_index;
	if(rootblock == 0)
	{
		file_index->getdata(buf, start, 8);
		rootblock = *(unsigned int*)buf;
		length = *(short int*)(buf + 4);
	}

	s = "Index length ";
	s += length;
	s += "\r\n";
	f->Write(s.c_str(), s.Length());

	dump_recursive(file_index, f, 0, rootblock);

	delete f;
}
#endif //#ifdef PublicRelease

//---------------------------------------------------------------------------
char* __fastcall index::unpack_leafpage(unsigned int page_offset, unsigned int& number_indexes)
{
	char* buf;
	char* ret;

	if(!tbase->file_index) return NULL;

	buf = new char[0x1000];
	tbase->file_index->getdata(buf, page_offset, 0x1000);
	ret = unpack_leafpage(buf, number_indexes);
	delete[] buf;
	return ret;
}

//---------------------------------------------------------------------------
char* __fastcall index::unpack_leafpage(char* page, unsigned int& number_indexes)
{
	char* outbuf;
	char* rbuf;
	char* ibuf;
	char* obuf;
	leaf_page_header* header;

	unsigned int numrecmask;
	unsigned int leftmask;
	unsigned int rightmask;
	unsigned int numrecbits;
	unsigned int leftbits;
//	unsigned int rightbits;
	unsigned int recbytes;
	__int64 indrec;

	unsigned int i, j, step;
	unsigned int numrec;
	unsigned int left;
	unsigned int right;

	if(length == 0)
	{
		number_indexes = 0;
		return NULL;
	}

	header = (leaf_page_header*)page;

	if(!(header->flags & indexpage_is_leaf))
	{
		if(err) err->AddError(L"������� ���������� �������� ������� �� ���������� ������.",
			L"�������", tbase->name,
			L"������", name);
		number_indexes = 0;
		return NULL;
	}

	number_indexes = header->number_indexes;
	if(!number_indexes)
	{
//		if(err) err->AddError(L"������� ���������� ��������-����� ������� � ������� ����������� ��������.",
//			L"�������", tbase->name,
//			L"������", name);
		return NULL;
	}

	numrecmask = header->numrecmask;
	leftmask = header->leftmask;
	rightmask = header->rightmask;
	numrecbits = header->numrecbits;
	leftbits = header->leftbits;
//	rightbits = header->rightbits;
	recbytes = header->recbytes;

	step = length + 4;
	outbuf = new char[number_indexes * step];

	rbuf = page + 30;
	ibuf = page + 0x1000;
	obuf = outbuf;

	for(i = 0; i < number_indexes; i++)
	{
		indrec = *(__int64*)rbuf;
		numrec = indrec & numrecmask;
		indrec >>= numrecbits;
		left = indrec & leftmask;
		indrec >>= leftbits;
		right = indrec & rightmask;
		rbuf += recbytes;
		j = length - left - right;
		ibuf -= j;

		*(unsigned int*)obuf = numrec;
		obuf += 4;

		if(left) memcpy(obuf, obuf - step, left);
		if(j) memcpy(obuf + left, ibuf, j);
		obuf += length;
		if(right) memset(obuf - right, 0, right);

	}

	return outbuf;
}

//---------------------------------------------------------------------------
bool __fastcall index::pack_leafpage(char* unpack_index, unsigned int number_indexes, char* page_buf)
{
	unsigned int min_numrec_bits;
	unsigned int min_bits;
	unsigned int max_numrec;

	leaf_page_header* header;
	unsigned int freebytes;
	unsigned int numrecmask;
	unsigned int leftmask;
	unsigned int rightmask;
	unsigned int numrecbits;
	unsigned int leftbits;
	unsigned int rightbits;
	unsigned int recbytes;

	unsigned int i, j;
	unsigned int step;
	unsigned int pack_index_len;
	unsigned int left, right;
	unsigned int numrec;
	char* cur;
	char* curp;
	__int64 indrec;

	for(i = length, rightbits = 0; i; i >>= 1, rightbits++);
	leftbits = rightbits;

	step = length + 4;

	max_numrec = 0;
	cur = unpack_index;
	curp = page_buf + 0x1000;
	pack_index_len = 0;

	_pack_index_record* _pack_index_record_array = new _pack_index_record[number_indexes];
	memset(page_buf + 30, 0, 4066);

	for(i = 0; i < number_indexes; i++)
	{
		numrec = *(unsigned int*)cur;
		cur += 4;

		if(i)
		{
			for(j = 0; cur[j] == cur[j - step] && j < length; j++);
			left = j;
		}
		else left = 0;

		for(j = 1; cur[length - j] == 0 && j <= length; j++);
		right = j - 1;


//		if(left + right >= length)
		if(left + right > length)
		{
			if(left < length || is_primary)
			{
				if(err) err->AddError(L"������ �������� �������� �� ��������-�����. ������ �� ���������� ���� ������� ������������.",
					L"�������", tbase->name,
					L"������", name);

				//delete[] _pack_index_record_array;
				//return false;
			}
			right = length - left;
		}
		j = length - left - right;
		curp -= j;
		pack_index_len += j;

		if(pack_index_len >= 4066)
		{
			delete[] _pack_index_record_array;
			return false;
		}

		memcpy(curp, cur + left, j);

		cur += length;

		if(max_numrec < numrec) max_numrec = numrec;

		_pack_index_record_array[i].numrec = numrec;
		_pack_index_record_array[i].left = left;
		_pack_index_record_array[i].right = right;
	}
	for(min_numrec_bits = 0, i = max_numrec; i; i >>= 1, min_numrec_bits++);

	//if(min_numrec_bits < 11) min_numrec_bits = 11; // 11 - ����������� ��������� ��� 4066 / 2 = 2033 - ������������ ���-�� ������� �� ��������. 2033 ������������ � 11 ���
	min_bits = min_numrec_bits + leftbits + rightbits;
	recbytes = (min_bits + 7) >> 3;

	if(recbytes * number_indexes + pack_index_len > 4066)
	{
		delete[] _pack_index_record_array;
		return false;
	}
	freebytes = 4066 - recbytes * number_indexes - pack_index_len;

	numrecbits = recbytes * 8 - leftbits - rightbits;

	for(i = 0, numrecmask = 0; i < numrecbits; i++, numrecmask = (numrecmask << 1) | 1);
	for(i = 0, leftmask = 0; i < leftbits; i++, leftmask = (leftmask << 1) | 1);
	for(i = 0, rightmask = 0; i < rightbits; i++, rightmask = (rightmask << 1) | 1);

	header = (leaf_page_header*)page_buf;
	header->flags = indexpage_is_leaf;
	header->number_indexes = number_indexes;
	header->freebytes = freebytes;
	header->numrecmask = numrecmask;
	header->leftmask = leftmask;
	header->rightmask = rightmask;
	header->numrecbits = numrecbits;
	header->leftbits = leftbits;
	header->rightbits = rightbits;
	header->recbytes = recbytes;

	for(i = 0, curp = page_buf + 30; i < number_indexes; i++, curp += recbytes)
	{
		indrec = _pack_index_record_array[i].right;
		indrec <<= leftbits;
		indrec |= _pack_index_record_array[i].left;
		indrec <<= numrecbits;
		indrec |= _pack_index_record_array[i].numrec;

		memcpy(curp, &indrec, recbytes);
	}

	delete[] _pack_index_record_array;
	return true;
}

//---------------------------------------------------------------------------
#ifndef PublicRelease
void __fastcall index::calcRecordIndex(const char* rec, char* indexBuf)
{
	int i, j, k;

	j = length;
	for(i = 0; i < num_records; i++)
	{
		k = records[i].field->getSortKey(rec, indexBuf, j);
		indexBuf += k;
		j -= k;
	}
	if(j) memset(indexBuf, 0, j);
}

//---------------------------------------------------------------------------
void __fastcall index::delete_index(const char* rec, const unsigned int phys_numrec)
{
	char* index_buf;
	index_buf = new char[length];
	calcRecordIndex(rec, index_buf);
	delete_index_record(index_buf, phys_numrec);
	delete[] index_buf;
}

//---------------------------------------------------------------------------
void __fastcall index::delete_index_record(const char* index_buf, const unsigned int phys_numrec)
{
	bool is_last_record, page_is_empty; // �������� ��� ������ ����������� �������
	unsigned int new_last_phys_num; // �������� ��� ������ ����������� �������
	char* new_last_index_buf = new char[length]; // �������� ��� ������ ����������� �������
	delete_index_record(index_buf, phys_numrec, rootblock, is_last_record, page_is_empty, new_last_index_buf, new_last_phys_num);
	delete[] new_last_index_buf;
}

//---------------------------------------------------------------------------
void __fastcall index::delete_index_record(const char* index_buf, const unsigned int phys_numrec, unsigned int block, bool& is_last_record, bool& page_is_empty, char* new_last_index_buf, unsigned int& new_last_phys_num)
{
	char* page;
	branch_page_header* bph;
	leaf_page_header* lph;
	branch_page_header* bph2;
	bool _is_last_record, _page_is_empty;
	unsigned int _new_last_phys_num;
	unsigned int number_indexes;
	char* unpack_indexes_buf;
	short int flags;

	char* cur_index;
	int i, j, k, delta;

	page = new char[0x1000];
	tbase->file_index->getdata(page, block, 0x1000);

	is_last_record = false;
	page_is_empty = false;

	if(*page & indexpage_is_leaf)
	{
		// ��������-����
		lph = (leaf_page_header*)page;
		flags = lph->flags;
		unpack_indexes_buf = unpack_leafpage(page, number_indexes);
		cur_index = unpack_indexes_buf;
		delta = length + 4;
		for(i = 0; i < lph->number_indexes; i++, cur_index += delta)
		{
			j = memcmp(index_buf, cur_index + 4, length);
			if(j == 0 && *(unsigned int*)cur_index == phys_numrec)
			{
				if(i == lph->number_indexes - 1) is_last_record = true;

				lph->number_indexes--;
				for(k = i; k < lph->number_indexes; k++) memcpy(unpack_indexes_buf + k * delta, unpack_indexes_buf + (k + 1) * delta, delta);

				if(lph->number_indexes == 0)
				{
					page_is_empty = true;
					if(lph->prev_page != 0xffffffff)
					{
						tbase->file_index->setdata(&(lph->next_page), lph->prev_page + 8, 4);
					}
					if(lph->next_page != 0xffffffff)
					{
						tbase->file_index->setdata(&(lph->prev_page), lph->next_page + 4, 4);
					}
					tbase->file_index->getdata(&k, 0, 4);
					memset(page, 0, 0x1000);
					*(unsigned int*)page = k;
					k = block >> 12;
					tbase->file_index->setdata(&k, 0, 4);
				}
				else
				{
					if(is_last_record)
					{
						cur_index = unpack_indexes_buf + (lph->number_indexes - 1) * delta;
						memcpy(new_last_index_buf, cur_index + 4, length);
						new_last_phys_num = *(unsigned int*)cur_index;
					}
					pack_leafpage(unpack_indexes_buf, lph->number_indexes, page);
					lph->flags = flags;
				}
				tbase->file_index->setdata(page, block, 0x1000);

				break;
			}
		}
		delete[] unpack_indexes_buf;
	}
	else
	{
		// ��������-�����
		bph = (branch_page_header*)page;

		cur_index = page + 12; // 12 = size_of(branch_page_header)
		delta = length + 8;
		for(i = 0; i < bph->number_indexes; i++, cur_index += delta)
		{
			j = memcmp(index_buf, cur_index, length);
			if(j <= 0)
			{
				if(i == bph->number_indexes - 1 && j==0) is_last_record = true;

				delete_index_record(index_buf, phys_numrec, reverse_byte_order(*(unsigned int*)(cur_index + length + 4)), _is_last_record, _page_is_empty, new_last_index_buf, _new_last_phys_num);

				if(_page_is_empty)
				{
					bph->number_indexes--;
					//for(k = i; k < bph->number_indexes; k++) memcpy(page + 12 + k * delta, page + 12 + (k + 1) * delta, delta);
					if(bph->number_indexes > i) memcpy(cur_index, cur_index + delta, (bph->number_indexes - i) * delta);
					memset(page + 12 + bph->number_indexes * delta, 0, delta);
				}
				else if(_is_last_record)
				{
					memcpy(cur_index, new_last_index_buf, length);
					*(unsigned int*)(cur_index + length) = reverse_byte_order(_new_last_phys_num);
				}

				if(bph->number_indexes == 0)
				{
					page_is_empty = true;
					if(bph->prev_page != 0xffffffff)
					{
						tbase->file_index->setdata(&(bph->next_page), bph->prev_page + 8, 4);
					}
					if(bph->next_page != 0xffffffff)
					{
						tbase->file_index->setdata(&(bph->prev_page), bph->next_page + 4, 4);
					}
					tbase->file_index->getdata(&k, 0, 4);
					memset(page, 0, 0x1000);
					*(unsigned int*)page = k;
					k = block >> 12;
					tbase->file_index->setdata(&k, 0, 4);
				}
				else
				{
					if(is_last_record)
					{
						cur_index = page + 12 + (bph->number_indexes - 1) * delta;
						memcpy(new_last_index_buf, cur_index, length);
						new_last_phys_num = reverse_byte_order(*(unsigned int*)(cur_index + length));
					}
				}
				if(_page_is_empty || _is_last_record || page_is_empty || is_last_record) tbase->file_index->setdata(page, block, 0x1000);

				break;
			}
		}
	}

	delete[] page;
}

//---------------------------------------------------------------------------
void __fastcall index::write_index(const unsigned int phys_numrecord, const char* rec)
{
	char* index_buf;
	index_buf = new char[length];
	calcRecordIndex(rec, index_buf);
	write_index_record(phys_numrecord, index_buf);
	delete[] index_buf;

}

//---------------------------------------------------------------------------
void __fastcall index::write_index_record(const unsigned int phys_numrecord, const char* index_buf)
{
	int result;
	char* new_last_index_buf = new char[length];
	unsigned int new_last_phys_num;
	char* new_last_index_buf2 = new char[length];
	unsigned int new_last_phys_num2;
	unsigned int new_last_block2;

	char* page;
	branch_page_header* bph;
	unsigned int block;
	unsigned int k;
	char* cur_index;

	write_index_record(phys_numrecord, index_buf, rootblock, result, new_last_index_buf, new_last_phys_num, new_last_index_buf2, new_last_phys_num2, new_last_block2);

	if(result == 2)
	{
		page = new char[0x1000];
		memset(page, 0, 0x1000);
		bph = (branch_page_header*)page;

		tbase->file_index->getdata(&k, 0, 4);
		if(k)
		{
			block = k << 12;
			tbase->file_index->getdata(&k, block, 4);
			tbase->file_index->setdata(&k, 0, 4);
		}
		else block = tbase->file_index->getlen();

		bph->flags = indexpage_is_root | indexpage_is_leaf;
		bph->number_indexes = 2;
		bph->prev_page = 0xffffffff;
		bph->next_page = 0xffffffff;

		cur_index = page + 12;
		memcpy(cur_index, new_last_index_buf, length);
		cur_index += length;
		*(unsigned int*)cur_index = reverse_byte_order(new_last_phys_num);
		cur_index += 4;
		*(unsigned int*)cur_index = reverse_byte_order(rootblock);
		cur_index += 4;

		memcpy(cur_index, new_last_index_buf2, length);
		cur_index += length;
		*(unsigned int*)cur_index = reverse_byte_order(new_last_phys_num2);
		cur_index += 4;
		*(unsigned int*)cur_index = reverse_byte_order(new_last_block2);
		//cur_index += 4;

		tbase->file_index->setdata(page, block, 0x1000);

		rootblock = block;
		tbase->file_index->setdata(&rootblock, start, 4);

		delete[] page;
	}

	delete[] new_last_index_buf;
	delete[] new_last_index_buf2;
}

//---------------------------------------------------------------------------
void __fastcall index::write_index_record(const unsigned int phys_numrecord, const char* index_buf, unsigned int block, int& result, char* new_last_index_buf, unsigned int& new_last_phys_num, char* new_last_index_buf2, unsigned int& new_last_phys_num2, unsigned int& new_last_block2)
{
	// result - ��������� ����������.
	// 0 - ������ ������ �� ����.
	// 1 - ���� �������� ������ �� new_last_index_buf.
	// 2 - ��������� ��������� �� 2 ��������, ���� �������� �� 2 ������

	char* page;
	branch_page_header* bph;
	leaf_page_header* lph;

	int _result;
	unsigned int _new_last_phys_num;
	unsigned int _new_last_phys_num2;
	unsigned int _new_last_block2;

	char* unpack_indexes_buf;
	char* unpack_indexes_buf_new;

	short int flags;
	unsigned int number_indexes;
	unsigned int prev_page;
	unsigned int next_page;

	char* page2;
	branch_page_header* bph2;
	leaf_page_header* lph2;
	unsigned int number_indexes1;
	unsigned int number_indexes2;
	unsigned int block1;
	unsigned int max_num_indexes;

	char* cur_index;
	char* cur_index2;
	unsigned int i, k;
	int j, delta;
	bool ok;

	page = new char[0x1000];
	tbase->file_index->getdata(page, block, 0x1000);
	result = 0;

	bph = (branch_page_header*)page;
	flags = bph->flags;
	number_indexes = bph->number_indexes;
	prev_page = bph->prev_page;
	next_page = bph->next_page;

	if(flags & indexpage_is_leaf)
	{
		// ��������-����
		lph = (leaf_page_header*)page;
		unpack_indexes_buf = unpack_leafpage(page, number_indexes);
		cur_index = unpack_indexes_buf;
		delta = length + 4;
		ok = true;
		for(i = 0; i < number_indexes; i++, cur_index += delta)
		{
			j = memcmp(index_buf, cur_index + 4, length);


			if(j == 0)
			{
				if(is_primary || *(unsigned int*)cur_index == phys_numrecord)
				{
					if(err) err->AddError(L"������ ������ �������. ������ ��� ����������.",
						L"�������", tbase->name,
						L"������", name,
						L"���������� ����� ������������", *(unsigned int*)cur_index,
						L"���������� ����� ������������", phys_numrecord);
					ok = false;
				}
				break;
			}
			if(j < 0) break;
		}
		if(ok)
		{
			if(i == number_indexes) result = 1;

			number_indexes++;
			unpack_indexes_buf_new = new char[delta * number_indexes];

			if(i) memcpy(unpack_indexes_buf_new, unpack_indexes_buf, delta * i);
			*(unsigned int*)(unpack_indexes_buf_new + i * delta) = phys_numrecord;
			memcpy(unpack_indexes_buf_new + i * delta + 4, index_buf, length);
			if(number_indexes - i - 1) memcpy(unpack_indexes_buf_new + (i + 1) * delta, unpack_indexes_buf + i * delta, delta * (number_indexes - i - 1));

			if(pack_leafpage(unpack_indexes_buf_new, number_indexes, page))
			{
				if(result == 1)
				{
					cur_index = unpack_indexes_buf_new + (number_indexes - 1) * delta;
					memcpy(new_last_index_buf, cur_index + 4, length);
					new_last_phys_num = *(unsigned int*)cur_index;
				}
				lph->flags = flags;
				lph->prev_page = prev_page;
				lph->next_page = next_page;
			}
			else
			{
				result = 2;
				page2 = new char[0x1000];
				number_indexes1 = number_indexes >> 1;
				number_indexes2 = number_indexes - number_indexes1;
				pack_leafpage(unpack_indexes_buf_new, number_indexes1, page);
				pack_leafpage(unpack_indexes_buf_new + number_indexes1 * delta, number_indexes2, page2);
				lph2 = (leaf_page_header*)page2;

				tbase->file_index->getdata(&k, 0, 4);
				if(k)
				{
					new_last_block2 = k << 12;
					tbase->file_index->getdata(&k, new_last_block2, 4);
					tbase->file_index->setdata(&k, 0, 4);
				}
				else new_last_block2 = tbase->file_index->getlen();

				flags &= ~indexpage_is_root;
				lph->flags = flags;
				lph->prev_page = prev_page;
				lph->next_page = new_last_block2;
				lph2->flags = flags;
				lph2->prev_page = block;
				lph2->next_page = next_page;

				tbase->file_index->setdata(page2, new_last_block2, 0x1000);

				cur_index = unpack_indexes_buf_new + (number_indexes1 - 1) * delta;
				memcpy(new_last_index_buf, cur_index + 4, length);
				new_last_phys_num = *(unsigned int*)cur_index;
				cur_index = unpack_indexes_buf_new + (number_indexes - 1) * delta;
				memcpy(new_last_index_buf2, cur_index + 4, length);
				new_last_phys_num2 = *(unsigned int*)cur_index;

				delete[] page2;
			}

			tbase->file_index->setdata(page, block, 0x1000);
			delete[] unpack_indexes_buf_new;
		}


		delete[] unpack_indexes_buf;
	}
	else
	{
		// ��������-�����

		cur_index = page + 12; // 12 = size_of(branch_page_header)
		delta = length + 8;
		max_num_indexes = (0x1000 - 12) / delta;

		ok = true;
		for(i = 0; i < number_indexes; i++, cur_index += delta)
		{
			j = memcmp(index_buf, cur_index, length);
			if(j == 0)
			{
				if(is_primary)
				{
					if(err) err->AddError(L"������ ������ �������. ������ ��� ����������.",
						L"�������", tbase->name,
						L"������", name,
						L"���������� ����� ������������", reverse_byte_order(*(unsigned int*)(cur_index + length)),
						L"���������� ����� ������������", phys_numrecord);
					ok = false;
                }
				break;
			}
			if(j < 0) break;
		}

		if(i == number_indexes)
		{
			i--;
			cur_index -= delta;
        }

		if(ok)
		{
			block1 = reverse_byte_order(*(unsigned int*)(cur_index + length + 4));
			write_index_record(phys_numrecord, index_buf, block1, _result, new_last_index_buf, _new_last_phys_num, new_last_index_buf2, _new_last_phys_num2, _new_last_block2);

			if(_result == 1)
			{
				memcpy(cur_index, new_last_index_buf, length);
				*(unsigned int*)(cur_index + length) = reverse_byte_order(_new_last_phys_num);
				if(i == number_indexes - 1)
				{
					result = 1;
					new_last_phys_num = _new_last_phys_num;
					// new_last_index_buf �������!
				}
			}
			else if(_result == 2)
			{
				number_indexes++;
				if(number_indexes > max_num_indexes)
				{
					result = 2;

					tbase->file_index->getdata(&k, 0, 4);
					if(k)
					{
						new_last_block2 = k << 12;
						tbase->file_index->getdata(&k, new_last_block2, 4);
						tbase->file_index->setdata(&k, 0, 4);
					}
					else new_last_block2 = tbase->file_index->getlen();

					flags &= ~indexpage_is_root;

					unpack_indexes_buf = new char[delta * number_indexes];

					cur_index = unpack_indexes_buf;
					cur_index2 = page + 12;
					k = i * delta;
					if(k) memcpy(cur_index, cur_index2, k);
					cur_index += k;
					cur_index2 += k + delta;

					memcpy(cur_index, new_last_index_buf, length);
					cur_index += length;
					*(unsigned int*)cur_index = reverse_byte_order(_new_last_phys_num);
					cur_index += 4;
					*(unsigned int*)cur_index = reverse_byte_order(block1);
					cur_index += 4;

					memcpy(cur_index, new_last_index_buf2, length);
					cur_index += length;
					*(unsigned int*)cur_index = reverse_byte_order(_new_last_phys_num2);
					cur_index += 4;
					*(unsigned int*)cur_index = reverse_byte_order(_new_last_block2);
					cur_index += 4;

					k = (number_indexes - i - 2) * delta;
					if(k) memcpy(cur_index, cur_index2, k);

					number_indexes1 = number_indexes >> 1;
					number_indexes2 = number_indexes - number_indexes1;

					page2 = new char[0x1000];
					bph2 = (branch_page_header*)page2;
					memset(page, 0, 0x1000);
					memset(page2, 0, 0x1000);

					bph->flags = flags;
					bph->number_indexes = number_indexes1;
					bph->prev_page = prev_page;
					bph->next_page = new_last_block2;
					bph2->flags = flags;
					bph2->number_indexes = number_indexes2;
					bph2->prev_page = block;
					bph2->next_page = next_page;

					memcpy(page + 12, unpack_indexes_buf, number_indexes1 * delta);
					memcpy(page2 + 12, unpack_indexes_buf + number_indexes1 * delta, number_indexes2 * delta);

					tbase->file_index->setdata(page2, new_last_block2, 0x1000);

					cur_index = unpack_indexes_buf + (number_indexes1 - 1) * delta;
					memcpy(new_last_index_buf, cur_index, length);
					cur_index += length;
					new_last_phys_num = reverse_byte_order(*(unsigned int*)cur_index);
					cur_index = unpack_indexes_buf + (number_indexes - 1) * delta;
					memcpy(new_last_index_buf2, cur_index + 4, length);
					cur_index += length;
					new_last_phys_num2 = reverse_byte_order(*(unsigned int*)cur_index);

					delete[] unpack_indexes_buf;
					delete[] page2;
				}
				else
				{

					k = (number_indexes - i - 2) * delta;
					if(k)
					{
						cur_index = page + 12 + ((i + 1) * delta);
						cur_index2 = page + 12 + ((i + 2) * delta);
						memcpy(cur_index, cur_index2, k);
					}

					cur_index = page + 12 + (i * delta);
					memcpy(cur_index, new_last_index_buf, length);
					cur_index += length;
					*(unsigned int*)cur_index = reverse_byte_order(_new_last_phys_num);
					cur_index += 4;
					*(unsigned int*)cur_index = reverse_byte_order(block1);
					cur_index += 4;

					memcpy(cur_index, new_last_index_buf2, length);
					cur_index += length;
					*(unsigned int*)cur_index = reverse_byte_order(_new_last_phys_num2);
					cur_index += 4;
					*(unsigned int*)cur_index = reverse_byte_order(_new_last_block2);

					if(i == number_indexes - 2)
					{
						result = 1;
						new_last_phys_num = _new_last_phys_num2;
						memcpy(new_last_index_buf, new_last_index_buf2, length);
					}

					delete[] unpack_indexes_buf;
					delete[] page2;
				}
			}

			if(_result) tbase->file_index->setdata(page, block, 0x1000);
		}

	}

	delete[] page;

}
#endif //#ifdef PublicRelease

//---------------------------------------------------------------------------

//********************************************************
// ����� field

//---------------------------------------------------------------------------
__fastcall field::field(table* _parent)
{
	if(!null_index_initialized)
	{
		*null_index = 1;
		memset(null_index + 1, 0, 4095);
		null_index_initialized = true;
	}
	null_exists = false;
	length = 0;
	precision = 0;
	case_sensitive = false;

	parent = _parent;
	len = 0;
	offset = 0;

	err = parent->err;
}

//---------------------------------------------------------------------------
String __fastcall field::getname()
{
	return name;
}

//---------------------------------------------------------------------------
int __fastcall field::getlen() // ���������� ����� ���� � ������
{
	if(len) return len;

	len = null_exists ? 1 : 0;
	switch(type)
	{
		case tf_binary: len += length; break;
		case tf_bool: len += 1; break;
		case tf_numeric: len += (length + 2) >> 1; break;
		case tf_char: len += length * 2; break;
		case tf_varchar: len += length * 2 + 2; break;
		case tf_version: len += 16; break;
		case tf_string: len += 8; break;
		case tf_text: len += 8; break;
		case tf_image: len += 8; break;
		case tf_datetime: len += 7; break;
		case tf_version8: len += 8; break;
		case tf_varbinary: len += length + 2; break;
	}
	return len;
}

//---------------------------------------------------------------------------
// ��� ignore_showGUID binary16 ������ ������������� � GUID
String __fastcall field::get_presentation(const char* rec, bool EmptyNull, wchar_t Delimiter, bool ignore_showGUID)
{
	char sym;
	int i, j, m;
	bool k;

	unsigned char* fr = (unsigned char*)rec + offset;

	if(null_exists)
	{
		if(fr[0] == 0) return EmptyNull ? L"" : L"{NULL}";
		fr++;
	}
	switch(type)
	{
		case tf_binary:
			if(length == 16 && (showGUID || ignore_showGUID))
			{
				if(showGUIDasMS) return GUIDasMS(fr);
				else return GUIDas1C(fr);
			}
			else
			{
				for(i = 0; i < length; i++)
				{
					sym = '0' + (fr[i] >> 4);
					if(sym > '9') sym += ('a' - '9' - 1);
					buf[i << 1] = sym;
					sym = '0' + (fr[i] & 0xf);
					if(sym > '9') sym += ('a' - '9' - 1);
					buf[(i << 1) + 1] = sym;
				}
				buf[length << 1] = 0;
			}
			return buf;
		case tf_bool:
			if(fr[0]) return L"true";
			return L"false";
		case tf_numeric:
			i = 0; // ������� ������ � buf
			k = true; // �������, ��� �������� ����� ��� �� ��������
			m = length - precision; // ������� ���������� ����� �����
			if(fr[0] >> 4 == 0) buf[i++] = '-';
			for(j = 0; j < length; j++)
			{
				if(Delimiter) if(!k) if(m - j > 0) if((m - j) % 3 == 0) buf[i++] = Delimiter;
				if(j == m)
				{
					buf[i++] = '.';
					k = false;
				}
				if(j & 1) sym = fr[(j + 1) >> 1] >> 4;
				else sym = fr[j >> 1] & 0xf;
				if(sym == 0 && k) continue;
				k = false;
				buf[i++] = '0' + sym;
			}

			//if(k) return "0";
			if(k) buf[i++] = '0';

			buf[i] = 0;
			return buf;
		case tf_char:
			return String((wchar_t*)fr, length);
		case tf_varchar:
			i = *(short int*)fr;
			return String((wchar_t*)(fr + 2), i);
		case tf_version:
			return String(*(int*)fr) + ":" + *(int*)(fr + 4) + ":" + *(int*)(fr + 8) + ":" + *(int*)(fr + 12);
		case tf_version8:
			return String(*(int*)fr) + ":" + *(int*)(fr + 4);
		case tf_string:
			return L"{MEMO}";
		case tf_text:
			return L"{TEXT}";
		case tf_image:
			return L"{IMAGE}";
		case tf_datetime:
			return date_to_string(fr);
		case tf_varbinary:
			m = *(short int*)fr; // ����� + ��������
			for(i = 0; i < m; i++)
			{
				sym = '0' + (fr[i + 2] >> 4);
				if(sym > '9') sym += ('a' - '9' - 1);
				buf[i << 1] = sym;
				sym = '0' + (fr[i + 2] & 0xf);
				if(sym > '9') sym += ('a' - '9' - 1);
				buf[(i << 1) + 1] = sym;
			}
			buf[m << 1] = 0;
			return buf;
	}

	return L"{?}";
}

//---------------------------------------------------------------------------
bool __fastcall field::get_bynary_value(char* binary_value, bool null, String& value)
{
	wchar_t sym;
	int i, j, m, l, p, q;
	bool k, n;
	unsigned char* b;

	unsigned char* fr = (unsigned char*)binary_value;
	memset(fr, 0, len);

	if(null_exists)
	{
		if(null)
		{
			return true;
		}
		*fr = 1;
		fr++;
	}
	switch(type)
	{
		case tf_binary:
			if(value.Length() == 0) break;
			j = 1;
			if(length == 16 && showGUID) // ���� �������� ��� showGUIDasMS
			{
				if(value.Length() < 36) break;
				for(i = 12; i < 16; i++) fr[i] = (from_hex_digit(value[j++]) << 4) + from_hex_digit(value[j++]);
				j++;
				for(i = 10; i < 12; i++) fr[i] = (from_hex_digit(value[j++]) << 4) + from_hex_digit(value[j++]);
				j++;
				for(i = 8; i < 10; i++) fr[i] = (from_hex_digit(value[j++]) << 4) + from_hex_digit(value[j++]);
				j++;
				for(i = 0; i < 2; i++) fr[i] = (from_hex_digit(value[j++]) << 4) + from_hex_digit(value[j++]);
				j++;
				for(i = 2; i < 8; i++) fr[i] = (from_hex_digit(value[j++]) << 4) + from_hex_digit(value[j++]);
			}
			else{
				if(value.Length() < length * 2) break;
				for(i = 0; i < length; i++) fr[i] = (from_hex_digit(value[j++]) << 4) + from_hex_digit(value[j++]);
			}
			break;
		case tf_bool:
			if(value == L"true") *fr = 1;
			else *fr = 0;
			break;
		case tf_numeric:
			l = value.Length();
			if(!l) break;

			b = new unsigned char[l];
			k = false; // ���� �����
			m = -1; // ������� �����
			n = false; // ������� ������� �������� ���� � ������

			for(i = 0, j = 0; i < l; i++)
			{
				sym = value[i + 1];
				if(sym == L'-')
				{
					k = true;
					continue;
				}
				if(sym == L'.')
				{
					m = j;
					n = true;
					continue;
				}
				if(!n) if(sym == L'0')
				{
					continue;
				}
				if(sym >= L'0' || sym <= L'9')
				{
					b[j++] = sym - L'0';
					n = true;
				}
			}
			if(m == -1) m = j;

			// ��� �����:
			// � b �������� �����
			// k - ������� ������
			// j - ����� �������� ����
			// m - ������� ����� (���������� ���� �� �������, ��� ���� � �� ��)

			//     0     1     2     3
			//+-----+-----+-----+-----+
			//I  .  I  .  I  .  I  .  I
			//+-----+-----+-----+-----+
			//  S  0  1  2  3  4  5  6  (����� ����� (���������), ���� ����� i)

			l = length - precision; // ����. ���������� ���� �� �������
			if(m > l)
			{
				// �������� ��������� ����������� ����������, �������� �� ��� 9��
				for(i = 0; i < length; i++)
				{
					if(i & 1) fr[(i + 1) >> 1] |= 0x90;
					else fr[(i + 1) >> 1] |= 0x9;
				}
			}
			else
			{
				for(i = l - 1, p = m - 1; p >= 0; i--, p--)
				{
					if(i & 1) fr[(i + 1) >> 1] |= b[p] << 4;
					else fr[(i + 1) >> 1] |= b[p];
				}
				q = min(j - m, precision); // ���������� ���� ����� �������
				for(i = l, p = m; p < m + q; i++, p++)
				{
					if(i & 1) fr[(i + 1) >> 1] |= b[p] << 4;
					else fr[(i + 1) >> 1] |= b[p];
				}
			}

			if(!k) *fr |= 0x10; // ����

			delete[] b;

			break;
		case tf_char:
			l = value.Length();
			i = min(l, length);
			memcpy(fr, value.c_str(), i << 1);
			while(i < length) ((wchar_t*)fr)[i++] = L' ';
			break;
		case tf_varchar:
			l = value.Length();
			i = min(l, length);
			*(short int*)fr = i;
			memcpy(fr + 2, value.c_str(), i * 2);
			while(i < length) ((wchar_t*)(fr + 2))[i++] = L' ';
			break;
		case tf_version:
			return false;
//			return String(*(int*)fr) + ":" + *(int*)(fr + 4) + ":" + *(int*)(fr + 8) + ":" + *(int*)(fr + 12);
		case tf_version8:
			return false;
//			return String(*(int*)fr) + ":" + *(int*)(fr + 4);
		case tf_string:
			return false;
//			return L"{MEMO}";
		case tf_text:
			return false;
//			return L"{TEXT}";
		case tf_image:
			return false;
//			return L"{IMAGE}";
		case tf_datetime:
			if(value.Length() < 19)
			{
				//memset(fr, 0, 7);
				fr[1] = 1;
				fr[2] = 1;
				fr[3] = 1;
			}
			else
			{

				#define correct_spaces(A,B)\
				if(value[B] == L' ')\
				{\
					value[B] = value[A];\
					value[A] = L'0';\
				}\
				if(value[A] == L' ') value[A] = L'0';

				correct_spaces(1,2) // ������������ ����
				correct_spaces(4,5) // ������������ �����
				correct_spaces(12,13) // ������������ ����
				correct_spaces(15,16) // ������������ ������
				correct_spaces(18,19) // ������������ �������

				i = 0;
				// ������������ ���
				while(value[10] == L' ')
				{
					value[10] = value[9];
					value[9] = value[8];
					value[8] = value[7];
					value[7] = L'0';
					i++;
				}
				while(value[9] == L' ')
				{
					value[9] = value[8];
					value[8] = value[7];
					value[7] = L'0';
					i++;
				}
				while(value[8] == L' ')
				{
					value[8] = value[7];
					value[7] = L'0';
					i++;
				}
				if(value[7] == L' ') value[7] = L'0';

				// ��������� ��� ��� �������������
				switch(i)
				{
					case 1:
						value[7] = L'2';
						break;
					case 2:
						value[7] = L'2';
						value[8] = L'0';
						break;
					case 3:
						value[7] = L'2';
						value[8] = L'0';
						value[9] = L'1';
						break;
				}

				// ���������� ����, �����, ���
				i = (value[1] - L'0') * 10 + (value[2] - L'0'); // ����
				m = (value[4] - L'0') * 10 + (value[5] - L'0'); // �����
				j = (value[7] - L'0') * 1000 + (value[8] - L'0') * 100 + (value[9] - L'0') * 10 + (value[10] - L'0'); // ���

				if(m > 12)
				{
					m = 12;
					value[4] = L'1';
					value[5] = L'2';
				}
				else if(m == 0)
				{
					m = 1;
					value[4] = L'0';
					value[5] = L'1';
				}

				if(j == 0)
				{
					j = 1;
					value[7] = L'0'; //-V525
					value[8] = L'0';
					value[9] = L'0';
					value[10] = L'1';
				}

				if(i == 0)
				{
					value[1] = L'0';
					value[2] = L'1';
				}
				else if(i > 28) switch(m)
				{
					case 1:
					case 3:
					case 5:
					case 7:
					case 8:
					case 10:
					case 12:
						if(i > 31)
						{
							value[1] = L'3';
							value[2] = L'1';
						}
						break;
					case 4:
					case 6:
					case 9:
					case 11:
						if(i > 30)
						{
							value[1] = L'3';
							value[2] = L'0';
						}
						break;
					case 2:
						if(j % 4 == 0 && (j % 100 != 0 || j % 400 == 0))
						{
							if(i > 29)
							{
								value[1] = L'2';
								value[2] = L'9';
							}
						}
						else
						{
							if(i > 28)
							{
								value[1] = L'2';
								value[2] = L'8';
							}
						}
						break;
				}

				// ���������� ����, ������, �������
				i = (value[12] - L'0') * 10 + (value[13] - L'0'); // ����
				if(i > 23)
				{
					value[12] = L'2';
					value[13] = L'3';
				}

				i = (value[15] - L'0') * 10 + (value[16] - L'0'); // ������
				if(i > 59)
				{
					value[15] = L'5';
					value[16] = L'9';
				}

				i = (value[18] - L'0') * 10 + (value[19] - L'0'); // �������
				if(i > 59)
				{
					value[18] = L'5';
					value[19] = L'9';
				}


				fr[3] = ((value[1] - L'0') << 4) + (value[2] - L'0');
				fr[2] = ((value[4] - L'0') << 4) + (value[5] - L'0');
				fr[0] = ((value[7] - L'0') << 4) + (value[8] - L'0');
				fr[1] = ((value[9] - L'0') << 4) + (value[10] - L'0');
				fr[4] = ((value[12] - L'0') << 4) + (value[13] - L'0');
				fr[5] = ((value[15] - L'0') << 4) + (value[16] - L'0');
				fr[6] = ((value[18] - L'0') << 4) + (value[19] - L'0');
			}
			break;
		case tf_varbinary:
			return false;
//			m = *(short int*)fr; // ����� + ��������
//			for(i = 0; i < m; i++)
//			{
//				sym = '0' + (fr[i + 2] >> 4);
//				if(sym > '9') sym += ('a' - '9' - 1);
//				buf[i << 1] = sym;
//				sym = '0' + (fr[i + 2] & 0xf);
//				if(sym > '9') sym += ('a' - '9' - 1);
//				buf[(i << 1) + 1] = sym;
//			}
//			buf[m << 1] = 0;
//			return buf;
	}

	return true;
}

//---------------------------------------------------------------------------
String __fastcall field::get_XML_presentation(char* rec, bool ignore_showGUID)
{
	char sym;
	int i, j, m;
	bool k;

	TMemoryStream* in;
	TMemoryStream* out;
	String s;

	unsigned char* fr = (unsigned char*)rec + offset;

	if(null_exists)
	{
		if(fr[0] == 0) return L"";
		fr++;
	}
	switch(type)
	{
		case tf_binary:
			if(length == 16 && (showGUID || ignore_showGUID))
			{
				if(showGUIDasMS) return GUIDasMS(fr);
				else return GUIDas1C(fr);
			}
			else
			{
				for(i = 0; i < length; i++)
				{
					sym = '0' + (fr[i] >> 4);
					if(sym > '9') sym += ('a' - '9' - 1);
					buf[i << 1] = sym;
					sym = '0' + (fr[i] & 0xf);
					if(sym > '9') sym += ('a' - '9' - 1);
					buf[(i << 1) + 1] = sym;
				}
				buf[length << 1] = 0;
			}
			return buf;
		case tf_bool:
			if(fr[0]) return L"true";
			return L"false";
		case tf_numeric:
			i = 0; // ������� ������ � buf
			k = true; // �������, ��� �������� ����� ��� �� ��������
			m = length - precision; // ������� ���������� ����� �����
			if(fr[0] >> 4 == 0) buf[i++] = '-';
			for(j = 0; j < length; j++)
			{
				if(j == m)
				{
					buf[i++] = '.';
					k = false;
				}
				if(j & 1) sym = fr[(j + 1) >> 1] >> 4;
				else sym = fr[j >> 1] & 0xf;
				if(sym == 0 && k) continue;
				k = false;
				buf[i++] = '0' + sym;
			}
			if(k) return "0";
			buf[i] = 0;
			return buf;
		case tf_char:
			return toXML(String((wchar_t*)fr, length));
		case tf_varchar:
			i = *(short int*)fr;
			return toXML(String((wchar_t*)(fr + 2), i));
		case tf_version:
			return String(*(int*)fr) + ":" + *(int*)(fr + 4) + ":" + *(int*)(fr + 8) + ":" + *(int*)(fr + 12);
		case tf_version8:
			return String(*(int*)fr) + ":" + *(int*)(fr + 4);
		case tf_string:
			out = new TMemoryStream();
			parent->readBlob(out, *(unsigned int*)fr, *(unsigned int*)(fr + 4));
			s = toXML(String((wchar_t*)(out->Memory), out->Size / 2));
			delete out;
			return s;
		case tf_text:
			out = new TMemoryStream();
			parent->readBlob(out, *(unsigned int*)fr, *(unsigned int*)(fr + 4));
			s = toXML(String((char*)(out->Memory), out->Size));
			delete out;
			return s;
		case tf_image:
			in = new TMemoryStream();
			out = new TMemoryStream();
			parent->readBlob(in, *(unsigned int*)fr, *(unsigned int*)(fr + 4));
			base64_encode(in, out, 72);
			s = String((wchar_t*)(out->Memory), out->Size / 2);
			delete in;
			delete out;
			return s;
		case tf_datetime:
			buf[0] = '0' + (fr[0] >> 4);
			buf[1] = '0' + (fr[0] & 0xf);
			buf[2] = '0' + (fr[1] >> 4);
			buf[3] = '0' + (fr[1] & 0xf);
			buf[4] = '-';
			buf[5] = '0' + (fr[2] >> 4);
			buf[6] = '0' + (fr[2] & 0xf);
			buf[7] = '-';
			buf[8] = '0' + (fr[3] >> 4);
			buf[9] = '0' + (fr[3] & 0xf);
			buf[10] = 'T';
			buf[11] = '0' + (fr[4] >> 4);
			buf[12] = '0' + (fr[4] & 0xf);
			buf[13] = ':';
			buf[14] = '0' + (fr[5] >> 4);
			buf[15] = '0' + (fr[5] & 0xf);
			buf[16] = ':';
			buf[17] = '0' + (fr[6] >> 4);
			buf[18] = '0' + (fr[6] & 0xf);
			buf[19] = 0;
			return buf;
		case tf_varbinary:
			m = *(short int*)fr; // ����� + ��������
			for(i = 0; i < m; i++)
			{
				sym = '0' + (fr[i + 2] >> 4);
				if(sym > '9') sym += ('a' - '9' - 1);
				buf[i << 1] = sym;
				sym = '0' + (fr[i + 2] & 0xf);
				if(sym > '9') sym += ('a' - '9' - 1);
				buf[(i << 1) + 1] = sym;
			}
			buf[m << 1] = 0;
			return buf;
	}

	return L"{?}";
}

//---------------------------------------------------------------------------
type_fields __fastcall field::gettype()
{
	return type;
}

//---------------------------------------------------------------------------
table* __fastcall field::getparent()
{
	return parent;
}

//---------------------------------------------------------------------------
bool __fastcall field::getnull_exists()
{
	return null_exists;
}

//---------------------------------------------------------------------------
int __fastcall field::getlength()
{
	return length;
}

//---------------------------------------------------------------------------
int __fastcall field::getprecision()
{
	return precision;
}

//---------------------------------------------------------------------------
bool __fastcall field::getcase_sensitive()
{
	return case_sensitive;
}

//---------------------------------------------------------------------------
int __fastcall field::getoffset()
{
	return offset;
}

//---------------------------------------------------------------------------
String __fastcall field::get_presentation_type()
{
	switch(type)
	{
//		case tf_binary: return "�������� ������"; break;
//		case tf_bool: return "������"; break;
//		case tf_numeric: return "�����"; break;
//		case tf_char: return "������ ������������� �����"; break;
//		case tf_varchar: return "������ ���������� �����"; break;
//		case tf_version: return "������"; break;
//		case tf_string: return "Unicode-c����� �������������� �����"; break;
//		case tf_text: return "Ascii-c����� �������������� �����"; break;
//		case tf_image: return "�������� ������ �������������� �����"; break;
//		case tf_datetime: return "����-�����"; break;
//		case tf_version8: return "������� ������"; break;
		case tf_binary: return "binary";
		case tf_bool: return "bool";
		case tf_numeric: return "number";
		case tf_char: return "fixed string";
		case tf_varchar: return "string";
		case tf_version: return "version";
		case tf_string: return "memo";
		case tf_text: return "text";
		case tf_image: return "image";
		case tf_datetime: return "datetime";
		case tf_version8: return "hidden version";
		case tf_varbinary: return "var binary";
	}
	return "{?}";
}

String __fastcall TrimSpacesRight(String s)
{
	while(s.SubString(s.Length(), 1) == L" ") s.SetLength(s.Length() - 1);
	return s;
}

#ifndef PublicRelease
unsigned int __fastcall field::getSortKey(const char* rec, unsigned char* SortKey, int maxlen)
{
	T_1CD* base;
	ICU_Result res;
	int i, j, m;
	bool k;
	unsigned int addlen = 0;
	int maxl = maxlen;
	bool isnull = false;
	String s;
	unsigned char c;

	unsigned char* fr = rec + offset;

	char nbuf[64];

	if(!maxlen)
	{
		if(err) err->AddError(L"������ ��������� ����� ���������� ����. ������� ����� ������.",
			L"�������", parent->name,
			L"����", name,
			L"�������� ����", get_presentation(rec));

		return 0;
	}

	if(null_exists)
	{
		if(*fr == 0)
		{
			*(SortKey++) = 0;
			isnull = true;
		}
		else *(SortKey++) = 1;

		fr++;
		maxl--;
		addlen = 1;
	}

	switch(type)
	{
		case tf_binary:
		case tf_datetime:
		case tf_bool:
			if(len > maxlen)
			{
				if(err) err->AddError(L"������ ��������� ����� ���������� ����. ����� ������ ������ �����������.",
					L"�������", parent->name,
					L"����", name,
					L"�������� ����", get_presentation(rec),
					L"����� ������", maxlen,
					L"����������� ����� ������", len);

				memcpy(SortKey, isnull ? null_index : fr, maxl);
				return maxlen;
			}
			memcpy(SortKey, isnull ? null_index : fr, len - addlen);
			return len;

		case tf_numeric:

			if(isnull)
			{
				if(len > maxlen)
				{
					if(err) err->AddError(L"������ ��������� ����� ���������� ����. ����� ������ ������ �����������.",
						L"�������", parent->name,
						L"����", name,
						L"�������� ����", get_presentation(rec),
						L"����� ������", maxlen,
						L"����������� ����� ������", len);

					memcpy(SortKey, null_index, maxl);
					return maxlen;
				}
				memcpy(SortKey, null_index, len - addlen);
				return len;

			}

			memcpy(nbuf, fr, len - addlen);
			if((*nbuf & 0xf0) == 0) //�������������!
			{
				k = false; // �������
				for(i = length; i > 0; i--)
				{
					j = i >> 1;
					if(i & 1)
					{
						c = nbuf[j] & 0x0f;
						if(k) c = 9 - c;
						else
						{
							if(c)
							{
								k = true;
								c = 10 - c;
							}
						}
						nbuf[j] = (nbuf[j] & 0xf0) | c;
					}
					else
					{
						c = nbuf[j] >> 4 & 0x0f;
						if(k) c = 9 - c;
						else
						{
							if(c)
							{
								k = true;
								c = 10 - c;
							}
						}
						nbuf[j] = (nbuf[j] & 0x0f) | (c << 4);
					}
				}
				if(!k) *nbuf = 0x10; // ���� �������� ��������� -0 (����� ����), �� ������ ���������� +0.
			}

			if(len > maxlen)
			{
				if(err) err->AddError(L"������ ��������� ����� ���������� ����. ����� ������ ������ �����������.",
					L"�������", parent->name,
					L"����", name,
					L"�������� ����", get_presentation(rec),
					L"����� ������", maxlen,
					L"����������� ����� ������", len);

				memcpy(SortKey, nbuf, maxl);
				return maxlen;
			}
			memcpy(SortKey, nbuf, len - addlen);
			return len;

		case tf_char:
			base = parent->base;
			if(!isnull) s = TrimSpacesRight(String((wchar_t*)fr, length));

			if(isnull || s.Length() == 0)
			{
				if(maxl < 2)
				{
					if(err) err->AddError(L"������ ��������� ����� ���������� ����. ����� ������ ������ �����������.",
						L"�������", parent->name,
						L"����", name,
						L"�������� ����", get_presentation(rec),
						L"����� ������", maxlen,
						L"����������� ����� ������", 2 + addlen);
					memcpy(SortKey, null_index, maxl);
					return maxlen;
				}
				memcpy(SortKey, null_index, 2);
				return 2 + addlen;
			}

			res = base->icu->getSortKey(s.c_str(), SortKey, maxl, j, case_sensitive);
			i = length * 3 + 2;
			if(j > i) j = i;
			switch(res)
			{
				case r_OK:
					return j + addlen;
				case r_badLocale:
					if(err) err->AddError(L"������ ��������� ����� ���������� ����. ������������ Locale.",
						L"�������", parent->name,
						L"����", name,
						L"�������� ����", get_presentation(rec),
						L"Locale", base->locale);
					return addlen;
				case r_keyTooSmall:
					if(err) err->AddError(L"������ ��������� ����� ���������� ����. ����� ������ ������ �����������.",
						L"�������", parent->name,
						L"����", name,
						L"�������� ����", get_presentation(rec),
						L"����� ������", maxlen,
						L"����������� ����� ������", j + addlen);
					return maxlen;
				case r_LocaleNotSet:
					if(err) err->AddError(L"������ ��������� ����� ���������� ����. Locale �� ����������.",
						L"�������", parent->name,
						L"����", name,
						L"�������� ����", get_presentation(rec));
					return addlen;
				case r_notInit:
					if(err) err->AddError(L"������ ��������� ����� ���������� ����. ��������� ICU �� ���������������.",
						L"�������", parent->name,
						L"����", name,
						L"�������� ����", get_presentation(rec));
					return addlen;
				default:
					if(err) err->AddError(L"������ ��������� ����� ���������� ����. ����������� ��� ��������.",
						L"�������", parent->name,
						L"����", name,
						L"�������� ����", get_presentation(rec));
					return addlen;
			}

		case tf_varchar:
			i = *(short int*)fr;
			base = parent->base;
			if(!isnull) s = TrimSpacesRight(String((wchar_t*)(fr + 2), i));

			if(isnull || s.Length() == 0)
			{
				if(maxl < 2)
				{
					if(err) err->AddError(L"������ ��������� ����� ���������� ����. ����� ������ ������ �����������.",
						L"�������", parent->name,
						L"����", name,
						L"�������� ����", get_presentation(rec),
						L"����� ������", maxlen,
						L"����������� ����� ������", 2 + addlen);
					memcpy(SortKey, null_index, maxl);
					return maxlen;
				}
				memcpy(SortKey, null_index, 2);
				return 2 + addlen;
			}

			res = base->icu->getSortKey(s.c_str(), SortKey, maxl, j, case_sensitive);
			i = length * 3 + 2;
			if(j > i) j = i;
			switch(res)
			{
				case r_OK:
					return j + addlen;
				case r_badLocale:
					if(err) err->AddError(L"������ ��������� ����� ���������� ����. ������������ Locale.",
						L"�������", parent->name,
						L"����", name,
						L"�������� ����", get_presentation(rec),
						L"Locale", base->locale);
					return addlen;
				case r_keyTooSmall:
					if(err) err->AddError(L"������ ��������� ����� ���������� ����. ����� ������ ������ �����������.",
						L"�������", parent->name,
						L"����", name,
						L"�������� ����", get_presentation(rec),
						L"����� ������", maxlen,
						L"����������� ����� ������", j + addlen);
					return maxlen;
				case r_LocaleNotSet:
					if(err) err->AddError(L"������ ��������� ����� ���������� ����. Locale �� ����������.",
						L"�������", parent->name,
						L"����", name,
						L"�������� ����", get_presentation(rec));
					return addlen;
				case r_notInit:
					if(err) err->AddError(L"������ ��������� ����� ���������� ����. ��������� ICU �� ���������������.",
						L"�������", parent->name,
						L"����", name,
						L"�������� ����", get_presentation(rec));
					return addlen;
				default:
					if(err) err->AddError(L"������ ��������� ����� ���������� ����. ����������� ��� ��������.",
						L"�������", parent->name,
						L"����", name,
						L"�������� ����", get_presentation(rec));
					return addlen;
			}

		case tf_version:
		case tf_version8:
		case tf_string:
		case tf_text:
		case tf_image:
		case tf_varbinary:
			if(err) err->AddError(L"������� ��������� ����� ���������� ����������������� ���� ����.",
				L"�������", parent->name,
				L"����", name,
				L"��� ����", get_presentation_type());
			return 0;
	}

	return addlen;

}
#endif //#ifdef PublicRelease

bool __fastcall field::save_blob_to_file(char* rec, String _filename, bool unpack)
{
	TStream* blob_stream;
	bool zippedContainer;
	char _buf[16];
	TStream* _s;
	TStream* _s2;
	TStream* _sx;
	TStream* _sx2;
	__int64 len1C;
	bool is_users_usr;
	unsigned int i, j, k, l;
	char* _xor_mask;
	char* _xor_buf;
	table* tab;
	TStream* temp_stream;
	bool zipped;
	field* _f;
	char* _bb;
	v8catalog* cat;
	char* orec;
	bool maybezipped2;

	bool usetemporaryfiles = false;
	bool usetempfile1 = false;
	bool usetempfile2 = false;
	bool usetempfile3 = false;
	wchar_t temppath[MAX_PATH];
	wchar_t tempfile1[MAX_PATH];
	wchar_t tempfile2[MAX_PATH];
	wchar_t tempfile3[MAX_PATH];

	orec = rec;
	rec += offset;
	if(null_exists)
	{
		if(*rec == 0) return false;
		rec++;
	}

	if(*(unsigned int*)rec == 0 || *(unsigned int*)(rec + 4) == 0) return false;

	if(!unpack)
	{
		temp_stream = new TFileStream(_filename, fmCreate);
		parent->readBlob(temp_stream, *(unsigned int*)rec, *(unsigned int*)(rec + 4));
		delete temp_stream;
		return true;
	}

	usetemporaryfiles = *(unsigned int*)(rec + 4) > 10 * 1024 * 1024;
	if(usetemporaryfiles)
	{
		GetTempPath(MAX_PATH - 1, temppath);
		GetTempFileName(temppath, L"t1cd", 0, tempfile1);
		blob_stream = new TFileStream(String(tempfile1), fmCreate);
		usetempfile1 = true;
	}
	else blob_stream = new TMemoryStream;
	parent->readBlob(blob_stream, *(unsigned int*)rec, *(unsigned int*)(rec + 4));
	if(blob_stream->Size == 0)
	{
		delete blob_stream;
		if(usetempfile1) DeleteFile(tempfile1);
		//temp_stream = new TFileStream(_filename, fmCreate);
		//delete temp_stream;
		return false;
	}

	tab = parent;
	if(usetemporaryfiles)
	{
		GetTempFileName(temppath, L"t1cd", 0, tempfile2);
		_s = new TFileStream(String(tempfile2), fmCreate);
		usetempfile2 = true;
	}
	else _s = new TMemoryStream;

	if(tab->get_issystem())
	{

		// ������������� ��� users.usr
		String tabname = tab->getname();
		is_users_usr = false;
		if(tabname.CompareIC(L"PARAMS") == 0)
		{
			_f = tab->getfield(0);
			if(_f->get_presentation(orec).CompareIC(L"users.usr") == 0) is_users_usr = true;
		}
		else if(tabname.CompareIC(L"V8USERS") == 0) is_users_usr = true;

		maybezipped2 = true;
		if(tabname.CompareIC(L"CONFIG") == 0 || tabname.CompareIC(L"CONFIGSAVE") == 0)
		{
			_f = tab->getfield(0);
			maybezipped2 = _f->get_presentation(orec).Length() > 72;
		}

		if(is_users_usr)
		{

			i = blob_stream->Size;
			_bb = new char[i];
			blob_stream->Seek(0, soFromBeginning);
			blob_stream->Read(_bb, i);

			j = _bb[0];
			_xor_mask = _bb + 1;
			_xor_buf = _xor_mask + j;
			l = i - j - 1;
			for(i = 0, k = 0; i < l; i++, k++)
			{
				if(k >= j) k = 0;
				_xor_buf[i] ^= _xor_mask[k];
			}
			temp_stream = new TFileStream(_filename, fmCreate);
			temp_stream->Size = 0;
			temp_stream->WriteBuffer(_xor_buf, l);
			delete temp_stream;
			delete[] _bb;
		}
		else
		{
			zippedContainer = false;
			try
			{
				blob_stream->Seek(0, soFromBeginning);
				//_s->Size = 0;
				InflateStream(blob_stream, _s);
				zipped = true;
				if(maybezipped2) _sx = _s;
				else _sx2 = _s;
				_s = NULL;
				delete blob_stream;
				blob_stream = NULL;
			}
			catch (...)
			{
				//_s->Size = 0;
				//_s->CopyFrom(blob_stream, 0);
				_sx2 = blob_stream;
				delete _s;
				_s = NULL;
				blob_stream = NULL;
				zipped = false;
			}

			if(zipped && maybezipped2)
			{
				//_s2->Size = 0;
				//_s2->CopyFrom(_s, 0);
				if(usetemporaryfiles)
				{
					GetTempFileName(temppath, L"t1cd", 0, tempfile3);
					_s2 = new TFileStream(String(tempfile3), fmCreate);
					usetempfile3 = true;
				}
				else _s2 = new TMemoryStream;
				try
				{
					_sx->Seek(0, soFromBeginning);
					//_s->Size = 0;
					InflateStream(_sx, _s2);
					zippedContainer = true;
					_sx2 = _s2;
					_s2 = NULL;
					delete _sx;
					_sx = NULL;
				}
				catch (...)
				{
					//_s->Size = 0;
					//_s->CopyFrom(_s2, 0);
					_sx2 = _sx;
					_sx = NULL;
					delete _s2;
					_s2 = NULL;
				}
			}

			cat = new v8catalog(_sx2, zippedContainer, true);
			if(!cat->GetFirst())
			{
				temp_stream = new TFileStream(_filename, fmCreate);
				temp_stream->CopyFrom(_sx2, 0);
				delete temp_stream;
			}
			else cat->SaveToDir(_filename);
			delete cat;
			delete _sx2;

		}
	}
	else /*if(tab->get_issystem())*/
	{
		_s->CopyFrom(blob_stream, 0);
		blob_stream->Seek(0, soFromBeginning);
		if(blob_stream->Read(_buf, 2) >= 2) if((_buf[0] == 1 || _buf[0] == 2) && _buf[1] == 1)
		{
			if(usetemporaryfiles)
			{
				GetTempFileName(temppath, L"t1cd", 0, tempfile3);
				_s2 = new TFileStream(String(tempfile3), fmCreate);
				usetempfile3 = true;
			}
			else _s2 = new TMemoryStream;
			bool isOK = true;
			if(_buf[0] == 1) // ������������� ���������
			{
				_s2->CopyFrom(blob_stream, blob_stream->Size - 2);
			}
			else
			{
				if(blob_stream->Read(_buf, 16) < 16) isOK = false;
				else
				{
					if(memcmp(_buf, SIG_ZIP, 16) != 0) isOK = false;
					else
					{
						try
						{
							InflateStream(blob_stream, _s2);
						}
						catch(...)
						{
							isOK = false;
						}
					}
				}
			}
			if(isOK)
			{
				_s2->Seek(0, soFromBeginning);
				if(_s2->Read(_buf, 8) < 8) isOK = false;
				else
				{
					_s->Size = 0;
					_s->CopyFrom(_s2, _s2->Size - 8);
				}

			}

			if(isOK)
			{
				len1C = *(__int64*)_buf;
				if(_s->Size > len1C)
				{
					_s->Seek(len1C, (TSeekOrigin)soFromBeginning);
					_s2->Size = 0;
					_s2->CopyFrom(_s, _s->Size - len1C);
					_s2->Seek(0, soFromBeginning);
					if(_s2->Read(_buf, 12) >= 12)
					{
						len1C = *(__int64*)&_buf[4];
						if(len1C <= _s2->Size - 12)
						{
							_s->Size = 0;
							_s->CopyFrom(_s2, len1C);
						}
					}
				}
			}
			delete _s2;
		}

		temp_stream = new TFileStream(_filename, fmCreate);
		temp_stream->CopyFrom(_s, 0);
		delete temp_stream;
	}

	delete _s;
	delete blob_stream;
	if(usetempfile1) DeleteFile(tempfile1);
	if(usetempfile2) DeleteFile(tempfile2);
	if(usetempfile3) DeleteFile(tempfile3);

	return true;
}


//********************************************************
// ����� changed_rec

//---------------------------------------------------------------------------
__fastcall changed_rec::changed_rec(table* _parent, changed_rec_type crt, unsigned int phys_numrecord)
{
	parent = _parent;
	numrec = phys_numrecord;
	changed_type = crt;
	if(crt == crt_delete)
	{
		fields = NULL;
		rec = NULL;
	}
	else
	{
		fields = new char[parent->num_fields];
		memset(fields, 0, parent->num_fields);
		rec = new char[parent->recordlen];
		memset(rec, 0, parent->recordlen);
	}
	next = parent->ch_rec;
	parent->ch_rec = this;
}
//---------------------------------------------------------------------------
__fastcall changed_rec::~changed_rec()
{
	clear();
	delete[] fields;
	delete[] rec;
}

//---------------------------------------------------------------------------
void __fastcall changed_rec::clear()
{
	int i;
	field* f;
	type_fields tf;
	TStream* b;

	if(rec && fields) for(i = 0; i < parent->num_fields; i++) if(fields[i])
	{
		f = parent->fields[i];
		tf = f->gettype();
		if(tf == tf_image || tf == tf_string || tf == tf_text)
		{
			b = *(TStream**)(rec + f->getoffset() + (f->getnull_exists() ? 1 : 0));
			delete b;
		}
	}

}



//********************************************************
// ����� table

//---------------------------------------------------------------------------
bool __fastcall table::get_issystem()
{
	return issystem;
}

//---------------------------------------------------------------------------
__fastcall table::table(T_1CD* _base, int block_descr)
{
	String description;
	tree* t;
	tree* f;
	tree* ff;
	tree* in;
	tree* rt;
	int i, j, k;
	String ws;
	index* ind;
	int numrec;
	int blockfile[3];
	field* fld;
	unsigned int* buf;

	init();
	bad = true;
	base = _base;
	err = base->err;
	lockinmemory = 0;

	descr_table = new v8object(base, block_descr);
	description = getdescription();

	tree* root = parse_1Ctext(description, err, String("���� ") + block_descr);

	if(!root)
	{
		if(err) err->AddError(L"������ ������� ������ �������� �������.",
			L"����", tohex(block_descr));
		init();
		return;
	}

	if(root->get_num_subnode() != 1)
	{
		if(err) err->AddError(L"������ ������� ������ �������� �������. ���������� ����� �� ����� 1.",
			L"����", tohex(block_descr),
			L"�����", root->get_num_subnode());
		init();
		delete root;
		return;
	}
	rt = root->get_first();

	if(rt->get_num_subnode() != 6)
	{
		if(err) err->AddError(L"������ ������� ������ �������� �������. ���������� ����� �� ����� 6.",
			L"����", tohex(block_descr),
			L"�����", rt->get_num_subnode());
		init();
		delete root;
		return;
	}

	t = rt->get_first();
	if(t->get_type() != nd_string)
	{
		if(err) err->AddError(L"������ ��������� ����� �������. ���� �� �������� �������.",
			L"����", tohex(block_descr));
		init();
		delete root;
		return;
	}
	name = t->get_value();
	issystem = name[1] != L'_'
		|| name.SubString(name.Length() - 6, 7).CompareIC(L"STORAGE") == 0
		|| name.CompareIC(L"_SYSTEMSETTINGS") == 0
		|| name.CompareIC(L"_COMMONSETTINGS") == 0
		|| name.CompareIC(L"_REPSETTINGS") == 0
		|| name.CompareIC(L"_REPVARSETTINGS") == 0
		|| name.CompareIC(L"_FRMDTSETTINGS") == 0
		|| name.CompareIC(L"_SCHEDULEDJOBS") == 0;

#ifdef getcfname
	if(name.CompareIC(L"CONFIG"))
	{
		delete root;
		return;
	}
#endif

	t = t->get_next();
	// ���������� ����, ��� ��� ��� ������ ���������� "0", � ��� ��� �����, ���������� (������ ������� �������� ������?)
	t = t->get_next();
	//if(t->get_type() != nd_empty)
	if(t->get_type() != nd_list)
	{
		if(err) err->AddError(L"������ ��������� ����� �������. ���� �� �������� �������.",
			L"����", tohex(block_descr),
			L"�������", name);
		init();
		delete root;
		return;
	}
	if(t->get_num_subnode() < 2)
	{
		if(err) err->AddError(L"������ ��������� ����� �������. ��� ����� ������� �����.",
			L"����", tohex(block_descr),
			L"�������", name);
		init();
		delete root;
		return;
	}

	num_fields = t->get_num_subnode() - 1;
	num_fields2 = num_fields + 1; // ��������� ������ ���� �� ������ ������� �������� ���� ������
	fields = new field*[num_fields2];
	bool has_version = false; // ������� ������� ���� ������
	for(i = 0; i < num_fields2; i++) fields[i] = new field(this);

	f = t->get_first();
	if(f->get_type() != nd_string)
	{
		if(err) err->AddError(L"������ ��������� ����� �������. ��������� ���� Fields �� �������� �������.",
			L"����", tohex(block_descr),
			L"�������", name);
		deletefields();
		init();
		delete root;
		return;
	}
	if(f->get_value() != L"Fields")
	{
		if(err) err->AddError(L"������ ��������� ����� �������. ���� �� Fields.",
			L"����", tohex(block_descr),
			L"�������", name,
			L"����", f->get_value());
		deletefields();
		init();
		delete root;
		return;
	}

	for(i = 0; i < num_fields; i++)
	{
		f = f->get_next();
		if(f->get_num_subnode() != 6)
		{
			if(err) err->AddError(L"������ ��������� ���� ���������� ���� �������. ���������� ����� ���� �� ����� 6.",
				L"����", tohex(block_descr),
				L"�������", name,
				L"����� ����", i + 1,
				L"�����", f->get_num_subnode());
			deletefields();
			init();
			delete root;
			return;
		}

		ff = f->get_first();
		if(ff->get_type() != nd_string)
		{
			if(err) err->AddError(L"������ ��������� ����� ���� �������. ���� �� �������� �������.",
				L"����", tohex(block_descr),
				L"�������", name,
				L"����� ����", i + 1);
			deletefields();
			init();
			delete root;
			return;
		}
		fld = fields[i];
		fld->name = ff->get_value();

		ff = ff->get_next();
		if(ff->get_type() != nd_string)
		{
			if(err) err->AddError(L"������ ��������� ���� ���� �������. ���� �� �������� �������.",
				L"����", tohex(block_descr),
				L"�������", name,
				L"����", fld->name);
			deletefields();
			init();
			delete root;
			return;
		}
		ws = ff->get_value();
		if(ws == L"B") fld->type = tf_binary;
		else if(ws == L"L") fld->type = tf_bool;
		else if(ws == L"N") fld->type = tf_numeric;
		else if(ws == L"NC") fld->type = tf_char;
		else if(ws == L"NVC") fld->type = tf_varchar;
		else if(ws == L"RV")
		{
			fld->type = tf_version;
			has_version = true;
		}
		else if(ws == L"NT") fld->type = tf_string;
		else if(ws == L"T") fld->type = tf_text;
		else if(ws == L"I") fld->type = tf_image;
		else if(ws == L"DT") fld->type = tf_datetime;
		else if(ws == L"VB") fld->type = tf_varbinary;
		else
		{
			if(err) err->AddError(L"����������� ��� ���� �������.",
				L"����", tohex(block_descr),
				L"�������", name,
				L"����", fld->name,
				L"��� ����", ws);
			deletefields();
			init();
			delete root;
			return;
		}

		ff = ff->get_next();
		if(ff->get_type() != nd_number)
		{
			if(err) err->AddError(L"������ ��������� �������� NULL ���� �������. ���� �� �������� ������.",
				L"����", tohex(block_descr),
				L"�������", name,
				L"����", fld->name,
				L"��� ����", ws);
			deletefields();
			init();
			delete root;
			return;
		}
		ws = ff->get_value();
		if(ws == L"0") fld->null_exists = false;
		else if(ws == L"1") fld->null_exists = true;
		else
		{
			if(err) err->AddError(L"����������� �������� �������� NULL ���� �������.",
				L"����", tohex(block_descr),
				L"�������", name,
				L"����", fld->name,
				L"������� NULL", ws);
			deletefields();
			init();
			delete root;
			return;
		}

		ff = ff->get_next();
		if(ff->get_type() != nd_number)
		{
			if(err) err->AddError(L"������ ��������� ����� ���� �������. ���� �� �������� ������.",
				L"����", tohex(block_descr),
				L"�������", name,
				L"����", fld->name);
			deletefields();
			init();
			delete root;
			return;
		}
		fld->length = StrToInt(ff->get_value());

		ff = ff->get_next();
		if(ff->get_type() != nd_number)
		{
			if(err) err->AddError(L"������ ��������� �������� ���� �������. ���� �� �������� ������.",
				L"����", tohex(block_descr),
				L"�������", name,
				L"����", fld->name);
			deletefields();
			init();
			delete root;
			return;
		}
		fld->precision = StrToInt(ff->get_value());

		ff = ff->get_next();
		if(ff->get_type() != nd_string)
		{
			if(err) err->AddError(L"������ ��������� ������������������������ ���� �������. ���� �� �������� �������.",
				L"����", tohex(block_descr),
				L"�������", name,
				L"����", fld->name);
			deletefields();
			init();
			delete root;
			return;
		}
		ws = ff->get_value();
		if(ws == L"CS") fld->case_sensitive = true;
		else if(ws == L"CI") fld->case_sensitive = false;
		else
		{
			if(err) err->AddError(L"����������� �������� ������������������������ ���� �������.",
				L"����", tohex(block_descr),
				L"�������", name,
				L"����", fld->name,
				L"������������������������", ws);
			deletefields();
			init();
			delete root;
			return;
		}
	}

	t = t->get_next();
	if(t->get_type() != nd_list)
	{
		if(err) err->AddError(L"������ ��������� �������� �������. ���� �� �������� �������.",
			L"����", tohex(block_descr),
			L"�������", name);
		deletefields();
		init();
		delete root;
		return;
	}
	if(t->get_num_subnode() < 1)
	{
		if(err) err->AddError(L"������ ��������� �������� �������. ��� ����� ������� ��������.",
			L"����", tohex(block_descr),
			L"�������", name);
		deletefields();
		init();
		delete root;
		return;
	}

	num_indexes = t->get_num_subnode() - 1;
	if(num_indexes)
	{
		indexes = new index*[num_indexes];
		for(i = 0; i < num_indexes; i++) indexes[i] = new index(this);

		f = t->get_first();
		if(f->get_type() != nd_string)
		{
			if(err) err->AddError(L"������ ��������� �������� �������. ��������� ���� Indexes �� �������� �������.",
				L"����", tohex(block_descr),
				L"�������", name);
			deletefields();
			deleteindexes();
			init();
			delete root;
			return;
		}
		if(f->get_value() != L"Indexes")
		{
			if(err) err->AddError(L"������ ��������� �������� �������. ���� �� Indexes.",
				L"����", tohex(block_descr),
				L"�������", name,
				L"����", f->get_value());
			deletefields();
			deleteindexes();
			init();
			delete root;
			return;
		}

		for(i = 0; i < num_indexes; i++)
		{
			f = f->get_next();
			numrec = f->get_num_subnode() - 2;
			if(numrec < 1)
			{
				if(err) err->AddError(L"������ ��������� ���������� ������� �������. ��� ����� ������� ����� �������.",
					L"����", tohex(block_descr),
					L"�������", name,
					L"����� �������", i + 1);
				deletefields();
				deleteindexes();
				init();
				delete root;
				return;
			}
			ind = indexes[i];
			ind->num_records = numrec;

			if(f->get_type() != nd_list)
			{
				if(err) err->AddError(L"������ ��������� ���������� ������� �������. ���� �� �������� �������.",
					L"����", tohex(block_descr),
					L"�������", name,
					L"����� �������", i + 1);
				deletefields();
				deleteindexes();
				init();
				delete root;
				return;
			}

			ff = f->get_first();
			if(ff->get_type() != nd_string)
			{
				if(err) err->AddError(L"������ ��������� ����� ������� �������. ���� �� �������� �������.",
					L"����", tohex(block_descr),
					L"�������", name,
					L"����� �������", i + 1);
				deletefields();
				deleteindexes();
				init();
				delete root;
				return;
			}
			ind->name = ff->get_value();

			ff = ff->get_next();
			if(ff->get_type() != nd_number)
			{
				if(err) err->AddError(L"������ ��������� ���� ������� �������. ���� �� �������� ������.",
					L"����", tohex(block_descr),
					L"�������", name,
					L"������", ind->name);
				deletefields();
				deleteindexes();
				init();
				delete root;
				return;
			}
			ws = ff->get_value();
			if(ws == L"0") ind->is_primary = false;
			else if(ws == L"1") ind->is_primary = true;
			else
			{
				if(err) err->AddError(L"����������� ��� ������� �������.",
					L"����", tohex(block_descr),
					L"�������", name,
					L"������", ind->name,
					L"��� �������", ws);
				deletefields();
				deleteindexes();
				init();
				delete root;
				return;
			}

			ind->records = new index_record[numrec];
			for(j = 0; j < numrec; j++)
			{
				ff = ff->get_next();
				if(ff->get_num_subnode() != 2)
				{
					if(err) err->AddError(L"������ ��������� ���������� ���� ������� �������. ���������� ����� ���� �� ����� 2.",
						L"����", tohex(block_descr),
						L"�������", name,
						L"������", ind->name,
						L"����� ���� �������", j + 1,
						L"�����", ff->get_num_subnode());
					deletefields();
					deleteindexes();
					init();
					delete root;
					return;
				}

				in = ff->get_first();
				if(in->get_type() != nd_string)
				{
					if(err) err->AddError(L"������ ��������� ����� ���� ������� �������. ���� �� �������� �������.",
						L"����", tohex(block_descr),
						L"�������", name,
						L"������", ind->name,
						L"����� ���� �������", j + 1);
					deletefields();
					deleteindexes();
					init();
					delete root;
					return;
				}

				ws = in->get_value();
				for(k = 0; k < num_fields; k++)
				{
					if(fields[k]->name == ws)
					{
						ind->records[j].field = fields[k];
						break;
					}
				}
				if(k >= num_fields)
				{
					if(err) err->AddError(L"������ ��������� ������� �������. �� ������� ���� ������� �� ����� ���� �������.",
						L"����", tohex(block_descr),
						L"�������", name,
						L"������", ind->name,
						L"���� �������", ws);
					deletefields();
					deleteindexes();
					init();
					delete root;
					return;
				}

				in = in->get_next();
				if(in->get_type() != nd_number)
				{
					if(err) err->AddError(L"������ ��������� ����� ���� ������� �������. ���� �� �������� ������.",
						L"����", tohex(block_descr),
						L"�������", name,
						L"������", ind->name,
						L"���� �������", ws);
					deletefields();
					deleteindexes();
					init();
					delete root;
					return;
				}
				ind->records[j].len = StrToInt(in->get_value());
			}
		}
	}
	else indexes = NULL;

	t = t->get_next();
	if(t->get_num_subnode() != 2)
	{
		if(err) err->AddError(L"������ ��������� ���� ���������� �������. ���������� ����� �� ����� 2.",
			L"����", tohex(block_descr),
			L"�������", name);
		deletefields();
		deleteindexes();
		init();
		delete root;
		return;
	}

	f = t->get_first();
	if(f->get_type() != nd_string)
	{
		if(err) err->AddError(L"������ ��������� ���� ���������� �������. ��������� ���� Recordlock �� �������� �������.",
			L"����", tohex(block_descr),
			L"�������", name);
		deletefields();
		deleteindexes();
		init();
		delete root;
		return;
	}
	if(f->get_value() != L"Recordlock")
	{
		if(err) err->AddError(L"������ ��������� ���� ���������� �������. ���� �� Recordlock.",
			L"����", tohex(block_descr),
			L"�������", name,
			L"����", f->get_value());
		deletefields();
		deleteindexes();
		init();
		delete root;
		return;
	}

	f = f->get_next();
	if(f->get_type() != nd_string)
	{
		if(err) err->AddError(L"������ ��������� ���� ���������� �������. ���� �� �������� �������.",
			L"����", tohex(block_descr),
			L"�������", name);
		deletefields();
		deleteindexes();
		init();
		delete root;
		return;
	}
	ws = f->get_value();
	if(ws == L"0") recordlock = false;
	else if(ws == L"1") recordlock = true;
	else
	{
		if(err) err->AddError(L"����������� �������� ���� ���������� �������.",
			L"����", tohex(block_descr),
			L"�������", name,
			L"��� ����������", ws);
		deletefields();
		deleteindexes();
		init();
		delete root;
		return;
	}

	if(recordlock && !has_version)
	{// ��������� ������� ���� ������
		fld = fields[num_fields++];
		fld->name = L"VERSION";
		fld->type = tf_version8;
	}

	t = t->get_next();
	if(t->get_num_subnode() != 4)
	{
		if(err) err->AddError(L"������ ��������� ������ �������. ���������� ����� �� ����� 4.",
			L"����", tohex(block_descr),
			L"�������", name);
		deletefields();
		deleteindexes();
		init();
		delete root;
		return;
	}

	f = t->get_first();
	if(f->get_type() != nd_string)
	{
		if(err) err->AddError(L"������ ��������� ������ �������. ��������� ���� Files �� �������� �������.",
			L"����", tohex(block_descr),
			L"�������", name);
		deletefields();
		deleteindexes();
		init();
		delete root;
		return;
	}
	if(f->get_value() != L"Files")
	{
		if(err) err->AddError(L"������ ��������� ������ �������. ���� �� Files.",
			L"����", tohex(block_descr),
			L"�������", name,
			L"����", f->get_value());
		deletefields();
		deleteindexes();
		init();
		delete root;
		return;
	}

	for(i = 0; i < 3; i++)
	{
		f = f->get_next();
		if(f->get_type() != nd_number)
		{
			if(err) err->AddError(L"������ ��������� ����� �������. ���� �� �������� ������.",
				L"����", tohex(block_descr),
				L"�������", name,
				L"����� �����", i + 1);
			deletefields();
			deleteindexes();
			init();
			delete root;
			return;
		}
		blockfile[i] = StrToInt(f->get_value());
	}

	delete root;

	if(blockfile[0]) file_data = new v8object(base, blockfile[0]); else file_data = NULL;
	if(blockfile[1]) file_blob = new v8object(base, blockfile[1]); else file_blob = NULL;
	if(blockfile[2]) file_index = new v8object(base, blockfile[2]); else file_index = NULL;

	if(num_indexes && !file_index)
	{
		if(err) err->AddError(L"� ������� ���� �������, ������ ���� �������� �����������.",
			L"����", tohex(block_descr),
			L"�������", name,
			L"���������� ��������", num_indexes);
	}
	else if(!num_indexes && file_index)
	{
		if(err) err->AddError(L"� ������� ��� ��������, ������ ������������ ���� ��������.",
			L"����", tohex(block_descr),
			L"�������", name,
			L"���� ��������", tohex(blockfile[2]));
	}
	else if(file_index)
	{
		if(file_index->getlen() & 0xfff)
		{
			if(err) err->AddError(L"������ ������ ��������. ����� ����� �������� �� ������ 0x1000",
				L"�������", name,
				L"����� ����� ��������", tohex(file_index->getlen()),
				L"����� �������", i);
		}
		else
		{
			int buflen = num_indexes * 4 + 4;
//			buf = new unsigned int[buflen];
			buf = new unsigned int[num_indexes + 1];
			file_index->getdata(buf, 0, buflen);

//			// ��������, ��� ������� >>
//			if(buf[0]) if(err) err->AddMessage_(L"���������� ��������� �������� � ����� ��������", msHint,
//					L"�������", name,
//					L"������ ��������� ��������", tohex(buf[0]));
//			// ��������, ��� �������� <<

			if(buf[0] << 12 >= file_index->getlen())
			{
				if(err) err->AddError(L"������ ������ ��������. ������ ������� ���������� ����� �� ��������� ����� ��������",
					L"�������", name,
					L"����� ����� ��������", tohex(file_index->getlen()),
					L"������ ��������� ��������", tohex(buf[0]));
			}
			else
			{
				for(i = 1; i <= num_indexes; i++)
				{
					if(buf[i] >= file_index->getlen())
					{
						if(err) err->AddError(L"������ ������ ��������. ��������� �������� ������� �� ��������� ����� ��������",
							L"�������", name,
							L"����� ����� ��������", tohex(file_index->getlen()),
							L"����� �������", i,
							L"�������� �������", tohex(buf[i]));
					}
					else if(buf[i] & 0xfff)
					{
						if(err) err->AddError(L"������ ������ ��������. ��������� �������� ������� �� ������ 0x1000",
							L"�������", name,
							L"����� ����� ��������", tohex(file_index->getlen()),
							L"����� �������", i,
							L"�������� �������", tohex(buf[i]));
					}
					else indexes[i - 1]->start = buf[i];
				}
			}

			delete[] buf;

		}

	}

	// ��������� ����� ������ ������� ��� ����� ����� ����� � ��������� �������� ����� � ������
	recordlen = 1; // ������ ���� ������ - ������� �����������
	// ������� ���� ���� (����) � ����� "������"
	for(i = 0; i < num_fields; i++) if(fields[i]->type == tf_version || fields[i]->type == tf_version8)
	{
		fields[i]->offset = recordlen;
		recordlen += fields[i]->getlen();
	}
	// ����� ���� ��� ��������� ����
	for(i = 0; i < num_fields; i++) if(fields[i]->type != tf_version && fields[i]->type != tf_version8)
	{
		fields[i]->offset = recordlen;
		recordlen += fields[i]->getlen();
	}
	if(recordlen < 5) recordlen = 5; // ����� ����� ������ �� ����� ���� ������ 5 ���� (1 ���� �������, ��� ������ ��������, 4 ���� - ������ ��������� ��������� ��������� ������)

	if(!recordlen || !file_data) phys_numrecords = 0;
	else phys_numrecords = file_data->getlen() / recordlen;;

	if(file_data)
	{
		if(phys_numrecords * recordlen != file_data->getlen())
		{
			if(err) err->AddError(L"����� ������� �� ������ ����� ������.",
				L"����", tohex(block_descr),
				L"�������", name,
				L"����� �������", file_data->getlen(),
				L"����� ������", recordlen);
		}
	}
	else
	{
		if(err) err->AddError(L"����������� ���� ������ �������.",
			L"����", tohex(block_descr),
			L"�������", name);
			return;
	}

	// ������������� ������ �������
	for(i = 0; i < num_indexes; i++) indexes[i]->get_length();

	#ifdef _DEBUG
	if(err) err->AddDebugMessage(L"������� �������.", msInfo,
		L"�������", name,
		L"����� �������", file_data->getlen(),
		L"����� ������", recordlen);
	#endif

	bad = false;

}

//---------------------------------------------------------------------------
__fastcall table::table()
{
	init();
}

//---------------------------------------------------------------------------
void __fastcall table::deletefields()
{
	int i;
	if(fields)
	{
		for(i = 0; i < num_fields2; i++) delete fields[i];
		delete[] fields;
	}
}

//---------------------------------------------------------------------------
void __fastcall table::deleteindexes()
{
	int i;
	if(indexes)
	{
		for(i = 0; i < num_indexes; i++) delete indexes[i];
		delete[] indexes;
		indexes = NULL;
	}
}

//---------------------------------------------------------------------------
__fastcall table::~table()
{
	changed_rec* cr;
	changed_rec* cr2;
	for(cr = ch_rec; cr;)
	{
		cr2 = cr->next;
		delete cr;
		cr = cr2;
	}

	deletefields();
	deleteindexes();
	if(file_data)
	{
		delete file_data;
		file_data = NULL;
	}
	if(file_blob)
	{
		delete file_blob;
		file_blob = NULL;
	}
	if(file_index)
	{
		delete file_index;
		file_index = NULL;
	}
	if(descr_table)
	{
		delete descr_table;
		descr_table = NULL;
	}

	delete[] recordsindex;
}

//---------------------------------------------------------------------------
void __fastcall table::init()
{
	num_fields = 0;
	fields = NULL;
	num_indexes = 0;
	indexes = 0;
	recordlock = false;
	file_data = NULL;
	file_blob = NULL;
	file_index = NULL;
	lockinmemory = 0;

	recordsindex_complete = false;
	numrecords_review = 0;
	numrecords_found = 0;
	recordsindex = NULL;

#ifndef PublicRelease
	edit = false;
#endif //#ifdef PublicRelease
	ch_rec = NULL;
	added_numrecords = 0;

	phys_numrecords = 0;
	log_numrecords = 0;

}

//---------------------------------------------------------------------------
String __fastcall table::getname()
{
	return name;
}

//---------------------------------------------------------------------------
String __fastcall table::getdescription()
{
	return String((wchar_t*)descr_table->getdata(), descr_table->getlen() / 2);
}

//---------------------------------------------------------------------------
int __fastcall table::get_numfields()
{
	return num_fields;
}

//---------------------------------------------------------------------------
int __fastcall table::get_numindexes()
{
	return num_indexes;
}

//---------------------------------------------------------------------------
field* __fastcall table::getfield(int numfield)
{
	if(numfield >= num_fields)
	{
		if(err) err->AddError(L"������� ��������� ���� ������� �� ������, ������������ ���������� �����",
			L"�������", name,
			L"���������� �����", num_fields,
			L"����� ����", numfield + 1);
		return NULL;
	}
	return fields[numfield];
}

//---------------------------------------------------------------------------
index* __fastcall table::getindex(int numindex)
{
	if(numindex >= num_indexes)
	{
		if(err) err->AddError(L"������� ��������� ������� ������� �� ������, ������������ ���������� ��������",
			L"�������", name,
			L"���������� ��������", num_indexes,
			L"����� �������", numindex + 1);
		return NULL;
	}
	return indexes[numindex];
}

//---------------------------------------------------------------------------
unsigned int __fastcall table::get_phys_numrecords()
{
	return phys_numrecords;
}

//---------------------------------------------------------------------------
unsigned int __fastcall table::get_log_numrecords()
{
	return log_numrecords;
}

//---------------------------------------------------------------------------
void __fastcall table::set_log_numrecords(unsigned int _log_numrecords)
{
	log_numrecords = _log_numrecords;
}

//---------------------------------------------------------------------------
unsigned int __fastcall table::get_added_numrecords()
{
	return added_numrecords;
}

//---------------------------------------------------------------------------
char* __fastcall table::getrecord(unsigned int phys_numrecord, char* buf)
{
	#ifndef console
	#ifndef getcfname
	tr_syn->BeginWrite();
	#endif
	#endif
	char* b = file_data->getdata(buf, phys_numrecord * recordlen, recordlen);
	#ifndef console
	#ifndef getcfname
	tr_syn->EndWrite();
	#endif
	#endif
	return b;
}

//---------------------------------------------------------------------------
int __fastcall table::get_recordlen()
{
	return recordlen;
}

//---------------------------------------------------------------------------
bool __fastcall table::get_recordlock()
{
	return recordlock;
}

//---------------------------------------------------------------------------
v8object* __fastcall table::get_file_data()
{
	return file_data;
}

//---------------------------------------------------------------------------
v8object* __fastcall table::get_file_blob()
{
	return file_blob;
}

//---------------------------------------------------------------------------
v8object* __fastcall table::get_file_index()
{
	return file_index;
}

//---------------------------------------------------------------------------
void __fastcall table::set_lockinmemory(bool _lock)
{
	if(_lock)
	{
		lockinmemory++;
		if(descr_table) descr_table->set_lockinmemory(true);
		if(file_data) file_data->set_lockinmemory(true);
		if(file_blob) file_blob->set_lockinmemory(true);
		if(file_index) file_index->set_lockinmemory(true);
	}
	else
	{
		if(lockinmemory > 0) lockinmemory--;
		if(!lockinmemory)
		{
			if(descr_table) descr_table->set_lockinmemory(false);
			if(file_data) file_data->set_lockinmemory(false);
			if(file_blob) file_blob->set_lockinmemory(false);
			if(file_index) file_index->set_lockinmemory(false);
		}
	}
}

//---------------------------------------------------------------------------
// rewrite - �������������� ����� _str. ������ - �������������� (�� ���������), ���� - ����������
TStream* table::readBlob(TStream* _str, unsigned int _startblock, unsigned int _length, bool rewrite)
{
//	char* _b;
	unsigned int _curblock;
	char* _curb;
	unsigned short int _curlen;
	unsigned int _filelen, _numblock;
	unsigned int startlen;

	if(rewrite) _str->Size = 0;
	startlen = _str->Position;

	if(!_startblock && _length)
	{
		if(err) err->AddError(L"������� ������ �������� ����� ����� Blob",
			L"�������", name);
		return _str;
	}

	if(file_blob)
	{
		_filelen = file_blob->getlen();
		_numblock = _filelen >> 8;
		if(_numblock << 8 != _filelen) if(err)
			err->AddError(L"����� ����� Blob �� ������ 0x100",
				L"�������", name,
				L"����� �����", tohex(_filelen));

//		_b = file_blob->getdata();
		_curb = new char[0x100];
		_curblock = _startblock;
		while(_curblock)
		{
			if(_curblock >= _numblock)
			{
				if(err) err->AddError(L"������� ������ ����� ����� Blob �� ��������� �����",
					L"�������", name,
					L"����� ������", _numblock,
					L"�������� ����", _curblock);
				return _str;
			}
//			_curb = _b + (_curblock << 8);
			file_blob->getdata(_curb, _curblock << 8, 0x100);
			_curblock = *(unsigned int*)_curb;
			_curlen = *(unsigned short int*)(_curb + 4);
			if(_curlen > 0xfa)
			{
				if(err) err->AddError(L"������� ������ �� ����� ����� Blob ����� 0xfa ����",
					L"�������", name,
					L"������ �����", _curblock,
					L"�������� ����", _curlen);
				return _str;
			}
			_str->Write(_curb + 6, _curlen);

			if(_str->Size - startlen > _length) break; // ��������� ����� �� ���������� ���������� ������������
		}
		delete[] _curb;

		if(_str->Size - startlen != _length)
		{
			if(err) err->AddError(L"������������ ����� Blob-����, ���������� � ������, � ������ ����������� ����������� ������",
				L"�������", name,
				L"����� ����", _length,
				L"���������", _str->Size - startlen);
		}
	}
	else
	{
		if(err) err->AddError(L"������� ������ Blob-���� ��� �������������� ����� Blob",
			L"�������", name,
			L"����� ����", _length);
	}

	return _str;
}

//---------------------------------------------------------------------------
//bool __fastcall table::export_to_xml(String _filename, index* curindex, bool blob_to_file, bool unpack)
bool __fastcall table::export_to_xml(String _filename, bool blob_to_file, bool unpack)
{
	String* us;
	String s;
	String recname;
	//String fieldname;
	int i;
	unsigned int j, numr, nr;
	char* rec;
	bool dircreated = false;
	bool canwriteblob = false;
	String dir;
	index* curindex;
	int ic; // image count, ���������� ����� � ����� image
	int rc; // repeat count, ���������� �������� ����� ������ ������ (��� ������, ���� ������ �� ����������)

	char UnicodeHeader[2] = {0xff, 0xfe};
	String part1 = L"<?xml version=\"1.0\" encoding=\"UTF-16\"?>\r\n<!--���� ����������� ���������� Tool_1CD-->\r\n<Table Name=\"";
	String part2 = L"\">\r\n\t<Fields>\r\n";
	String part3 = L"\t</Fields>\r\n\t<Records>\r\n";
	String part4 = L"\t</Records>\r\n</Table>\r\n";

	String fpart1 = L"\t\t<Field Name=\"";
	String fpart2 = L"\" Type=\"";
	String fpart3 = L"\" Length=\"";
	String fpart4 = L"\" Precision=\"";
	String fpart5 = L"\"/>\r\n";

	String rpart1 = L"\t\t<Record>\r\n";
	String rpart2 = L"\t\t</Record>\r\n";
	String rpart3 = L"\t\t\t<";
	String status = L"������� ������� ";
	status += name;
	status += L" ";

	TFileStream* f = new TFileStream(_filename, fmCreate);

	f->Write(UnicodeHeader, 2);
	f->Write(part1.c_str(), part1.Length() * 2);
	f->Write(name.c_str(), name.Length() * 2);
	f->Write(part2.c_str(), part2.Length() * 2);

	if(num_indexes)
	{
		curindex = indexes[0];
		for(i = 0; i < num_indexes; i++) if(indexes[i]->get_is_primary())
		{
			curindex = indexes[i];
			break;
		}
	}
	else curindex = NULL;

	ic = 0;
	for(i = 0; i < num_fields; i++)
	{
		f->Write(fpart1.c_str(), fpart1.Length() * 2);
		us = &(fields[i]->name);
		f->Write(us->c_str(), us->Length() * 2);
		f->Write(fpart2.c_str(), fpart2.Length() * 2);
		s = fields[i]->get_presentation_type();
		f->Write(s.c_str(), s.Length() * 2);
		f->Write(fpart3.c_str(), fpart3.Length() * 2);
		s = fields[i]->getlength();
		f->Write(s.c_str(), s.Length() * 2);
		f->Write(fpart4.c_str(), fpart4.Length() * 2);
		s = fields[i]->getprecision();
		f->Write(s.c_str(), s.Length() * 2);
		f->Write(fpart5.c_str(), fpart5.Length() * 2);
		if(fields[i]->type == tf_image) ic++;
	}

	f->Write(part3.c_str(), part3.Length() * 2);

	if(curindex) numr = curindex->get_numrecords();
	else numr = numrecords_found;

	rec = new char[get_recordlen()];

	if(err) err->Status(status);

	recname = L"";
	rc = 0;
	for(j = 0; j < numr; j++)
	{
		if(j % 100 == 0 && j) if(err) err->Status(status + j);

		f->Write(rpart1.c_str(), rpart1.Length() * 2);
		if(curindex) nr = curindex->get_numrec(j);
		else nr = recordsindex[j];
		getrecord(nr, rec);
		if(ic){
			s = get_file_name_for_record(rec);
			if(s.CompareIC(recname) == 0) rc++;
			else
			{
				recname = s;
				rc = 0;
			}
		}
		for(i = 0; i < num_fields; i++)
		{
			f->Write(rpart3.c_str(), rpart3.Length() * 2);
			us = &(fields[i]->name);
			f->Write(us->c_str(), us->Length() * 2);
			f->Write(L">", 2);

			if(blob_to_file && fields[i]->type == tf_image)
			{
				if(!dircreated)
				{
					dircreated = true;
					dir = _filename + ".blob";
					if(!DirectoryExists(dir))
					{
						try
						{
							CreateDir(dir);
							dir += L"\\";
							canwriteblob = true;
						}
						catch(...)
						{
							if(err) err->AddMessage_(L"�� ������� ������� ������� blob", msWarning,
								L"�������", name,
								L"����", dir);
						}
					}
					else
					{
						dir += L"\\";
						canwriteblob = true;
					}
				}

				if(canwriteblob)
				{
					s = recname;
					if(ic > 1)
					{
						if(s.Length()) s += L"_";
						s += fields[i]->name;
                    }
					if(rc)
					{
						s += L"_";
						s += rc + 1;
					}
					if(!fields[i]->save_blob_to_file(rec, dir + s, unpack)) s = L"{NULL}";
				}
				else s = L"{ERROR}";
			}
			else s = fields[i]->get_XML_presentation(rec);

			f->Write(s.c_str(), s.Length() * 2);
			f->Write(L"</", 4);
			f->Write(us->c_str(), us->Length() * 2);
			f->Write(L">\r\n", 6);
		}
		f->Write(rpart2.c_str(), rpart2.Length() * 2);

	}
	f->Write(part4.c_str(), part4.Length() * 2);

	delete[] rec;
	delete f;
	if(err) err->Status(L"");
	return true;
}

//---------------------------------------------------------------------------
__int64 __fastcall table::get_fileoffset(unsigned int phys_numrecord)
{
	unsigned int _offset = phys_numrecord * recordlen;
	return file_data->get_fileoffset(_offset);
}

//---------------------------------------------------------------------------
bool __fastcall table::get_edit()
{
#ifndef PublicRelease
	return edit;
#else
	return false;
#endif //#ifdef PublicRelease
}
//---------------------------------------------------------------------------

#ifndef PublicRelease
void __fastcall table::begin_edit()
{
	if(edit) return;
	if(base->readonly)
	{
		if(err) err->AddMessage_(L"������� ����� � ����� �������������� � ������ \"������ ������\"", msWarning,
			L"�������", name);
		return;
	}
	edit = true;
}

//---------------------------------------------------------------------------
changed_rec_type __fastcall table::get_rec_type(unsigned int phys_numrecord)
{
	changed_rec* cr;
	if(!edit) return crt_not_changed;
	cr = ch_rec;
	while(cr)
	{
		if(cr->numrec == phys_numrecord) return cr->changed_type;
		cr = cr->next;
	}
	return crt_not_changed;
}

//---------------------------------------------------------------------------
changed_rec_type __fastcall table::get_rec_type(unsigned int phys_numrecord, int numfield)
{
	changed_rec* cr;
	if(!edit) return crt_not_changed;
	cr = ch_rec;
	while(cr)
	{
		if(cr->numrec == phys_numrecord)
		{
			if(cr->changed_type == crt_changed) return cr->fields[numfield] ? crt_changed : crt_not_changed;
			return cr->changed_type;
		}
		cr = cr->next;
	}
	return crt_not_changed;
}

//---------------------------------------------------------------------------
void __fastcall table::export_table(String path)
{
	TFileStream* f;
	String dir;
	export_import_table_root* root;

	dir = path + L"\\" + name;
	CreateDir(dir);
	dir += L"\\";

	root = new export_import_table_root;

	if(file_data)
	{
		root->has_data = true;
		root->data_version_1 = file_data->version.version_1;
		root->data_version_2 = file_data->version.version_2;
	}
	else root->has_data = false;

	if(file_blob)
	{
		root->has_blob = true;
		root->blob_version_1 = file_blob->version.version_1;
		root->blob_version_2 = file_blob->version.version_2;
	}
	else root->has_blob = false;

	if(file_index)
	{
		root->has_index = true;
		root->index_version_1 = file_index->version.version_1;
		root->index_version_2 = file_index->version.version_2;
	}
	else root->has_index = false;

	if(descr_table)
	{
		root->has_descr = true;
		root->descr_version_1 = descr_table->version.version_1;
		root->descr_version_2 = descr_table->version.version_2;
	}
	else root->has_descr = false;

	f = new TFileStream(dir + L"root", fmCreate);
	f->Write(root, sizeof(export_import_table_root));
	delete f;
	delete root;

	if(file_data) file_data->savetofile(dir + L"data");
	if(file_blob) file_blob->savetofile(dir + L"blob");
	if(file_index) file_index->savetofile(dir + L"index");
	if(descr_table) descr_table->savetofile(dir + L"descr");

}

//---------------------------------------------------------------------------
void __fastcall table::import_table(String path)
{
	String dir;

	dir = path + L"\\" + name;
	if(!DirectoryExists(dir))
	{
//		if(err) err->AddMessage_(L"���������� ������� ������� �� �������", msWarning,
//			L"�������", name,
//			L"����������", dir);
		return;
	}
	import_table2(dir);
}

//---------------------------------------------------------------------------
void __fastcall table::import_table2(String path)
{
	TFileStream* f;
	bool fopen;
	String dir;
	export_import_table_root* root;
	v8ob* ob;

	String str;
	char* buf;
	unsigned int i;

	bool descr_changed = false;

	if(!DirectoryExists(path))
	{
//		if(err) err->AddMessage_(L"���������� ������� ������� �� �������", msWarning,
//			L"�������", name,
//			L"����������", dir);
		return;
	}
	dir = path + L"\\";

	try
	{
		f = new TFileStream(dir + L"root", fmOpenRead);
	}
	catch(...)
	{
		if(err) err->AddMessage_(L"������ �������� ����� ������� ������� root", msWarning,
			L"�������", name,
			L"����", dir + L"root");
		return;
	}
	root = new export_import_table_root;
	f->Read(root, sizeof(export_import_table_root));
	delete f;

	if(root->has_data)
	{
		fopen = false;
		try
		{
			f = new TFileStream(dir + L"data", fmOpenRead);
			fopen = true;
		}
		catch(...)
		{
			if(err) err->AddMessage_(L"������ �������� ����� ������� ������� data", msWarning,
				L"�������", name,
				L"����", dir + L"data");
		}
		if(fopen)
		{
			if(!file_data){
				file_data = new v8object(base);
				descr_changed = true;
			}
			file_data->setdata(f);
			ob = (v8ob*)base->getblock_for_write(file_data->block, true);
			ob->version.version_1 = root->data_version_1;
			ob->version.version_2 = root->data_version_2;
			delete f;
		}
	}

	if(root->has_blob)
	{
		fopen = false;
		try
		{
			f = new TFileStream(dir + L"blob", fmOpenRead);
			fopen = true;
		}
		catch(...)
		{
			if(err) err->AddMessage_(L"������ �������� ����� ������� ������� blob", msWarning,
				L"�������", name,
				L"����", dir + L"blob");
		}
		if(fopen)
		{
			if(!file_blob){
				file_blob = new v8object(base);
				descr_changed = true;
			}
			file_blob->setdata(f);
			ob = (v8ob*)base->getblock_for_write(file_blob->block, true);
			ob->version.version_1 = root->blob_version_1;
			ob->version.version_2 = root->blob_version_2;
			delete f;
		}
	}

	if(root->has_index)
	{
		fopen = false;
		try
		{
			f = new TFileStream(dir + L"index", fmOpenRead);
			fopen = true;
		}
		catch(...)
		{
			if(err) err->AddMessage_(L"������ �������� ����� ������� ������� index", msWarning,
				L"�������", name,
				L"����", dir + L"index");
		}
		if(fopen)
		{
			if(!file_index){
				file_index = new v8object(base);
				descr_changed = true;
			}
			file_index->setdata(f);
			ob = (v8ob*)base->getblock_for_write(file_index->block, true);
			ob->version.version_1 = root->index_version_1;
			ob->version.version_2 = root->index_version_2;
			delete f;
		}
	}

	if(descr_changed) if(root->has_descr)
	{
		fopen = false;
		try
		{
			f = new TFileStream(dir + L"descr", fmOpenRead);
			fopen = true;
		}
		catch(...)
		{
			if(err) err->AddMessage_(L"������ �������� ����� ������� ������� descr", msWarning,
				L"����", dir + L"descr");
		}
		if(fopen)
		{
			if(!descr_table) descr_table = new v8object(base); // ������, ���� descr_table == NULL, �� ��� �������� ������!
			ob = (v8ob*)base->getblock_for_write(descr_table->get_block_number(), true);
			ob->version.version_1 = root->descr_version_1;
			ob->version.version_2 = root->descr_version_2;

			i = f->Size;
			buf = new char[i + 2];
			f->Read(buf, i);
			buf[i] =0;
			buf[i + 1] =0;
			str = String((wchar_t*)buf);
			delete[] buf;
			delete f;

			i = str.Pos(L"{\"Files\",");
			if(i == 0)
			{
				if(err) err->AddMessage_(L"������ ������ ������� Files � ����� ������� ������� descr", msWarning,
					L"����", dir + L"descr");
				delete root;
				return;
			}
			str.SetLength(i - 1);
			str += L"{\"Files\",";
			str += file_data ? String(file_data->get_block_number()) : String(L"0");
			str += L",";
			str += file_blob ? String(file_blob->get_block_number()) : String(L"0");
			str += L",";
			str += file_index ? String(file_index->get_block_number()) : String(L"0");
			str += L"}\n}";
			descr_table->setdata(str.c_str(), str.Length() * 2);

		}


	}

	base->flush();

	delete root;
}

//---------------------------------------------------------------------------
void __fastcall table::set_edit_value(unsigned int phys_numrecord, int numfield, bool null, String value, TStream* st)
{
	field* fld;
	char* rec;
	char* k;
	char* editrec;
	char* fldvalue;
	type_fields tf;
	bool changed;
	changed_rec* cr;
	changed_rec* cr2;
	int i, j;
	TStream** ost;

	if(!edit) return;

	fld = fields[numfield];
	tf = fld->gettype();
	if(tf == tf_version || tf == tf_version8) return;
	if(null && !fld->null_exists) return;

	rec = new char[recordlen];
	fldvalue = new char[fld->getlen()];

	if(tf == tf_string || tf == tf_text || tf == tf_image)
	{
		memset(fldvalue, 0, fld->getlen());
		k = fldvalue;
		if(fld->null_exists)
		{
			if(!null && st) *k = 1;
			k++;
		}
		*(TStream**)k = st;
	}
	else fld->get_bynary_value(fldvalue, null, value);

	changed = true;
	if(phys_numrecord < phys_numrecords)
	{
		getrecord(phys_numrecord, rec);
		changed = memcmp(rec + fld->offset, fldvalue, fld->len) != 0;
	}

	for(cr = ch_rec; cr; cr = cr->next) if(cr->numrec == phys_numrecord) break;
	if(!cr)
	{
		if(!changed)
		{
			delete[] rec;
			delete[] fldvalue;
			return; // �������� �� ����������, ������ �� ������
		}
		cr = new changed_rec(this, phys_numrecord >= phys_numrecords ? crt_insert : crt_changed, phys_numrecord);
		if(phys_numrecord <= phys_numrecords) memcpy(cr->rec, rec, recordlen);
	}

	editrec = cr->rec;

	if(cr->fields[numfield]) if(tf == tf_string || tf == tf_text || tf == tf_image)
	{
		ost = (TStream**)(editrec + fld->offset + (fld->getnull_exists() ? 1 : 0));
		if(*ost != st)
		{
			delete *ost;
			*ost = NULL;
		}
	}

	if(changed)
	{
		memcpy(editrec + fld->offset, fldvalue, fld->len);
		cr->fields[numfield] = 1;
	}
	else
	{
		cr->fields[numfield] = 0;
		j = 0;
		for(i = 0; i < num_fields; i++) j += cr->fields[i];
		if(j == 0)
		{
			// ���������� ����� ������ ���, ���� ������� ������ �� ����������
			if(ch_rec == cr) ch_rec = cr->next;
			else for(cr2 = ch_rec; cr2; cr2 = cr2->next) if(cr2->next == cr)
			{
				cr2->next = cr->next;
				break;
			}
			delete cr;
		}
		else memcpy(editrec + fld->offset, fldvalue, fld->len);
	}

	delete[] rec;
	delete[] fldvalue;
}

//---------------------------------------------------------------------------
void __fastcall table::restore_edit_value(unsigned int phys_numrecord, int numfield)
{
	field* fld;
	char* rec;
	changed_rec* cr;
	changed_rec* cr2;
	int i, j;
	type_fields tf;
	TStream** ost;

	if(phys_numrecord >= phys_numrecords) return;

	for(cr = ch_rec; cr; cr = cr->next) if(cr->numrec == phys_numrecord) break;
	if(!cr) return;
	if(cr->changed_type != crt_changed) return;

	fld = fields[numfield];
	tf = fld->gettype();
	if(cr->fields[numfield])
	{
		if(tf == tf_string || tf == tf_text || tf == tf_image)
		{
			ost = (TStream**)(cr->rec + fld->offset + (fld->getnull_exists() ? 1 : 0));
			delete *ost;
			*ost = NULL;
		}
	}

	cr->fields[numfield] = 0;
	j = 0;
	for(i = 0; i < num_fields; i++) j += cr->fields[i];
	if(j == 0)
	{
		// ���������� ����� ������ ���, ���� ������� ������ �� ����������
		if(ch_rec == cr) ch_rec = cr->next;
		else for(cr2 = ch_rec; cr2; cr2 = cr2->next) if(cr2->next == cr)
		{
			cr2->next = cr->next;
			break;
		}
		delete cr;
	}
	else{
		rec = new char[recordlen];
		getrecord(phys_numrecord, rec);
		memcpy(cr->rec + fld->offset, rec + fld->offset, fld->len);
		delete[] rec;
	}

}

//---------------------------------------------------------------------------
void __fastcall table::set_rec_type(unsigned int phys_numrecord, changed_rec_type crt)
{
	changed_rec* cr;
	changed_rec* cr2;

	for(cr = ch_rec; cr; cr = cr->next) if(cr->numrec == phys_numrecord) break;

	if(phys_numrecord < phys_numrecords)
	{
		switch(crt)
		{
			case crt_changed:
				if(err) err->AddError(L"������� ������ ��������� �������� \"��������\" ������������ ������ �������",
					L"�������", name,
					L"���������� ����� ������", phys_numrecord);
				break;
			case crt_insert:
				if(err) err->AddError(L"������� ������ ��������� �������� \"���������\" ������������ ������ �������",
					L"�������", name,
					L"���������� ����� ������", phys_numrecord);
			case crt_delete:
				if(cr)
				{
					cr->clear();
					delete[] cr->rec;
					delete[] cr->fields;
					cr->changed_type = crt;
				}
				else new changed_rec(this, crt, phys_numrecord);
				break;
			case crt_not_changed:
				if(cr)
				{
					if(ch_rec == cr) ch_rec = cr->next;
					else for(cr2 = ch_rec; cr2; cr2 = cr2->next) if(cr2->next == cr)
					{
						cr2->next = cr->next;
						break;
					}
					delete cr;
				}
				break;
		}
	}
	else
	{
		switch(crt)
		{
			case crt_changed:
				if(err) err->AddError(L"������� ������ ��������� �������� \"��������\" ����������� ������ �������",
					L"�������", name,
					L"���������� ����� ������", phys_numrecord);
				break;
			case crt_insert:
				if(cr) cr->changed_type = crt;
				else
				{
					if(phys_numrecord > phys_numrecords + added_numrecords)
					{
						if(err) err->AddError(L"������� ����������� ������ �������, � ������� ������ �������������",
							L"�������", name,
							L"������������ ����� ������", phys_numrecords + added_numrecords,
							L"���������� ����� ������", phys_numrecord);
					}
					else
					{
						new changed_rec(this, crt, phys_numrecord);
						added_numrecords++;
					}
				}
				break;
			case crt_delete:
				if(cr)
				{
					if(ch_rec == cr) ch_rec = cr->next;
					else for(cr2 = ch_rec; cr2; cr2 = cr2->next) if(cr2->next == cr)
					{
						cr2->next = cr->next;
						break;
					}
					delete cr;
					added_numrecords--;
				}
				for(cr = ch_rec; cr; cr = cr->next) if(cr->numrec > phys_numrecord) cr->numrec--;
				break;
			case crt_not_changed:
				if(err) err->AddError(L"������� ������ ��������� �������� \"�� ��������\" ����������� ������ �������",
					L"�������", name,
					L"���������� ����� ������", phys_numrecord);
				break;
		}
	}
}
#endif //#ifdef PublicRelease

//---------------------------------------------------------------------------
char* __fastcall table::get_edit_record(unsigned int phys_numrecord, char* rec)
{
	changed_rec* cr;
	for(cr = ch_rec; cr; cr = cr->next) if(phys_numrecord == cr->numrec)
	{
		if(cr->changed_type != crt_delete)
		{
			memcpy(rec, cr->rec, recordlen);
			return rec;
		}
		break;
	}
	return getrecord(phys_numrecord, rec);
}

//---------------------------------------------------------------------------
unsigned int __fastcall table::get_phys_numrec(int ARow, index* cur_index)
{
	unsigned int numrec;
//	unsigned int numrecords;

	if(ARow == 0)
	{
		if(err) err->AddError(L"������� ��������� ������ ���������� ������ �� �������� ������ ������.",
			L"�������", name);
		return 0;
	}

//	if(cur_index) numrecords = cur_index->get_numrecords();
//	else numrecords = numrecords_found;

#ifndef PublicRelease
	if(edit)
	{
		if((unsigned int)ARow > log_numrecords + added_numrecords)
		{
			if(err) err->AddError(L"������� ��������� ������ ���������� ������ �� ������ ������, ������������ ���������� �������",
				L"�������", name,
				L"���������� ���������� �������", log_numrecords,
				L"���������� ����������� �������", added_numrecords,
				L"����� ������", ARow);
			return 0;
		}
		if((unsigned int)ARow > log_numrecords) return ARow - 1 - log_numrecords + phys_numrecords;
	}
#endif //#ifdef PublicRelease

	if((unsigned int)ARow > log_numrecords)
	{
		if(err) err->AddError(L"������� ��������� ������ ���������� ������ �� ������ ������, ������������ ���������� �������",
			L"�������", name,
			L"���������� ���������� �������", log_numrecords,
			L"����� ������", ARow);
		return 0;
	}
	if(cur_index) numrec = cur_index->get_numrec(ARow - 1);
	else
	{
		#ifndef console
		#ifndef getcfname
		tr_syn->BeginRead();
		#endif
		#endif
		numrec = recordsindex[ARow - 1];
		#ifndef console
		#ifndef getcfname
		tr_syn->EndRead();
		#endif
		#endif
	}

	return numrec;
}

//---------------------------------------------------------------------------
void __fastcall table::create_file_data()
{
	if(!file_data) return;
	//if(!edit) return;
	file_data = new v8object(base);
	refresh_descr_table();
}

//---------------------------------------------------------------------------
void __fastcall table::create_file_blob()
{
	if(!file_blob) return;
	//if(!edit) return;
	file_blob = new v8object(base);
	refresh_descr_table();
}

//---------------------------------------------------------------------------
void __fastcall table::create_file_index()
{
	if(!file_index) return;
	//if(!edit) return;
	file_index = new v8object(base);
	refresh_descr_table();

}

//---------------------------------------------------------------------------
void __fastcall table::refresh_descr_table()
{
	if(err) err->AddError(L"������� ���������� ����� �������� �������.",
		L"�������", name);
	return;

}

#ifndef PublicRelease
//---------------------------------------------------------------------------
void __fastcall table::delete_data_record(unsigned int phys_numrecord)
{
	char* rec;
	int first_empty_rec;

	if(!edit)
	{
		if(err) err->AddError(L"������� �������� ������ �� ����� file_data �� � ������ ��������������.",
			L"�������", name,
			L"������ ��������� ������", phys_numrecord);
		return;
	}

	if(!file_data)
	{
		if(err) err->AddError(L"������� �������� ������ �� ��������������� ����� file_data.",
			L"�������", name,
			L"������ ��������� ������", phys_numrecord);
		return;
	}

	if(phys_numrecord >= phys_numrecords)
	{
		if(err) err->AddError(L"������� �������� ������ � ����� file_data �� ��������� �����.",
			L"�������", name,
			L"����� �������", phys_numrecords,
			L"������ ��������� ������", phys_numrecord);
		return;
	}

	if(!phys_numrecord)
	{
		if(err) err->AddError(L"������� �������� ������� ������ � ����� file_data.",
			L"�������", name,
			L"����� �������", phys_numrecords);
		return;
	}

	if(phys_numrecord == phys_numrecords - 1)
	{
		file_data->set_len(phys_numrecord * recordlen);
		phys_numrecords--;
	}
	else
	{
		rec = new char[recordlen];
		memset(rec, 0, recordlen);
		file_data->getdata(&first_empty_rec, 0, 4);
		*((int*)rec) = first_empty_rec;
		file_data->setdata(&first_empty_rec, 0, 4);
		write_data_record(phys_numrecord, rec);
		delete[] rec;
	}

}

//---------------------------------------------------------------------------
void __fastcall table::delete_blob_record(unsigned int blob_numrecord)
{
	int prev_free_first;
	int i, j;

	if(!edit)
	{
		if(err) err->AddError(L"������� �������� ������ �� ����� file_blob �� � ������ ��������������.",
			L"�������", name,
			L"�������� ��������� ������", blob_numrecord << 8);
		return;
	}

	if(!file_blob)
	{
		if(err) err->AddError(L"������� �������� ������ �� ��������������� ����� file_blob.",
			L"�������", name,
			L"�������� ��������� ������", blob_numrecord << 8);
		return;
	}

	if(blob_numrecord << 8 >= file_blob->getlen())
	{
		if(err) err->AddError(L"������� �������� ������ � ����� file_blob �� ��������� �����.",
			L"�������", name,
			L"�������� ��������� ������", blob_numrecord << 8,
			L"����� �����", file_blob->getlen());
		return;
	}

	if(blob_numrecord == 0)
	{
		if(err) err->AddError(L"������� �������� ������� ������ � ����� file_blob.",
			L"�������", name,
			L"����� �����", file_blob->getlen());
		return;
	}

	file_blob->getdata(&prev_free_first, 0, 4); // ������ ���������� ������ ��������� ������
	for(i = blob_numrecord; i; file_blob->getdata(&i, i << 8, 4)) j = i; // ���� ��������� ���� � ������� ���������
	file_blob->setdata(&prev_free_first, j << 8, 4); // ������������� � ����� ������� ��������� ������ ������ ������ ������� ��������� ������
	file_blob->setdata(&blob_numrecord, 0, 4); // ������������� ����� ������ ������� ��������� ������ �� ������ ��������� ������
}

//---------------------------------------------------------------------------
void __fastcall table::delete_index_record(unsigned int phys_numrecord)
{
	char* rec;

	rec = new char[recordlen];
	getrecord(phys_numrecord, rec);
	delete_index_record(phys_numrecord, rec);
	delete[] rec;
}

//---------------------------------------------------------------------------
void __fastcall table::delete_index_record(unsigned int phys_numrecord, char* rec)
{
	index* ind;
	int i;

	if(*rec)
	{
		if(err) err->AddError(L"������� �������� ������� ��������� ������.",
			L"�������", name,
			L"���������� ����� ������", phys_numrecord);
		return;
	}

	for(i = 0; i < num_indexes; i++) indexes[i]->delete_index(rec, phys_numrecord);
}

//---------------------------------------------------------------------------
void __fastcall table::write_data_record(unsigned int phys_numrecord, char* rec)
{
	char* b;

	if(!edit)
	{
		if(err) err->AddError(L"������� ������ � ���� file_data �� � ������ ��������������.",
			L"�������", name,
			L"������ ������������ ������", phys_numrecord);
		return;
	}

	if(!file_data) create_file_data();

	if(phys_numrecord > phys_numrecords && !(phys_numrecord == 1 && phys_numrecords == 0))
	{
		if(err) err->AddError(L"������� ������ � ���� file_data �� ��������� �����.",
			L"�������", name,
			L"����� �������", phys_numrecords,
			L"������ ������������ ������", phys_numrecord);
		return;
	}

	if(!phys_numrecord)
	{
		if(err) err->AddError(L"������� ������ ������� ������ � ���� file_data.",
			L"�������", name,
			L"����� �������", phys_numrecords);
		return;
	}

	if(phys_numrecords == 0)
	{
		b = new char[recordlen];
		memset(b, 0, recordlen);
		b[0] = 1;
		file_data->setdata(b, 0, recordlen);
		delete[] b;
	}
	file_data->setdata(rec, phys_numrecord * recordlen, recordlen);
}

//---------------------------------------------------------------------------
unsigned int __fastcall table::write_blob_record(char* blob_record, unsigned int blob_len)
{
	unsigned int cur_block, cur_offset, prev_offset, first_block, next_block;
	unsigned short int cur_len;
	unsigned int zero;

	if(!edit)
	{
		if(err) err->AddError(L"������� ������ � ���� file_blob �� � ������ ��������������.",
			L"�������", name);
		return 0;
	}

	if(!blob_len) return 0;

	if(!file_blob)
	{
		create_file_blob();
		char* b = new char[256];
		memset(b, 0, 256);
		file_blob->setdata(b, 0, 256);
		delete[] b;
	}

	file_blob->getdata(&first_block, 0, 4);
	if(!first_block) first_block = file_blob->getlen() >> 8;
	prev_offset = 0;

	for(cur_block = first_block; blob_len; blob_len -= cur_len, cur_block = next_block, blob_record += cur_len)
	{
		cur_len = min(blob_len, 250);
		if(cur_len < 250) memset(blob_record, 0, 250);

		if(prev_offset) file_blob->setdata(&cur_block, prev_offset, 4);

		cur_offset = cur_block << 8;
		next_block = 0;
		if(cur_offset < file_blob->getlen()) file_blob->getdata(&next_block, cur_offset, 4);
		if(!next_block) next_block = (file_blob->getlen() >> 8) + 1;

		file_blob->setdata(&zero, cur_offset, 4);
		file_blob->setdata(&cur_len, cur_offset + 4, 2);
		file_blob->setdata(blob_record, cur_offset + 6, 250);

		prev_offset = cur_offset;
	}
	if(next_block << 8 < file_blob->getlen()) file_blob->setdata(&next_block, 0, 4);
	else file_blob->setdata(&zero, 0, 4);

	return first_block;
}

//---------------------------------------------------------------------------
unsigned int __fastcall table::write_blob_record(TStream* bstr)
{
	unsigned int cur_block, cur_offset, prev_offset, first_block, next_block;
	unsigned short int cur_len;
	unsigned int blob_len;
	unsigned int zero;
	char blob_record[256];

	if(!edit)
	{
		if(err) err->AddError(L"������� ������ � ���� file_blob �� � ������ ��������������.",
			L"�������", name);
		return 0;
	}

	blob_len = bstr->Size;
	if(!blob_len) return 0;
	bstr->Seek(0, soFromBeginning);
	zero = 0;

	if(!file_blob)
	{
		create_file_blob();
		memset(blob_record, 0, 256);
		file_blob->setdata(blob_record, 0, 256);
	}

	file_blob->getdata(&first_block, 0, 4);
	if(!first_block) first_block = file_blob->getlen() >> 8;
	prev_offset = 0;

	for(cur_block = first_block; blob_len; blob_len -= cur_len, cur_block = next_block)
	{
		cur_len = min(blob_len, 250);
		if(cur_len < 250) memset(blob_record, 0, 250); //-V512
		bstr->Read(blob_record, cur_len);

		if(prev_offset) file_blob->setdata(&cur_block, prev_offset, 4);

		cur_offset = cur_block << 8;
		next_block = 0;
		if(cur_offset < file_blob->getlen()) file_blob->getdata(&next_block, cur_offset, 4);
		if(!next_block) next_block = (file_blob->getlen() >> 8) + 1;


		file_blob->setdata(&zero, cur_offset, 4);
		file_blob->setdata(&cur_len, cur_offset + 4, 2);
		file_blob->setdata(blob_record, cur_offset + 6, 250);

		prev_offset = cur_offset;
	}
	if(next_block << 8 < file_blob->getlen()) file_blob->setdata(&next_block, 0, 4);
	else file_blob->setdata(&zero, 0, 4);

	return first_block;
}

//---------------------------------------------------------------------------

void __fastcall table::write_index_record(const unsigned int phys_numrecord, const char* rec)
{
	char* index_buf;
	index* ind;
	int i;

	if(*rec)
	{
		if(err) err->AddError(L"������� ������ �������� ���������� �� �������� ������.",
			L"�������", name);
		return;
	}

	for(i = 0; i < num_indexes; i++) indexes[i]->write_index(phys_numrecord, rec);

}

//---------------------------------------------------------------------------
void __fastcall table::cancel_edit()
{
	changed_rec* cr;
	changed_rec* cr2;
	for(cr = ch_rec; cr;)
	{
		cr2 = cr->next;
		delete cr;
		cr = cr2;
	}

	edit = false;
	ch_rec = NULL;
	added_numrecords = 0;
}

//---------------------------------------------------------------------------
void __fastcall table::end_edit()
{
	changed_rec* cr;

	// ������� ��������� ������
	for(cr = ch_rec; cr; cr = cr->next) if(cr->changed_type == crt_delete) delete_record(cr->numrec);

	// ���������� ���������� ������
	for(cr = ch_rec; cr; cr = cr->next) if(cr->changed_type == crt_changed) update_record(cr->numrec, cr->rec, cr->fields);

	// ��������� ����� ������
	for(cr = ch_rec; cr; cr = cr->next) if(cr->changed_type == crt_insert) insert_record(cr->rec);

	cancel_edit();
	base->flush();
}

//---------------------------------------------------------------------------
void __fastcall table::delete_record(unsigned int phys_numrecord)
{
	int i;
	unsigned int j;
	field* f;
	type_fields tf;
	char* rec;

	rec = new char[recordlen];
	getrecord(phys_numrecord, rec);

	delete_index_record(phys_numrecord, rec);

	for(i = 0; i < num_fields; i++)
	{
		f = fields[i];
		tf = f->type;
		if(tf == tf_image || tf == tf_string || tf == tf_text)
		{
			j = *(unsigned int*)(rec + f->offset);
			if(j) delete_blob_record(j);
		}
	}

	delete_data_record(phys_numrecord);

	delete[] rec;
}

//---------------------------------------------------------------------------
void __fastcall table::insert_record(char* rec)
{
	int i, offset;
	char* j;
	field* f;
	type_fields tf;
	unsigned int phys_numrecord;
	unsigned int k, l;
	_version ver;
	TStream** st;

	if(!file_data) create_file_data();

	for(i = 0; i < num_fields; i++)
	{
		f = fields[i];
		tf = f->type;
		offset = f->offset + (f->getnull_exists() ? 1 : 0);
		switch(tf)
		{
			case tf_image:
			case tf_string:
			case tf_text:
				st = (TStream**)(rec + offset);
				if(*st)
				{
					l = (*st)->Size;
					k = write_blob_record(*st);
					delete *st;
					*st = NULL;
				}
				else{
					k = 0;
					l = 0;
				}
				*(unsigned int*)(rec + offset) = k;
				*(unsigned int*)(rec + offset + 4) = l;
				if(f->getnull_exists()) *(rec + f->offset) = l ? 1 : 0;
			case tf_version:
				file_data->get_version_rec_and_increase(&ver);
				memcpy(rec + offset, &ver, 8);
				memcpy(rec + offset + 8, &ver, 8);
				break;
			case tf_version8:
				file_data->get_version_rec_and_increase(&ver);
				memcpy(rec + offset, &ver, 8);
				break;
		}
	}

	if(file_data->len == 0)
	{
		j = new char[recordlen];
		memset(j, 0, recordlen);
		*j = 1;
		file_data->setdata(j, 0, recordlen);
		delete[] j;
		phys_numrecord = 1;
	}
	else
	{
		file_data->getdata(&phys_numrecord, 1, 4);
		if(phys_numrecord)
		{
			file_data->getdata(&k, phys_numrecord * recordlen + 1, 4);
			file_data->setdata(&k, 1, 4);
		}
		else phys_numrecord = file_data->len / recordlen;
	}

	write_data_record(phys_numrecord, rec);
	write_index_record(phys_numrecord, rec);

}

//---------------------------------------------------------------------------
void __fastcall table::update_record(unsigned int phys_numrecord, char* rec, char* changed_fields)
{
	char* orec;
	int i, offset;
	field* f;
	type_fields tf;
	unsigned int k, l;
	_version ver;
	TStream** st;

	orec = new char[recordlen];
	getrecord(phys_numrecord, orec);
	delete_index_record(phys_numrecord, orec);

	for(i = 0; i < num_fields; i++)
	{
		f = fields[i];
		tf = f->type;
		offset = f->offset + (f->getnull_exists() ? 1 : 0);
		if(changed_fields[i])
		{
			if(tf == tf_image || tf == tf_string || tf == tf_text)
			{
				if(f->getnull_exists())
				{
					if(*(orec + f->offset))
					{
						k = *(unsigned int*)(orec + offset);
						if(k) delete_blob_record(k);
					}
					if(*(rec + f->offset))
					{
						st = (TStream**)(rec + offset);
						if(*st)
						{
							l = (*st)->Size;
							k = write_blob_record(*st);
							delete *st;
							*st = NULL;
							*(orec + f->offset) = 1;
						}
						else{
							k = 0;
							l = 0;
							*(orec + f->offset) = 0;
						}
					}
					else{
						k = 0;
						l = 0;
						*(orec + f->offset) = 0;
					}
				}
				else
				{
					k = *(unsigned int*)(orec + offset);
					if(k) delete_blob_record(k);

					st = (TStream**)(rec + offset);
					if(*st)
					{
						l = (*st)->Size;
						k = write_blob_record(*st);
						delete *st;
						*st = NULL;
					}
					else{
						k = 0;
						l = 0;
					}
				}
				*(unsigned int*)(orec + offset) = k;
				*(unsigned int*)(orec + offset + 4) = l;
			}
			else memcpy(orec + f->offset, rec + f->offset, f->len);
		}
		else
		{
			if(tf == tf_version)
			{
				file_data->get_version_rec_and_increase(&ver);
				memcpy(orec + offset + 8, &ver, 8);
			}
			else if(tf == tf_version8)
			{
				file_data->get_version_rec_and_increase(&ver);
				memcpy(orec + offset, &ver, 8);
			}
		}

	}

	write_index_record(phys_numrecord, orec);
	write_data_record(phys_numrecord, orec);
	delete[] orec;

}

//---------------------------------------------------------------------------
// �������� ������ �������� ������ (������, ���������� ��� ������� ����� ������ �� 256 ����, ���������� 0, ���� �������� �� �������� � 1, ���� ���������)
char* __fastcall table::get_record_template_test()
{
	int len;
	char* res;
	char* curp;
	int i, j, l;
	field* f;
	bool required;

	len = recordlen << 8;
	res = new char[len];
	memset(res, 0, len);

	for(i = 0; i < num_fields; i++)
	{
		required = false;
		f = fields[i];
		curp = res + (f->getoffset() << 8);

		if(f->getnull_exists())
		{
			curp[0] = 1;
			curp[1] = 1;
			curp += 256;
		}

		l = f->getlength();
		switch(f->gettype())
		{
			case tf_binary: // B // ����� = length
				memset(curp, 1, 256 * l);
				break;
			case tf_bool: // L // ����� = 1
				curp[0] = 1;
				curp[1] = 1;
				break;
			case tf_numeric: // N // ����� = (length + 2) / 2
				j = (l + 2) / 2;
				for(; j > 0; --j)
				{
					memcpy(curp, NUM_TEST_TEMPLATE, 256);
					curp += 256;
				}
				break;
			case tf_char: // NC // ����� = length * 2
				memset(curp, 1, 256 * 2 * l);
				break;
			case tf_varchar: // NVC // ����� = length * 2 + 2
				if(l > 255) j = 256;
				else j = l + 1;
				memset(curp, 1, j);
				//curp[0x20] = 1;
				curp += 256;
				j = (l >> 8) + 1;
				memset(curp, 1, j);
				curp += 256;
				memset(curp, 1, 256 * 2 * l);
				break;
			case tf_version: // RV // 16, 8 ������ �������� � 8 ������ ����������� ? ������ ������ int(���������) + int(����������������)
				memset(curp, 1, 256 * 16);
				break;
			case tf_string: // NT // 8 (unicode text)
				memset(curp, 1, 256 * 8);
				break;
			case tf_text: // T // 8 (ascii text)
				memset(curp, 1, 256 * 8);
				break;
			case tf_image: // I // 8 (image = bynary data)
				memset(curp, 1, 256 * 8);
				break;
			case tf_datetime: // DT //7
				if(f->getname().CompareIC(L"_DATE_TIME") == 0) required = true;
				else if(f->getname().CompareIC(L"_NUMBERPREFIX") == 0) required = true;

				memcpy(curp, DATE1_TEST_TEMPLATE, 256);
				curp += 256;
				memcpy(curp, NUM_TEST_TEMPLATE, 256);
				curp += 256;
				memcpy(curp, DATE3_TEST_TEMPLATE, 256);
				if(required) curp[0] = 0;
				curp += 256;
				memcpy(curp, DATE4_TEST_TEMPLATE, 256);
				if(required) curp[0] = 0;
				curp += 256;
				memcpy(curp, DATE5_TEST_TEMPLATE, 256);
				curp += 256;
				memcpy(curp, DATE67_TEST_TEMPLATE, 256);
				curp += 256;
				memcpy(curp, DATE67_TEST_TEMPLATE, 256);
				break;
			case tf_version8: // 8, ������� ���� ��� recordlock == false � ���������� ���� ���� tf_version
				memset(curp, 1, 256 * 8);
				break;
			case tf_varbinary: // VB // ����� = length + 2
				if(l > 255) j = 256;
				else j = l + 1;
				memset(curp, 1, j);
				curp += 256;
				j = (l >> 8) + 1;
				memset(curp, 1, j);
				curp += 256;
				memset(curp, 1, 256 * l);
				break;
		}
	}

	res[0] = 1;
//	res[1] = 1;
//	memset(res + 256, 1, 256 * 4);

	return res;
}

#endif //#ifdef PublicRelease

//---------------------------------------------------------------------------
// ��������� recordsindex �� �����������
void __fastcall table::fillrecordsindex()
{
	unsigned int i;
	int j;
	char* rec;

    if(recordsindex_complete) return;
    recordsindex = new unsigned int [phys_numrecords];
	rec = new char[recordlen];

	j = 0;
	for(i = 0; i < phys_numrecords; i++)
	{
		getrecord(i, rec);
		if(*rec) continue;
		recordsindex[j++] = i;
	}
	recordsindex_complete = true;
	numrecords_review = phys_numrecords;
	numrecords_found = j;
	log_numrecords = j;

	delete[] rec;
}

String __fastcall table::get_file_name_for_field(int num_field, char* rec, unsigned int numrec)
{
	String s(L"");
	int i;
	index* ind;

	if(num_indexes)
	{
		ind = indexes[0];
		for(i = 0; i < num_indexes; i++) if(indexes[i]->is_primary)
		{
			ind = indexes[i];
			break;
		}
		for(i = 0; i < ind->num_records; i++)
		{
			if(s.Length()) s += "_";
			s += ind->records[i].field->get_XML_presentation(rec);
		}
		if(!ind->is_primary && numrec){
			s += "_";
			s += numrec;
		}
	}
	if(!issystem || !(name.CompareIC(L"CONFIG") == 0 || name.CompareIC(L"CONFIGSAVE") == 0 || name.CompareIC(L"FILES") == 0 || name.CompareIC(L"PARAMS") == 0))
	{
		if(s.Length()) s += "_";
		s += fields[num_field]->getname();
	}

	return s;
}

String __fastcall table::get_file_name_for_record(char* rec)
{
	String s(L"");
	int i;
	index* ind;

	if(num_indexes)
	{
		ind = indexes[0];
		for(i = 0; i < num_indexes; i++) if(indexes[i]->is_primary)
		{
			ind = indexes[i];
			break;
		}
		for(i = 0; i < ind->num_records; i++)
		{
			if(s.Length()) s += "_";
			s += ind->records[i].field->get_XML_presentation(rec);
		}
	}

	return s;
}

//---------------------------------------------------------------------------

//********************************************************
// ����� T_1CD

//---------------------------------------------------------------------------
bool __fastcall T_1CD::getblock(void* buf, unsigned int block_number, int blocklen)
{
	if(!fs) return false;
	if(block_number >= length)
	{
		if(err) err->AddError(L"������� ������ ����� �� ��������� �����.",
			L"������ �����", tohex(block_number),
			L"����� ������", tohex(length));
		return false;
	}

	memcpy(buf, memblock::getblock(fs, block_number), blocklen);
	return true;
}

//---------------------------------------------------------------------------
char*  __fastcall T_1CD::getblock(unsigned int block_number)
{
	if(!fs) return NULL;
	if(block_number >= length)
	{
		if(err) err->AddError(L"������� ������ ����� �� ��������� �����.",
			L"������ �����", tohex(block_number),
			L"����� ������", tohex(length));
		return NULL;
	}

	return memblock::getblock(fs, block_number);
}

//---------------------------------------------------------------------------
char*  __fastcall T_1CD::getblock_for_write(unsigned int block_number, bool read)
{
	v8con* bc;


	if(!fs) return NULL;
	if(block_number > length)
	{
		if(err) err->AddError(L"������� ��������� ����� �� ��������� ����� ����.",
			L"������ �����", tohex(block_number),
			L"����� ������", tohex(length));
		return NULL;
	}

	if(block_number == length)
	{
		length++;
		fs->Size += 0x1000;
		bc = (v8con*)getblock_for_write(0, true);
		bc->length = length;
	}

	return memblock::getblock_for_write(fs, block_number, read);
}

//---------------------------------------------------------------------------
int __fastcall T_1CD::get_numtables()
{
	return num_tables;
}

//---------------------------------------------------------------------------
table* __fastcall T_1CD::gettable(int numtable)
{
	if(numtable >= num_tables)
	{
		if(err) err->AddError(L"������� ��������� ������� �� ������, ������������ ���������� ������",
			L"���������� ������", num_tables,
			L"����� �������", numtable + 1);
		return NULL;
	}
	return tables[numtable];
}

//---------------------------------------------------------------------------
void __fastcall T_1CD::init()
{
	err = NULL;
	filename = "";
	fs = NULL;
	free_blocks = NULL;
	root_object = NULL;
	tables = NULL;
	num_tables = 0;
//	ibtype = tt_unknown;
	table_config = NULL;
	table_configsave = NULL;
	table_params = NULL;
	table_files = NULL;
	table_dbschema = NULL;
	table_configcas = NULL;
	table_configcassave = NULL;
	table__extensionsinfo = NULL;

	_files_config = NULL;
	_files_configsave = NULL;
	_files_params = NULL;
	_files_files = NULL;
	_files_configcas = NULL;
	_files_configcassave = NULL;

	cs_config = NULL;
	cs_configsave = NULL;

	table_depot = NULL;
	table_users = NULL;
	table_objects = NULL;
	table_versions = NULL;
	table_labels = NULL;
	table_history = NULL;
	table_lastestversions = NULL;
	table_externals = NULL;
	table_selfrefs = NULL;
	table_outrefs = NULL;

	supplier_configs_defined = false;
	locale = NULL;

	is_infobase = false;
	is_depot = false;
	field::showGUIDasMS = false;

}

//---------------------------------------------------------------------------
__fastcall T_1CD::T_1CD()
{
	init();
}

//---------------------------------------------------------------------------
__fastcall T_1CD::~T_1CD()
{
	err = NULL;
	//filename = "";
	if(free_blocks)
	{
		delete free_blocks;
		free_blocks = NULL;
	}

	if(root_object)
	{
		delete root_object;
		root_object = NULL;
	}

	delete cs_config;
	delete cs_configsave;

	delete _files_config;
	delete _files_configsave;
	delete _files_params;
	delete _files_files;
	delete _files_configcas;
	delete _files_configcassave;

	if(tables)
	{
		for(int i = 0; i < num_tables; i++) delete tables[i];
		delete[] tables;
		tables = NULL;
	}
	num_tables = 0;

	// ������� ��������� ������������ ����� (���������� ����� ���������� ���� � ����) ...
	memblock::delete_memblocks();

	// ... � ������ ����� ��������� ���� ����.
	if(fs)
	{
		delete fs;
		fs = NULL;
	}

	delete[] locale;

}

//---------------------------------------------------------------------------
__fastcall T_1CD::T_1CD(String _filename, MessageRegistrator* _err, bool _monopoly)
{
	char* b;
	unsigned int* table_blocks;
	int i, j;
#ifndef PublicRelease
	ICU_Result icu_res;
#endif //#ifdef PublicRelease

	init();

	err = _err;
	filename = System::Ioutils::TPath::GetFullPath(_filename);

	try
	{
		if(_monopoly) fs = new TFileStream(filename, fmOpenReadWrite | fmShareDenyWrite);
		else fs = new TFileStream(filename, fmOpenRead | fmShareDenyNone);
		//fs = new TFileStream(filename, fmOpenRead);
	}
	catch(...)
	{
		if(err) err->AddError(L"������ �������� ����� ���� (���� ������ ������ ����������?)");
		fs = NULL;
		return;
	}

	length = fs->Size >> 12;
	if((__int64)length << 12 != fs->Size)
	{
		if(err) err->AddError(L"����� ����� ���� �� ������ ����� ����� (0x1000)",
			L"����� �����", tohex(fs->Size));
		delete fs;
		fs = NULL;
		return;
	}

	memblock::create_memblocks(length);

	v8con* cont = new v8con;
	getblock(cont, 0, sizeof(v8con));

	if(memcmp(&(cont->sig), SIG_CON, 8) != 0)
	{
		if(err) err->AddError(L"���� �� �������� ����� 1� (��������� �� ����� \"1CDBMSV8\")");
		delete fs;
		fs = NULL;
		delete cont;
		return;
	}

	ver = cont->getver();
	if(ver == L"8.0.3.0"){
		version = ver8_0_3_0;
		readonly = true;
	}
	else if(ver == L"8.0.5.0"){
		version = ver8_0_5_0;
		readonly = true;
	}
	else if(ver == L"8.1.0.0")
	{
		version = ver8_1_0_0;
		#ifdef PublicRelease
		readonly = true;
		#else
		readonly = !_monopoly;
		#endif
	}
	else if(ver == L"8.2.0.0")
	{
		version = ver8_2_0_0;
		#ifdef PublicRelease
		readonly = true;
		#else
		readonly = !_monopoly;
		#endif
	}
	else if(ver == L"8.2.14.0")
	{
		version = ver8_2_14_0;
		#ifdef PublicRelease
		readonly = true;
		#else
		readonly = !_monopoly;
		#endif
	}
	else
	{
		if(err) err->AddError(L"���������������� ������ ���� 1�",
			L"������ ����", ver);
		delete fs;
		fs = NULL;
		delete cont;
		return;
	}

	if(length != cont->length)
	{
		if(err) err->AddError(L"����� ����� � ������ � ���������� ������ � ��������� �� �����",
			L"����� ����� � ������", length,
			L"������ � ���������", cont->length);
		/*
		delete fs;
		fs = NULL;
		delete cont;
		return;
		*/
	}

	free_blocks = new v8object(this, 1);
	root_object = new v8object(this, 2);

	b = root_object->getdata();

	if(version == ver8_0_3_0 || version == ver8_0_5_0)
	{
		root_80* root80 = (root_80*)b;

		locale = new char[strlen(root80->lang) + 1];
		strcpy(locale, root80->lang);
		//locale = root->lang;

		num_tables = root80->numblocks;
		table_blocks = &(root80->blocks[0]);
	}
	else //if(version == ver8_1_0_0 || version == ver8_2_0_0)
	{
		root_81* root81 = (root_81*)b;

		locale = new char[strlen(root81->lang) + 1];
		strcpy(locale, root81->lang);
		//locale = root->lang;

		num_tables = root81->numblocks;
		table_blocks = &(root81->blocks[0]);

#ifndef PublicRelease
		icu = new ICU(version);

		if(!readonly)
		{
			icu_res = icu->setLocale(locale);
			switch(icu_res)
			{
				case r_badLocale:
					if(err) err->AddError(L"������������ Locale",
						L"Locale", locale);
					readonly = true;
					break;

				case r_LocaleNotSet:
					if(err) err->AddError(L"������ ��� ��������� Locale",
						L"Locale", locale);
					readonly = true;
					break;

				case r_badVersion:
					if(err) err->AddError(L"���������������� ������ Locale",
						L"������ ����", ver,
						L"Locale", locale);
					readonly = true;
					break;

				case r_OK:
					break;

				default:
					if(err) err->AddError(L"����������� ��� �������� ���������� ICU ��� ��������� Locale",
						L"��� ��������", icu_res,
						L"Locale", locale);
					readonly = true;
					break;
			}
		}
#endif //#ifdef PublicRelease
	}

	tables = new table*[num_tables];
	for(i = 0, j = 0; i < num_tables; i++)
	{
		tables[j] = new table(this, table_blocks[i]);
		if(tables[j]->bad)
		{
			delete tables[j];
			continue;
		}
		if(!tables[j]->getname().CompareIC(L"CONFIG")) table_config = tables[j];
		if(!tables[j]->getname().CompareIC(L"CONFIGSAVE")) table_configsave = tables[j];
		if(!tables[j]->getname().CompareIC(L"PARAMS")) table_params = tables[j];
		if(!tables[j]->getname().CompareIC(L"FILES")) table_files = tables[j];
		if(!tables[j]->getname().CompareIC(L"DBSCHEMA")) table_dbschema = tables[j];
		if(!tables[j]->getname().CompareIC(L"CONFIGCAS")) table_configcas = tables[j];
		if(!tables[j]->getname().CompareIC(L"CONFIGCASSAVE")) table_configcassave = tables[j];
		if(!tables[j]->getname().CompareIC(L"_EXTENSIONSINFO")) table__extensionsinfo = tables[j];

		if(!tables[j]->getname().CompareIC(L"DEPOT")) table_depot = tables[j];
		if(!tables[j]->getname().CompareIC(L"USERS")) table_users = tables[j];
		if(!tables[j]->getname().CompareIC(L"OBJECTS")) table_objects = tables[j];
		if(!tables[j]->getname().CompareIC(L"VERSIONS")) table_versions = tables[j];
		if(!tables[j]->getname().CompareIC(L"LABELS")) table_labels = tables[j];
		if(!tables[j]->getname().CompareIC(L"HISTORY")) table_history = tables[j];
		if(!tables[j]->getname().CompareIC(L"LASTESTVERSIONS")) table_lastestversions = tables[j];
		if(!tables[j]->getname().CompareIC(L"EXTERNALS")) table_externals = tables[j];
		if(!tables[j]->getname().CompareIC(L"SELFREFS")) table_selfrefs = tables[j];
		if(!tables[j]->getname().CompareIC(L"OUTREFS")) table_outrefs = tables[j];

		if(j % 10 == 0) if(err) err->Status(String(L"������ ������ ") + j);
		j++;
	}
	if(err) err->Status(String(L"������ ������ ") + j);
	num_tables = j;

#ifdef getcfname
	if(!table_config) if(err) err->AddError(L"����������� ������� CONFIG");
#else
	if(!table_config && !table_configsave && !table_params && !table_files && !table_dbschema)
	{
		if(!table_depot && !table_users && !table_objects && !table_versions && !table_labels && !table_history && !table_lastestversions && !table_externals && !table_selfrefs && !table_outrefs)
		{
			if(err) err->AddMessage(L"���� �� �������� �������������� ����� 1�", msInfo);
		}
		else
		{
			is_depot = true;
			if(!table_depot) if(err) err->AddError(L"����������� ������� DEPOT");
			if(!table_users) if(err) err->AddError(L"����������� ������� USERS");
			if(!table_objects) if(err) err->AddError(L"����������� ������� OBJECTS");
			if(!table_versions) if(err) err->AddError(L"����������� ������� VERSIONS");
			if(!table_labels) if(err) err->AddError(L"����������� ������� LABELS");
			if(!table_history) if(err) err->AddError(L"����������� ������� HISTORY");
			if(!table_lastestversions) if(err) err->AddError(L"����������� ������� LASTESTVERSIONS");
			if(!table_externals) if(err) err->AddError(L"����������� ������� EXTERNALS");
			if(!table_selfrefs) if(err) err->AddError(L"����������� ������� SELFREFS");
			if(!table_outrefs) if(err) err->AddError(L"����������� ������� OUTREFS");
			field::showGUIDasMS = true;
		}
	}
	else
	{
		is_infobase = true;
		if(!table_config) if(err) err->AddError(L"����������� ������� CONFIG");
		if(!table_configsave) if(err) err->AddError(L"����������� ������� CONFIGSAVE");
		if(!table_params) if(err) err->AddError(L"����������� ������� PARAMS");
		if(!table_files) if(err) err->AddError(L"����������� ������� FILES");
		if(!table_dbschema) if(err) err->AddError(L"����������� ������� DBSCHEMA");
	}
#endif //#ifdef getcfname

	delete cont;
}

//---------------------------------------------------------------------------
bool __fastcall T_1CD::is_open()
{
	return fs != NULL;
}

//---------------------------------------------------------------------------
db_ver __fastcall T_1CD::getversion()
{
	return version;
}

//---------------------------------------------------------------------------
bool __fastcall T_1CD::save_config(String _filename)
{
	if(!cs_config) cs_config = new ConfigStorageTableConfig(files_config);
	if(!cs_config->getready()) return false;
	return cs_config->save_config(_filename);
}

//---------------------------------------------------------------------------
bool __fastcall T_1CD::save_configsave(String _filename)
{
	if(!cs_configsave) cs_configsave = new ConfigStorageTableConfigSave(files_config, files_configsave);
	if(!cs_configsave->getready()) return false;
	return cs_configsave->save_config(_filename);
}

//---------------------------------------------------------------------------
void __fastcall T_1CD::find_supplier_configs()
{
	std::map<String,table_file*>::iterator p;

	for(p = files_configsave->files.begin(); p != files_configsave->files.end(); ++p)
	{
		if(p->first.Length() == 73) add_supplier_config(p->second);
	}
	for(p = files_config->files.begin(); p != files_config->files.end(); ++p)
	{
		if(p->first.Length() == 73) add_supplier_config(p->second);
	}
	supplier_configs_defined = true;
}

//---------------------------------------------------------------------------
void __fastcall T_1CD::add_supplier_config(table_file* tf)
{
	container_file* f;
	wchar_t tempfile[MAX_PATH];
	String tempname;
	TFileStream* s;
	//int i, j;
	int i;
	v8catalog* cat = NULL;
	v8catalog* cat2;
	v8file* file;
	tree* tr = NULL;
	String filenamemeta;
	String nodetype;
	String _name; // ��� ������������ ����������
	String _supplier; // ������� ������������ ����������
	String _version; // ������ ������������ ����������
	SupplierConfig sc;

	f = new container_file(tf, tf->name);
	if(!f->open())
	{
		delete f;
		return;
	}

	GetTempFileName(f->temppath, L"awa", 0, tempfile);
	tempname = tempfile;
	s = new TFileStream(tempname, fmCreate);

	try
	{
		try
		{
			InflateStream(f->stream, s);
		}
		catch(...)
		{
			if(err) err->AddError(L"������ ���������� ������������ ����������",
				L"�������", tf->t->getname(),
				L"���", tf->name);
			delete s;
			DeleteFile(tempname);
			return;
		}
		f->close();
		delete f;
		f = NULL;

		cat = new v8catalog(s, true);
		s = NULL;
		file = cat->GetFile(L"version");
		if(!file)
		{
			if(err) err->AddError(L"�� ������ ���� version � ������������ ����������",
				L"�������", tf->t->getname(),
				L"��� �����", tf->name);
			delete cat;
			DeleteFile(tempname);
			return;
		}

		tr = get_treeFromV8file(file, err);
		i = (*tr)[0][0][0].get_value().ToInt();
		delete tr;
		tr = NULL;

		#ifdef _DEBUG
		if(err) err->AddDebugMessage(L"������� ������ ���������� ������������ ����������", msInfo,
			L"�������", tf->t->getname(),
			L"��� �����", tf->name,
			L"������", i);
		#endif

		if(i < 100) // 8.0
		{
			file = cat->GetFile(L"metadata");
			if(!file)
			{
				if(err) err->AddError(L"�� ������ ������� metadata � ������������ ����������",
					L"�������", tf->t->getname(),
					L"��� �����", tf->name);
				delete cat;
				DeleteFile(tempname);
				return;
			}
			cat2 = file->GetCatalog();
			if(!cat2)
			{
				if(err) err->AddError(L"���� metadata ���������� ��������� � ������������ ����������",
					L"�������", tf->t->getname(),
					L"��� �����", tf->name);
				delete cat;
				DeleteFile(tempname);
				return;
			}

		}
		else cat2 = cat; // else 8.1 ��� 8.2

		file = cat2->GetFile(L"root");
		if(!file)
		{
			if(err) err->AddError(L"�� ������ ���� root � ������������ ����������",
				L"�������", tf->t->getname(),
				L"��� �����", tf->name);
			delete cat;
			DeleteFile(tempname);
			return;
		}

		tr = get_treeFromV8file(file, err);
		filenamemeta = (*tr)[0][1].get_value();
		delete tr;
		tr = NULL;

		file = cat2->GetFile(filenamemeta);
		if(!file)
		{
			if(err) err->AddError(L"�� ������ ���� ���������� � ������������ ����������",
				L"�������", tf->t->getname(),
				L"��� �����", tf->name,
				L"��� ����", filenamemeta);
			delete cat;
			DeleteFile(tempname);
			return;
		}

		#ifdef _DEBUG
		if(err) err->AddDebugMessage(L"������ ���� ���������� � ������������ ����������", msInfo,
			L"�������", tf->t->getname(),
			L"��� �����", tf->name,
			L"��� ����", filenamemeta);
		#endif

		tr = get_treeFromV8file(file, err);
		int numnode = (*tr)[0][2].get_value().ToInt();
		for(i = 0; i < numnode; i++)
		{
			tree& node = (*tr)[0][3 + i];
			nodetype = node[0].get_value();
			if(nodetype.CompareIC(L"9cd510cd-abfc-11d4-9434-004095e12fc7") == 0) // ���� "�����"
			{
				tree& confinfo = node[1][1];
				int verconfinfo = confinfo[0].get_value().ToInt();
				switch(verconfinfo)
				{
					case 15:
						_name = confinfo[1][1][2].get_value();
						_supplier = confinfo[11].get_value();
						_version = confinfo[12].get_value();
						break;
					case 22:
					case 32:
					case 34:
					case 36:
					case 37:
						_name = confinfo[1][1][2].get_value();
						_supplier = confinfo[14].get_value();
						_version = confinfo[15].get_value();
						break;
					default:
						_name = confinfo[1][1][2].get_value();
						_supplier = confinfo[14].get_value();
						_version = confinfo[15].get_value();
//						_supplier = "";
//						_version = "";
						#ifdef _DEBUG
						if(err) err->AddDebugMessage(L"����������� ������ ������� ������������ ����������", msInfo,
							L"�������", tf->t->getname(),
							L"��� �����", tf->name,
							L"��� ����", filenamemeta,
							L"������ �������", verconfinfo);
						#endif
						break;
				}
				break;
			}
		}
		delete tr;
		tr = NULL;

		if(i >= numnode)
		{
			if(err) err->AddError(L"�� ������ ���� ����� � ���������� ������������ ����������",
				L"�������", tf->t->getname(),
				L"��� �����", tf->name,
				L"��� ����", filenamemeta);
			_supplier = "";
			_version = "";
		}
		#ifdef _DEBUG
		else
		{
			if(err) err->AddDebugMessage(L"������� ������������ ����������", msInfo,
				L"�������", tf->t->getname(),
				L"��� �����", tf->name,
				L"���", _name,
				L"������", _version,
				L"���������", _supplier);
		}
		#endif


		sc.file = tf;
		sc.name = _name;
		sc.supplier = _supplier;
		sc.version = _version;
		supplier_configs.push_back(sc);

		delete cat;
		cat = NULL;
		DeleteFile(tempname);
	}
	catch(...)
	{
		if(err) err->AddError(L"��������� ������ ��� ������� ������������ ����������",
			L"�������", tf->t->getname(),
			L"��� �����", tf->name);
		delete cat;
		delete s;
		DeleteFile(tempname);
		delete tr;
		delete f;
	}
}

//---------------------------------------------------------------------------
bool __fastcall T_1CD::save_supplier_configs(unsigned int numcon, const String& _filename)
{
	TFileStream* _fs;
	container_file* f;
	table_file* tf;

	if(numcon >= supplier_configs.size()) return false;
	tf = supplier_configs[numcon].file;
	f = new container_file(tf, tf->name);
	if(!f->open())
	{
		delete f;
		return false;
	}

	try
	{
		_fs = new TFileStream(_filename, fmCreate);
	}
	catch(...)
	{
		if(err) err->AddError(L"������ �������� ����� ������������ ����������",
			L"��� �����", _filename);
		delete f;
		return false;
	}

	try
	{
		InflateStream(f->stream, _fs);
	}
	catch(...)
	{
		if(err) err->AddError(L"������ ���������� ����� ������������ ����������",
			L"��� �����", _filename);
		delete f;
		delete _fs;
		return false;
	}

	delete _fs;
	delete f;
	return true;
}

//---------------------------------------------------------------------------
bool __fastcall T_1CD::get_readonly()
{
	return readonly;
}

//---------------------------------------------------------------------------
void __fastcall T_1CD::set_block_as_free(unsigned int block_number)
{
	free_blocks->set_block_as_free(block_number);
}

//---------------------------------------------------------------------------
unsigned int __fastcall T_1CD::get_free_block()
{
	return free_blocks->get_free_block();
}

//---------------------------------------------------------------------------
void __fastcall T_1CD::flush()
{
	memblock::flush();
}

//---------------------------------------------------------------------------

#ifndef PublicRelease
void __fastcall T_1CD::find_lost_objects()
{
	unsigned int i;
	char buf[8];
	v8object* v8obj;
	bool block_is_find;

	for(i = 1; i < length; i++)
	{
		getblock(buf, i, 8);
		if(memcmp(buf, SIG_OBJ, 8) == 0)
		{
			block_is_find = false;
			for(v8obj = v8object::get_first(); v8obj; v8obj = v8obj->get_next())
			{
				if(v8obj->get_block_number() == i)
				{
					block_is_find = true;
					break;
				}
			}
			if(!block_is_find) err->AddMessage_(L"������ ���������� ������", msInfo, L"����� �����", tohex(i));
		}
	}
	err->AddMessage(L"����� ���������� �������� ��������", msSuccesfull);
}
#endif //#ifdef PublicRelease

//---------------------------------------------------------------------------
bool __fastcall T_1CD::test_stream_format()
{
	unsigned int i;
	bool result;
	bool res;

	// CONFIGSAVE
	if(!table_config)
	{
		if(err) err->AddError(L"������ ������������. � ���� ��� ������� CONFIG");
		return false;
	}

	if(table_config->get_numfields() < 6)
	{
		if(err) err->AddError(L"������ ������������. � ������� CONFIG ������ 6 �����",
			L"���-�� �����", table_config->get_numfields());
		return false;
	}

	if(table_config->get_numfields() > 7)
	{
		if(err) err->AddError(L"������ ������������. � ������� CONFIG ������ 7 �����",
			L"���-�� �����", table_config->get_numfields());
		return false;
	}

	if(table_config->getfield(0)->getname().CompareIC(L"FILENAME"))
	{
		if(err) err->AddError(L"������ ������������. ������ ���� ������� CONFIG �� FILENAME",
			L"����", table_config->getfield(0)->getname());
		return false;
	}

	if(table_config->getfield(1)->getname().CompareIC(L"CREATION"))
	{
		if(err) err->AddError(L"������ ������������. ������ ���� ������� CONFIG �� CREATION",
			L"����", table_config->getfield(1)->getname());
		return false;
	}

	if(table_config->getfield(2)->getname().CompareIC(L"MODIFIED"))
	{
		if(err) err->AddError(L"������ ������������. ������ ���� ������� CONFIG �� MODIFIED",
			L"����", table_config->getfield(2)->getname());
		return false;
	}

	if(table_config->getfield(3)->getname().CompareIC(L"ATTRIBUTES"))
	{
		if(err) err->AddError(L"������ ������������. ��������� ���� ������� CONFIG �� ATTRIBUTES",
			L"����", table_config->getfield(3)->getname());
		return false;
	}

	if(table_config->getfield(4)->getname().CompareIC(L"DATASIZE"))
	{
		if(err) err->AddError(L"������ ������������. ����� ���� ������� CONFIG �� DATASIZE",
			L"����", table_config->getfield(4)->getname());
		return false;
	}

	if(table_config->getfield(5)->getname().CompareIC(L"BINARYDATA"))
	{
		if(err) err->AddError(L"������ ������������. ������ ���� ������� CONFIG �� BINARYDATA",
			L"����", table_config->getfield(5)->getname());
		return false;
	}

	if(table_config->get_numfields() > 6)
	{
		if(table_config->getfield(6)->getname().CompareIC(L"PARTNO"))
		{
			if(err) err->AddError(L"������ ������������. ������� ���� ������� CONFIG �� PARTNO",
				L"����", table_config->getfield(6)->getname());
			return false;
		}
	}

	// CONFIGSAVE
	if(!table_configsave)
	{
		if(err) err->AddError(L"������ ������������. � ���� ��� ������� CONFIGSAVE");
		return false;
	}

	if(table_configsave->get_numfields() < 6)
	{
		if(err) err->AddError(L"������ ������������. � ������� CONFIGSAVE ������ 6 �����",
			L"���-�� �����", table_configsave->get_numfields());
		return false;
	}

	if(table_configsave->get_numfields() > 7)
	{
		if(err) err->AddError(L"������ ������������. � ������� CONFIGSAVE ������ 7 �����",
			L"���-�� �����", table_configsave->get_numfields());
		return false;
	}

	if(table_configsave->getfield(0)->getname().CompareIC(L"FILENAME"))
	{
		if(err) err->AddError(L"������ ������������. ������ ���� ������� CONFIGSAVE �� FILENAME",
			L"����", table_configsave->getfield(0)->getname());
		return false;
	}

	if(table_configsave->getfield(1)->getname().CompareIC(L"CREATION"))
	{
		if(err) err->AddError(L"������ ������������. ������ ���� ������� CONFIGSAVE �� CREATION",
			L"����", table_configsave->getfield(1)->getname());
		return false;
	}

	if(table_configsave->getfield(2)->getname().CompareIC(L"MODIFIED"))
	{
		if(err) err->AddError(L"������ ������������. ������ ���� ������� CONFIGSAVE �� MODIFIED",
			L"����", table_configsave->getfield(2)->getname());
		return false;
	}

	if(table_configsave->getfield(3)->getname().CompareIC(L"ATTRIBUTES"))
	{
		if(err) err->AddError(L"������ ������������. ��������� ���� ������� CONFIGSAVE �� ATTRIBUTES",
			L"����", table_configsave->getfield(3)->getname());
		return false;
	}

	if(table_configsave->getfield(4)->getname().CompareIC(L"DATASIZE"))
	{
		if(err) err->AddError(L"������ ������������. ����� ���� ������� CONFIGSAVE �� DATASIZE",
			L"����", table_configsave->getfield(4)->getname());
		return false;
	}

	if(table_configsave->getfield(5)->getname().CompareIC(L"BINARYDATA"))
	{
		if(err) err->AddError(L"������ ������������. ������ ���� ������� CONFIGSAVE �� BINARYDATA",
			L"����", table_configsave->getfield(5)->getname());
		return false;
	}

	if(table_configsave->get_numfields() > 6)
	{
		if(table_configsave->getfield(6)->getname().CompareIC(L"PARTNO"))
		{
			if(err) err->AddError(L"������ ������������. ������� ���� ������� CONFIGSAVE �� PARTNO",
				L"����", table_configsave->getfield(6)->getname());
			return false;
		}
	}

	// PARAMS
	if(!table_params)
	{
		if(err) err->AddError(L"������ ������������. � ���� ��� ������� PARAMS");
		return false;
	}

	if(table_params->get_numfields() < 6)
	{
		if(err) err->AddError(L"������ ������������. � ������� PARAMS ������ 6 �����",
			L"���-�� �����", table_params->get_numfields());
		return false;
	}

	if(table_params->get_numfields() > 7)
	{
		if(err) err->AddError(L"������ ������������. � ������� PARAMS ������ 7 �����",
			L"���-�� �����", table_params->get_numfields());
		return false;
	}

	if(table_params->getfield(0)->getname().CompareIC(L"FILENAME"))
	{
		if(err) err->AddError(L"������ ������������. ������ ���� ������� PARAMS �� FILENAME",
			L"����", table_params->getfield(0)->getname());
		return false;
	}

	if(table_params->getfield(1)->getname().CompareIC(L"CREATION"))
	{
		if(err) err->AddError(L"������ ������������. ������ ���� ������� PARAMS �� CREATION",
			L"����", table_params->getfield(1)->getname());
		return false;
	}

	if(table_params->getfield(2)->getname().CompareIC(L"MODIFIED"))
	{
		if(err) err->AddError(L"������ ������������. ������ ���� ������� PARAMS �� MODIFIED",
			L"����", table_params->getfield(2)->getname());
		return false;
	}

	if(table_params->getfield(3)->getname().CompareIC(L"ATTRIBUTES"))
	{
		if(err) err->AddError(L"������ ������������. ��������� ���� ������� PARAMS �� ATTRIBUTES",
			L"����", table_params->getfield(3)->getname());
		return false;
	}

	if(table_params->getfield(4)->getname().CompareIC(L"DATASIZE"))
	{
		if(err) err->AddError(L"������ ������������. ����� ���� ������� PARAMS �� DATASIZE",
			L"����", table_params->getfield(4)->getname());
		return false;
	}

	if(table_params->getfield(5)->getname().CompareIC(L"BINARYDATA"))
	{
		if(err) err->AddError(L"������ ������������. ������ ���� ������� PARAMS �� BINARYDATA",
			L"����", table_params->getfield(5)->getname());
		return false;
	}

	if(table_params->get_numfields() > 6)
	{
		if(table_params->getfield(6)->getname().CompareIC(L"PARTNO"))
		{
			if(err) err->AddError(L"������ ������������. ������� ���� ������� PARAMS �� PARTNO",
				L"����", table_params->getfield(6)->getname());
			return false;
		}
	}

	// FILES
	if(!table_files)
	{
		if(err) err->AddError(L"������ ������������. � ���� ��� ������� FILES");
		return false;
	}

	if(table_files->get_numfields() < 6)
	{
		if(err) err->AddError(L"������ ������������. � ������� FILES ������ 6 �����",
			L"���-�� �����", table_files->get_numfields());
		return false;
	}

	if(table_files->get_numfields() > 7)
	{
		if(err) err->AddError(L"������ ������������. � ������� FILES ������ 7 �����",
			L"���-�� �����", table_files->get_numfields());
		return false;
	}

	if(table_files->getfield(0)->getname().CompareIC(L"FILENAME"))
	{
		if(err) err->AddError(L"������ ������������. ������ ���� ������� FILES �� FILENAME",
			L"����", table_files->getfield(0)->getname());
		return false;
	}

	if(table_files->getfield(1)->getname().CompareIC(L"CREATION"))
	{
		if(err) err->AddError(L"������ ������������. ������ ���� ������� FILES �� CREATION",
			L"����", table_files->getfield(1)->getname());
		return false;
	}

	if(table_files->getfield(2)->getname().CompareIC(L"MODIFIED"))
	{
		if(err) err->AddError(L"������ ������������. ������ ���� ������� FILES �� MODIFIED",
			L"����", table_files->getfield(2)->getname());
		return false;
	}

	if(table_files->getfield(3)->getname().CompareIC(L"ATTRIBUTES"))
	{
		if(err) err->AddError(L"������ ������������. ��������� ���� ������� FILES �� ATTRIBUTES",
			L"����", table_files->getfield(3)->getname());
		return false;
	}

	if(table_files->getfield(4)->getname().CompareIC(L"DATASIZE"))
	{
		if(err) err->AddError(L"������ ������������. ����� ���� ������� FILES �� DATASIZE",
			L"����", table_files->getfield(4)->getname());
		return false;
	}

	if(table_files->getfield(5)->getname().CompareIC(L"BINARYDATA"))
	{
		if(err) err->AddError(L"������ ������������. ������ ���� ������� FILES �� BINARYDATA",
			L"����", table_files->getfield(5)->getname());
		return false;
	}

	if(table_files->get_numfields() > 6)
	{
		if(table_files->getfield(6)->getname().CompareIC(L"PARTNO"))
		{
			if(err) err->AddError(L"������ ������������. ������� ���� ������� FILES �� PARTNO",
				L"����", table_files->getfield(6)->getname());
			return false;
		}
	}

	// DBSCHEMA
	if(!table_dbschema)
	{
		if(err) err->AddError(L"������ ������������. � ���� ��� ������� DBSCHEMA");
		return false;
	}

	if(table_dbschema->get_numfields() != 1)
	{
		if(err) err->AddError(L"������ ������������. � ������� DBSCHEMA �� 1 ����",
			L"���-�� �����", table_dbschema->get_numfields());
		return false;
	}

	if(table_dbschema->getfield(0)->getname().CompareIC(L"SERIALIZEDDATA"))
	{
		if(err) err->AddError(L"������ ������������. ������ ���� ������� DBSCHEMA �� SERIALIZEDDATA",
			L"����", table_dbschema->getfield(0)->getname());
		return false;
	}

	//================
	result = true;

	//table_config->set_lockinmemory(true);
	for(i = 0; i < table_config->get_phys_numrecords(); i++)
	{
		res = recursive_test_stream_format(table_config, i);
		result = result && res;
	}
	//table_config->set_lockinmemory(false);

	//table_configsave->set_lockinmemory(true);
	for(i = 0; i < table_configsave->get_phys_numrecords(); i++)
	{
		res = recursive_test_stream_format(table_configsave, i);
		result = result && res;
	}
	//table_configsave->set_lockinmemory(false);

	//table_params->set_lockinmemory(true);
	for(i = 0; i < table_params->get_phys_numrecords(); i++)
	{
		res = recursive_test_stream_format(table_params, i);
		result = result && res;
	}
	//table_params->set_lockinmemory(false);

	//table_dbschema->set_lockinmemory(true);
	if(table_dbschema->get_phys_numrecords() < 2)
	{
		if(err) err->AddError(L"������ ������������. � ������� DBSCHEMA ��� �������");
		result = false;
	}
	for(i = 0; i < table_dbschema->get_phys_numrecords(); i++)
	{
		res = recursive_test_stream_format2(table_dbschema, i);
		result = result && res;
	}
	//table_dbschema->set_lockinmemory(false);

	err->Status(L"");
	return result;
}

//---------------------------------------------------------------------------
bool __fastcall T_1CD::recursive_test_stream_format(table* t, unsigned int nrec)
{
	int j;
	char* rec;
	char* orec;
	field* f_name;
	field* f_data_size;
	field* f_binary_data;
	String path;
	String slen;
	TStream* str;
	bool result;
	bool res;
	bool usetempfile = false;
	wchar_t temppath[MAX_PATH];
	wchar_t tempfile[MAX_PATH];

	rec = new char[t->get_recordlen()];
	t->getrecord(nrec, rec);
	if(*rec)
	{
		delete[] rec;
		return true;
	}

	f_name = t->getfield(0);
	f_data_size = t->getfield(4);
	f_binary_data = t->getfield(5);

	path = t->getname() + L"/" + f_name->get_presentation(rec);

	orec = rec + f_binary_data->getoffset();
	if(*(unsigned int*)(orec + 4) > 10 * 1024 * 1024)
	{
		GetTempPath(MAX_PATH - 1, temppath);
		GetTempFileName(temppath, L"t1cd", 0, tempfile);
		str = new TFileStream(String(tempfile), fmCreate);
		usetempfile = true;
	}
	else str = new TMemoryStream();
	t->readBlob(str, *(unsigned int*)orec, *(unsigned int*)(orec + 4));

	result = true;
	slen = f_data_size->get_presentation(rec, true);
	try
	{
		j = slen.ToInt();
	}
	catch(...)
	{
		if(err) err->AddMessage_(L"������ ������ ����� �����", msWarning,
			L"����", path,
			L"����� �����", slen);
		result = false;
	}
	if(result) if((__int64)j != str->Size)
	{
		if(err) err->AddMessage_(L"����������� ����� ����� ���������� �� ��������� � �������", msWarning,
			L"����", path,
			L"����������� ����� �����", str->Size,
			L"��������� ����� �����", slen);
		result = false;
	}

	res = recursive_test_stream_format(str, path, f_name->get_presentation(rec).Length() > 72); // �������� ��������� ����� ���� ������ ������������ ���������� (����� � ������ ����� ����� 72 ��������)
	result = result && res;

	delete[] rec;
	delete str;
	if(usetempfile) DeleteFile(tempfile);

	return result;

}

//---------------------------------------------------------------------------
bool __fastcall T_1CD::recursive_test_stream_format2(table* t, unsigned int nrec)
{
	int j;
	char* rec;
	char* orec;
	field* f_sd;
	String path;
	TMemoryStream* str;
	bool result;

	rec = new char[t->get_recordlen()];
	t->getrecord(nrec, rec);
	if(*rec)
	{
		delete[] rec;
		return true;
	}

	f_sd = t->getfield(0);

	path = t->getname();

	orec = rec + f_sd->getoffset();
	str = new TMemoryStream();
	t->readBlob(str, *(unsigned int*)orec, *(unsigned int*)(orec + 4));

	result = recursive_test_stream_format(str, path);

	delete[] rec;
	delete str;

	return result;

}

//---------------------------------------------------------------------------
bool __fastcall T_1CD::recursive_test_stream_format(TStream* str, String path, bool maybezipped2)
{
	bool zipped1;
	bool zipped2;
	char _buf[12];
	TStream* _s;
	TStream* _s2;
	TBytesStream* _sb;
	bool result;
	TEncoding *enc;
	TBytes bytes1;
	TBytes bytes2;
	v8catalog* cat;
	int offset;
	String sf;
	wchar_t first_symbol;
	int i, j;
	wchar_t temppath[MAX_PATH];
	wchar_t tempfile1[MAX_PATH];
	wchar_t tempfile2[MAX_PATH];
	bool usetempfile = false;
	bool usetempfile1 = false;
	bool usetempfile2 = false;

	err->Status(path);

	if(path == L"PARAMS/users.usr") return true;

	if(str->Size == 0) return true;
	usetempfile = str->Size > 10 * 1024 * 1024;

	if(usetempfile)
	{
		GetTempPath(MAX_PATH - 1, temppath);
		GetTempFileName(temppath, L"t1cd", 0, tempfile1);
		_s = new TFileStream(String(tempfile1), fmCreate);
		usetempfile1 = true;
	}
	else _s = new TMemoryStream();
	zipped1 = false;
	zipped2 = false;

	if(usetempfile)
	{
		GetTempPath(MAX_PATH - 1, temppath);
		GetTempFileName(temppath, L"t1cd", 0, tempfile2);
		_s2 = new TFileStream(String(tempfile2), fmCreate);
		usetempfile2 = true;
	}
	else _s2 = new TMemoryStream();

	_s2->CopyFrom(str, 0);
	try
	{
		_s2->Seek(0, soFromBeginning);
		_s->Size = 0;
		InflateStream(_s2, _s);
		zipped1 = true;
	}
	catch (...)
	{
		_s->Size = 0;
		_s->CopyFrom(str, 0);
	}

	if(zipped1 && maybezipped2)
	{
		_s2->Size = 0;
		_s2->CopyFrom(_s, 0);
		try
		{
			_s2->Seek(0, soFromBeginning);
			_s->Size = 0;
			InflateStream(_s2, _s);
			zipped2 = true;
		}
		catch (...)
		{
			_s->Size = 0;
			_s->CopyFrom(_s2, 0);
		}
	}


	try
	{
		cat = new v8catalog(_s, zipped2, true);
	}
	catch (...)
	{
		if(err) err->AddError(L"������ ������������. ������ ������ �������.",
			L"����", path);
		cat = NULL;

	}
	if(!cat || !cat->GetFirst())
	{
//		delete cat;
//		cat = NULL;


		if(_s->Size >= 16)
		{
			_s->Seek(0, soFromBeginning);
			_s->ReadBuffer(_buf, 8);
			if(memcmp(_buf, SIG_MOXCEL, 7) == 0)
			{
				_s->Seek(13, soFromBeginning);
				_s2->Size = 0;
				_s2->CopyFrom(_s, _s->Size - 13);
				_s->Size = 0;
				_s->CopyFrom(_s2, 0);
			}
			else if(memcmp(_buf, SIG_SKD, 8) == 0)
			{
				_s->Seek(24, soFromBeginning);
				_s2->Size = 0;
				_s2->CopyFrom(_s, _s->Size - 24);
				_s->Size = 0;
				_s->CopyFrom(_s2, 0);
			}
		}

		_sb = new TBytesStream(bytes1);
		_s->Seek(0, soFromBeginning);
		_sb->CopyFrom(_s, 0);

		enc = NULL;
		offset = TEncoding::GetBufferEncoding(_sb->Bytes, enc);
		if(offset == 0)
		{
			if(err) err->AddError(L"������ ������������. ������ ����������� ��������� �����",
				L"����", path);
			result = false;
		}
		else
		{
			if(_sb->Size-offset > 0)
			{
				bytes2 = TEncoding::Convert(enc, TEncoding::Unicode, _sb->Bytes, offset, _sb->Size-offset);
				if(bytes2.Length == 0)
				{
					if(err) err->AddError(L"������ ������������. ������ �����������",
						L"����", path);
					result = false;
				}
				else
				{
					sf = String((wchar_t*)&bytes2[0], bytes2.Length / 2);
					for(i = 1; i <= sf.Length(); i++)
					{
						first_symbol = sf[i];
						if(first_symbol != L'\r' && first_symbol != L'\n' && first_symbol != L'\t' && first_symbol != L' ') break;
					}
					if(first_symbol == L'{' && sf.SubString(i, 15).CompareIC(L"{��������������"))
					{
						tree* rt = parse_1Ctext(sf, err, path);
						if(rt)
						{
							result = true;
							delete rt;
						}
						else result = false;
					}
					else result = true;
				}
			}
			else result = true;
		}

		delete _sb;

	}
	else result = recursive_test_stream_format(cat, path);

	delete cat;

	delete _s;
	delete _s2;
	if(usetempfile1) DeleteFile(tempfile1);
	if(usetempfile2) DeleteFile(tempfile2);

	return result;

}

//---------------------------------------------------------------------------
bool __fastcall T_1CD::recursive_test_stream_format(v8catalog* cat, String path)
{
	v8catalog* c;
	bool result;
	bool res;
	v8file* v8f;
	v8file* v8fp;
	String fname;

	result = true;
	v8f = cat->GetFirst();
	while(v8f)
	{
		result = false;
		try
		{
			c = v8f->GetCatalog();
			result = true;
		}
		catch(...)
		{
			if(err) err->AddError(L"������ ������������. ������ ������ �������.",
				L"����", path);
			c = NULL;
		}
		if(result)
		{
			if(c) result = recursive_test_stream_format(c, path + L"/" + v8f->GetFileName());
			else
			{
				fname = v8f->GetFileName();
				if(fname != L"module" && fname != L"text")
				{
					result = recursive_test_stream_format(v8f->get_data(), path + L"/" + v8f->GetFileName());
				}
			}
		}
		v8fp = v8f;
		v8f = v8f->GetNext();
		v8fp->Close();
	}
	return result;
}

//---------------------------------------------------------------------------
#ifndef PublicRelease
bool __fastcall T_1CD::create_table(String path)
{
	TFileStream* f;
	bool fopen;
	String dir;
	String str;
	char* buf;
	unsigned int i;
	int j;
	export_import_table_root* root;
	v8ob* ob;
	v8object* descr_table;
	v8object* file_data;
	v8object* file_blob;
	v8object* file_index;
	tree* t;

	if(!DirectoryExists(path))
	{
		if(err) err->AddMessage_(L"���������� ������� ������� �� �������", msWarning,
			L"����������", path);
		return false;
	}
	dir = path + L"\\";

	try
	{
		f = new TFileStream(dir + L"root", fmOpenRead);
	}
	catch(...)
	{
		if(err) err->AddMessage_(L"������ �������� ����� ������� ������� root", msWarning,
			L"����", dir + L"root");
		return false;
	}
	root = new export_import_table_root;
	f->Read(root, sizeof(export_import_table_root));
	delete f;


	try
	{
		f = new TFileStream(dir + L"descr", fmOpenRead);
	}
	catch(...)
	{
		if(err) err->AddMessage_(L"������ �������� ����� ������� ������� descr", msWarning,
			L"����", dir + L"descr");
		return false;
	}

	i = f->Size;
	buf = new char[i + 2];
	f->Read(buf, i);
	buf[i] = 0;
	buf[i + 1] = 0;
	str = String((wchar_t*)buf);
	delete[] buf;
	delete f;

	t = parse_1Ctext(str, err, dir + L"descr");
	str = (*t)[0][0].get_value();

	for(j = 0; j < num_tables; j++) if(tables[j]->getname().CompareIC(str) == 0)
	{
		tables[j]->import_table2(path);
		return true;
	}

	//descr_table = NULL;
	file_data = NULL;
	file_blob = NULL;
	file_index = NULL;

	if(root->has_data)
	{
		fopen = false;
		try
		{
			f = new TFileStream(dir + L"data", fmOpenRead);
			fopen = true;
		}
		catch(...)
		{
			if(err) err->AddMessage_(L"������ �������� ����� ������� ������� data", msWarning,
				L"����", dir + L"data");
		}
		if(fopen)
		{
			file_data = new v8object(this);
			file_data->setdata(f);
			ob = (v8ob*)getblock_for_write(file_data->get_block_number(), true);
			ob->version.version_1 = root->data_version_1;
			ob->version.version_2 = root->data_version_2;
			delete f;
		}
	}

	if(root->has_blob)
	{
		fopen = false;
		try
		{
			f = new TFileStream(dir + L"blob", fmOpenRead);
			fopen = true;
		}
		catch(...)
		{
			if(err) err->AddMessage_(L"������ �������� ����� ������� ������� blob", msWarning,
				L"����", dir + L"blob");
		}
		if(fopen)
		{
			file_blob = new v8object(this);
			file_blob->setdata(f);
			ob = (v8ob*)getblock_for_write(file_blob->get_block_number(), true);
			ob->version.version_1 = root->blob_version_1;
			ob->version.version_2 = root->blob_version_2;
			delete f;
		}
	}

	if(root->has_index)
	{
		fopen = false;
		try
		{
			f = new TFileStream(dir + L"index", fmOpenRead);
			fopen = true;
		}
		catch(...)
		{
			if(err) err->AddMessage_(L"������ �������� ����� ������� ������� index", msWarning,
				L"����", dir + L"index");
		}
		if(fopen)
		{
			file_index = new v8object(this);
			file_index->setdata(f);
			ob = (v8ob*)getblock_for_write(file_index->get_block_number(), true);
			ob->version.version_1 = root->index_version_1;
			ob->version.version_2 = root->index_version_2;
			delete f;
		}
	}

	if(root->has_descr)
	{
		fopen = false;
		try
		{
			f = new TFileStream(dir + L"descr", fmOpenRead);
			fopen = true;
		}
		catch(...)
		{
			if(err) err->AddMessage_(L"������ �������� ����� ������� ������� descr", msWarning,
				L"����", dir + L"descr");
		}
		if(fopen)
		{
			descr_table = new v8object(this);
			ob = (v8ob*)getblock_for_write(descr_table->get_block_number(), true);
			ob->version.version_1 = root->descr_version_1;
			ob->version.version_2 = root->descr_version_2;

			i = f->Size;
			buf = new char[i + 2];
			f->Read(buf, i);
			buf[i] =0;
			buf[i + 1] =0;
			str = String((wchar_t*)buf);
			delete[] buf;
			delete f;

			i = str.Pos(L"{\"Files\",");
			if(i == 0)
			{
				if(err) err->AddMessage_(L"������ ������ ������� Files � ����� ������� ������� descr", msWarning,
					L"����", dir + L"descr");
				delete root;
				return false;
			}
			str.SetLength(i - 1);
			str += L"{\"Files\",";
			str += file_data ? String(file_data->get_block_number()) : String(L"0");
			str += L",";
			str += file_blob ? String(file_blob->get_block_number()) : String(L"0");
			str += L",";
			str += file_index ? String(file_index->get_block_number()) : String(L"0");
			str += L"}\n}";
			descr_table->setdata(str.c_str(), str.Length() * 2);

			i = root_object->getlen();
			buf = new char[i + 4];
			root_object->getdata(buf, 0, i);

			if(version == ver8_0_3_0 || version == ver8_0_5_0)
			{
				root_80* root80 = (root_80*)buf;
				root80->blocks[root80->numblocks] = descr_table->get_block_number();
				root80->numblocks++;
				root_object->setdata(buf, i + 4);
			}
			else //if(version == ver8_1_0_0 || version == ver8_2_0_0)
			{
				root_81* root81 = (root_81*)buf;
				root81->blocks[root81->numblocks] = descr_table->get_block_number();
				root81->numblocks++;
				root_object->setdata(buf, i + 4);
			}


		}


	}

	flush();

	if(err) err->AddMessage_(L"������� ������� � �������������", msSuccesfull,
		L"����", dir);

	delete root;
	return true;
}
#endif //#ifdef PublicRelease

//---------------------------------------------------------------------------
void __fastcall T_1CD::set_readonly(bool ro)
{
	readonly = ro;
}

//---------------------------------------------------------------------------
bool __fastcall T_1CD::test_list_of_tables()
{
	char* rec;
	char* orec;
	field* f_name;
	field* f_data_size;
	field* f_binary_data;
	bool hasDBNames;
	bool result;
	bool is_slave;
	String slen;
	String sf;
	TMemoryStream* str;
	TBytesStream* _sb;
	TEncoding *enc;
	TBytes bytes1;
	TBytes bytes2;
	wchar_t first_symbol;
	int i, j, l, l2;
	unsigned int k;
	int offset;
	tree* t;
	tree* firstt;

	String _guid;
	String _name;
	String _num;
	String _tabname;

	if(!table_params)
	{
		if(err) err->AddError(L"������ ������������. � ���� ��� ������� PARAMS");
		return false;
	}

	if(table_params->get_numfields() < 6)
	{
		if(err) err->AddError(L"������ ������������. � ������� PARAMS ������ 6 �����",
			L"���-�� �����", table_params->get_numfields());
		return false;
	}

	if(table_params->get_numfields() > 7)
	{
		if(err) err->AddError(L"������ ������������. � ������� PARAMS ������ 7 �����",
			L"���-�� �����", table_params->get_numfields());
		return false;
	}

	if(table_params->getfield(0)->getname().CompareIC(L"FILENAME"))
	{
		if(err) err->AddError(L"������ ������������. ������ ���� ������� PARAMS �� FILENAME",
			L"����", table_params->getfield(0)->getname());
		return false;
	}

	if(table_params->getfield(1)->getname().CompareIC(L"CREATION"))
	{
		if(err) err->AddError(L"������ ������������. ������ ���� ������� PARAMS �� CREATION",
			L"����", table_params->getfield(1)->getname());
		return false;
	}

	if(table_params->getfield(2)->getname().CompareIC(L"MODIFIED"))
	{
		if(err) err->AddError(L"������ ������������. ������ ���� ������� PARAMS �� MODIFIED",
			L"����", table_params->getfield(2)->getname());
		return false;
	}

	if(table_params->getfield(3)->getname().CompareIC(L"ATTRIBUTES"))
	{
		if(err) err->AddError(L"������ ������������. ��������� ���� ������� PARAMS �� ATTRIBUTES",
			L"����", table_params->getfield(3)->getname());
		return false;
	}

	if(table_params->getfield(4)->getname().CompareIC(L"DATASIZE"))
	{
		if(err) err->AddError(L"������ ������������. ����� ���� ������� PARAMS �� DATASIZE",
			L"����", table_params->getfield(4)->getname());
		return false;
	}

	if(table_params->getfield(5)->getname().CompareIC(L"BINARYDATA"))
	{
		if(err) err->AddError(L"������ ������������. ������ ���� ������� PARAMS �� BINARYDATA",
			L"����", table_params->getfield(5)->getname());
		return false;
	}

	if(table_params->get_numfields() > 6)
	{
		if(table_params->getfield(6)->getname().CompareIC(L"PARTNO"))
		{
			if(err) err->AddError(L"������ ������������. ������� ���� ������� PARAMS �� PARTNO",
				L"����", table_params->getfield(6)->getname());
			return false;
		}
	}

	result = true;

	f_name = table_params->getfield(0);
	f_data_size = table_params->getfield(4);
	f_binary_data = table_params->getfield(5);
	rec = new char[table_params->get_recordlen()];

	hasDBNames = false;
	for(k = 0; k < table_params->get_phys_numrecords(); k++)
	{

		table_params->getrecord(k, rec);
		if(*rec) continue;

		if(f_name->get_presentation(rec).CompareIC(L"DBNames") != 0) continue;

		hasDBNames = true;

		orec = rec + f_binary_data->getoffset();
		str = new TMemoryStream();
		table_params->readBlob(str, *(unsigned int*)orec, *(unsigned int*)(orec + 4));

		slen = f_data_size->get_presentation(rec, true);
		try
		{
			j = slen.ToInt();
		}
		catch(...)
		{
			if(err) err->AddMessage_(L"������ ������ ����� �����", msWarning,
				L"����", L"PARAMS/DBNames",
				L"����� �����", slen);
			result = false;
			break;
		}
		if((__int64)j != str->Size)
		{
			if(err) err->AddMessage_(L"����������� ����� ����� ���������� �� ��������� � �������", msWarning,
				L"����", L"PARAMS/DBNames",
				L"����������� ����� �����", str->Size,
				L"��������� ����� �����", slen);
			result = false;
			break;
		}

		str->Seek(0, soFromBeginning);
		_sb = new TBytesStream(bytes1);

		if(version == ver8_0_3_0 || version == ver8_0_5_0)
		{
			_sb->CopyFrom(str, 0i64);
		}
		else
		{
			try
			{
				InflateStream(str, _sb);
			}
			catch (...)
			{
				if(err) err->AddMessage(L"������ ���������� ������ ����� PARAMS/DBNames", msError);
				result = false;
				break;
			}
		}
		delete str;

		enc = NULL;
		offset = TEncoding::GetBufferEncoding(_sb->Bytes, enc);
		if(offset == 0)
		{
			if(err) err->AddError(L"������ ������������. ������ ����������� ��������� ����� PARAMS/DBNames");
			result = false;
		}
		else
		{
			if(_sb->Size-offset > 0)
			{
				bytes2 = TEncoding::Convert(enc, TEncoding::Unicode, _sb->Bytes, offset, _sb->Size-offset);
				if(bytes2.Length == 0)
				{
					if(err) err->AddError(L"������ ������������. ������ ����������� ����� PARAMS/DBNames");
					result = false;
				}
				else
				{
					sf = String((wchar_t*)&bytes2[0], bytes2.Length / 2);
					for(i = 1; i <= sf.Length(); i++)
					{
						first_symbol = sf[i];
						if(first_symbol != L'\r' && first_symbol != L'\n' && first_symbol != L'\t' && first_symbol != L' ') break;
					}
					if(first_symbol == L'{')
					{
						tree* rt = parse_1Ctext(sf, err, L"PARAMS/DBNames");
						if(rt)
						{
							firstt = &((*rt)[0][1][1]);

							for(t = firstt; t; t = t->get_next())
							{
								is_slave = false;
								_name = t->get_subnode(1)->get_value();
								if(_name.CompareIC(L"Fld") == 0) continue;
								if(_name.CompareIC(L"LineNo") == 0) continue;
								if(_name.CompareIC(L"Turnover") == 0) continue;
								if(_name.CompareIC(L"TurnoverDt") == 0) continue;
								if(_name.CompareIC(L"TurnoverCt") == 0) continue;
								if(_name.CompareIC(L"ByField") == 0) continue;
								if(_name.CompareIC(L"ByOwnerField") == 0) continue;
								if(_name.CompareIC(L"ByParentField") == 0) continue;
								if(_name.CompareIC(L"ByProperty") == 0) continue;
								if(_name.CompareIC(L"ByPropRecorder") == 0) continue;
								if(_name.CompareIC(L"ByResource") == 0) continue;
								if(_name.CompareIC(L"ByDim") == 0) continue;
								if(_name.CompareIC(L"ByDims") == 0) continue;
								if(_name.CompareIC(L"ByDimension") == 0) continue;
								if(_name.CompareIC(L"ByDimensions") == 0) continue;
								if(_name.CompareIC(L"ByDimRecorder") == 0) continue;
								if(_name.CompareIC(L"VT") == 0) is_slave = true;
								if(_name.CompareIC(L"ExtDim") == 0) is_slave = true;

								_guid = t->get_subnode(0)->get_value();
								_num = t->get_subnode(2)->get_value();

								if(_guid == L"00000000-0000-0000-0000-000000000000") continue;

								_tabname = L"_";
								_tabname += _name;
//								if(_guid != L"00000000-0000-0000-0000-000000000000") _tabname += _num;
								_tabname += _num;
								l = _tabname.Length();

								//err->Status(_tabname);
//								if(_tabname.CompareIC(L"_Consts") == 0) continue;
//								if(_tabname.CompareIC(L"_ExtDataSrcPrms") == 0) continue;
//								if(_tabname.CompareIC(L"_AccRgOpt") == 0) continue;
//								if(_tabname.CompareIC(L"_ConstsChngR") == 0) continue;
//								if(_tabname.CompareIC(L"_AccumRgOpt") == 0) continue;
//								if(_tabname.CompareIC(L"_ScheduledJobs") == 0) continue;

								bool table_found = false;
								for(i = 0; i < get_numtables(); i++)
								{
									if(is_slave)
									{
										sf = gettable(i)->getname();
										l2 = sf.Length();
										if(l2 > l) if(sf.SubString(l2 - l + 1, l).CompareIC(_tabname) == 0)
										{
											table_found = true;
											break;
										}
									}
									else if(gettable(i)->getname().CompareIC(_tabname) == 0)
									{
										table_found = true;
										break;
									}
								}


								if(!table_found)
								{
									if(err) err->AddMessage_(L"����������� �������", msWarning,
										L"��� �������", _tabname);
									result = false;
								}
							}
							//err->AddMessage((*t)[0].get_value(), msWarning);

							delete rt;
						}
						else result = false;
					}
					else
					{
						if(err) err->AddError(L"������ ������������. ������ ������� ����� PARAMS/DBNames. ������ ������ �� \"{\".");
						result = false;
					}
				}
			}
			else
			{
				if(err) err->AddError(L"������ ������������. ������ ����������� ��������� ����� PARAMS/DBNames");
				result = false;
			}
		}

		delete _sb;

		break;

	}


	if(!hasDBNames)
	{
		if(err) err->AddError(L"������ ������������. � ������� PARAMS �� ������� ������ DBNames.");
		result = false;
	}

	delete[] rec;

	//err->Status(L"");
	return result;
}

#ifndef PublicRelease
//---------------------------------------------------------------------------
bool __fastcall T_1CD::replaceTREF(String mapfile)
{
	DynamicArray<int> map; // ������������ ������ ������������ �������
	int i,j,m;
	int k, l;
	unsigned int ii, jj, kk;
	char* rec;
	String str;
	TStringList* list;
	table* t;
	field* f;
	bool editsave;

	list = new TStringList;
	list->LoadFromFile(mapfile);

	m = 0;
	for(k = 0; k < list->Count; k++)
	{
		str = (*list)[k];
		l = str.Pos(L"\t");
		if(!l) continue;
		j = str.SubString(l + 1, str.Length() - l).ToInt();
		if(m < j) m = j;
	}

	map.set_length(m + 1);

	for(k = 0; k < list->Count; k++)
	{
		str = (*list)[k];
		l = str.Pos(L"\t");
		if(!l) continue;
		i = str.SubString(1, l - 1).ToInt();
		j = str.SubString(l + 1, str.Length() - l).ToInt();
		map[j] = i;
	}

	delete list;

	for(i = 0; i < num_tables; i++)
	{
		t = gettable(i);
		for(j = 0; j < t->get_numfields(); j ++)
		{
			f = t->getfield(j);
			str = f->getname();
			if(str.Length() > 3)
			if(str.SubString(str.Length() - 3, 4).CompareIC(L"TREF") == 0)
			if(f->gettype() == tf_binary)
			if(f->getlength() == 4)
			{
				if(err) err->Status(t->getname() + L" : " + f->getname());
				k = f->getoffset();
				if(f->getnull_exists()) k++;
				rec = new char[t->get_recordlen()];
				editsave = t->edit;
				t->edit = true;
				for(kk = 0; kk < t->get_phys_numrecords(); kk++)
				{
					t->getrecord(kk, rec);
					if(*rec) continue;
					ii = reverse_byte_order(*((unsigned int*)(rec + k)));
					if(ii == 0) continue;
					ii = map[ii];
					*((int*)(rec + k)) = reverse_byte_order(ii);
					t->write_data_record(kk, rec);
				}
				t->edit = editsave;

				delete[] rec;
			}
		}
	}
	if(err) err->Status(L"");

	flush();

	return true;
}

//---------------------------------------------------------------------------
bool __fastcall T_1CD::delete_table(table* tab)
{
	int i;
	unsigned int j;
	unsigned int bl;
	char* buf;
	bool res = num_tables > 0;

	bl = tab->descr_table->block;

	res = res && delete_object(tab->descr_table);
	if(res) res = res && delete_object(tab->file_data);
	if(res) res = res && delete_object(tab->file_blob);
	if(res) res = res && delete_object(tab->file_index);

	if(res)
	{
		tab->descr_table = NULL;
		tab->file_data = NULL;
		tab->file_blob = NULL;
		tab->file_index = NULL;


		for(i = 0; i < num_tables; i++) if(tables[i] == tab) break;
		num_tables--;
		for(; i < num_tables; i++) tables[i] = tables[i + 1];
		delete tab;

		j = root_object->getlen();
		buf = new char[j];
		root_object->getdata(buf, 0, j);

		if(version == ver8_0_3_0 || version == ver8_0_5_0)
		{
			root_80* root80 = (root_80*)buf;
			for(i = 0; i < root80->numblocks; i++) if(root80->blocks[i] == bl) break;
			root80->numblocks--;
			for(; i < root80->numblocks; i++) root80->blocks[i] = root80->blocks[i + 1];
		}
		else //if(version == ver8_1_0_0 || version == ver8_2_0_0)
		{
			root_81* root81 = (root_81*)buf;
			for(i = 0; i < root81->numblocks; i++) if(root81->blocks[i] == bl) break;
			root81->numblocks--;
			for(; i < root81->numblocks; i++) root81->blocks[i] = root81->blocks[i + 1];
		}
		root_object->setdata(buf, j - 4);
		delete[] buf;

	}

	return res;
}

//---------------------------------------------------------------------------
bool __fastcall T_1CD::delete_object(v8object* ob)
{
	objtab* b;
	unsigned int i;
	int j;

	if(!ob) return true;

	if(ob->block == 1)
	{
		err->AddMessage_(L"������� �������� ������� ������� ��������� ������", msWarning,
			L"����� ����� �������", ob->block);
			return false;
	}

	if(ob->block == 2)
	{
		err->AddMessage_(L"������� �������� ��������� �������", msWarning,
			L"����� ����� �������", ob->block);
			return false;
	}

	for(i = 0; i < ob->numblocks; i++)
	{
		b = (objtab*)getblock(ob->blocks[i]);
		for(j = 0; j < b->numblocks; j++) set_block_as_free(b->blocks[j]);
	}
	for(i = 0; i < ob->numblocks; i++) set_block_as_free(ob->blocks[i]);
	for(i = 0; i < ob->numblocks; i++) set_block_as_free(ob->block);

	delete ob;

	return true;

}

//---------------------------------------------------------------------------
void __fastcall T_1CD::find_and_create_lost_tables()
{
	unsigned int i, k;
	int j, numlosttables;
	char buf[8];
	v8object* v8obj;
	bool block_is_find;
	DynamicArray<unsigned int> losttables;
	char* b;

	numlosttables = 0;
	for(i = 1; i < length; i++)
	{
		getblock(buf, i, 8);
		if(memcmp(buf, SIG_OBJ, 8) == 0)
		{
			block_is_find = false;
			for(v8obj = v8object::get_first(); v8obj; v8obj = v8obj->get_next())
			{
				if(v8obj->get_block_number() == i)
				{
					block_is_find = true;
					break;
				}
			}
			if(!block_is_find)
			{
				v8obj = new v8object(this, i);
				if(v8obj->len > 3)
				{
					try
					{
						v8obj->getdata(buf, 0, 4);
						if(memcmp(buf, SIG_TABDESCR, 4) == 0)
						{
							if(losttables.get_length() <= numlosttables) losttables.set_length(losttables.get_length() + 1024);
							losttables[numlosttables++] = i;
						}
					}
					catch(...)
					{ //-V565

					}
				}
				delete v8obj;
			}
		}
	}

	if(numlosttables)
	{
		i = root_object->getlen();
		b = new char[i + numlosttables * 4];
		root_object->getdata(b, 0, i);

		if(version == ver8_0_3_0 || version == ver8_0_5_0)
		{
			root_80* root80 = (root_80*)b;
			for(j = 0, k = root80->numblocks; j < numlosttables; j++, k++) root80->blocks[k] = losttables[j];
			root80->numblocks += numlosttables;
		}
		else //if(version == ver8_1_0_0 || version == ver8_2_0_0)
		{
			root_81* root81 = (root_81*)b;
			for(j = 0, k = root81->numblocks; j < numlosttables; j++, k++) root81->blocks[k] = losttables[j];
			root81->numblocks += numlosttables;
		}
		root_object->setdata(b, i + numlosttables * 4);
		delete[] b;

	}

	err->AddMessage_(L"����� � �������������� ���������� ������ ���������", msSuccesfull,
	L"���������� ��������������� ������", numlosttables);

}

//---------------------------------------------------------------------------
void __fastcall T_1CD::find_and_save_lost_objects()
{
	unsigned int i;
	char buf[8];
	v8object* v8obj;
	bool block_is_find;
	bool dir_created;
	String path;

	dir_created = false;
	path = filename.SubString(1, filename.LastDelimiter(L"\\"));
	path += L"LostObjects\\";

	for(i = 1; i < length; i++)
	{
		getblock(buf, i, 8);
		if(memcmp(buf, SIG_OBJ, 8) == 0)
		{
			block_is_find = false;
			for(v8obj = v8object::get_first(); v8obj; v8obj = v8obj->get_next())
			{
				if(v8obj->get_block_number() == i)
				{
					block_is_find = true;
					break;
				}
			}
			if(!block_is_find)
			{
				if(!dir_created)
				{
					CreateDir(path);
					dir_created = true;
				}
				v8obj = new v8object(this, i);
				v8obj->savetofile(path + L"block" + i);
				delete v8obj;
				//err->AddMessage_(L"������ � �������� ���������� ������", msInfo, L"����� �����", tohex(i));
			}
		}
	}
	err->AddMessage(L"����� � ���������� ���������� �������� ��������", msSuccesfull);

}

#endif //#ifdef PublicRelease

//---------------------------------------------------------------------------
// ���� �� ������� �������� ������, ������������ 0, ����� ������������ ������������� �����
int __fastcall T_1CD::get_ver_depot_config(int ver) // ��������� ������ ������ ������������ (0 - ���������, -1 - ������������� � �.�.)
{
	char* rec;
	index* ind;
	field* fld;
	unsigned int i;
	int v;
	String s;

	if(!is_open()) return 0;

	if(!is_depot)
	{
		if(err) err->AddError(L"���� �� �������� ���������� ������������.");
		return 0;
	}

	if(ver > 0) return ver;

	// ���������� ����� ��������� ������ ������������
	if(!table_versions)
	{
		if(err) err->AddError(L"� ���� ��������� ����������� ������� VERSIONS.");
		return 0;
	}

	fld = get_field(table_versions, L"VERNUM");
	if(!fld) return 0;

	ind = get_index(table_versions, L"PK");
	if(!ind) return 0;

	i = ind->get_numrecords();
	if(i <= (unsigned int)(-ver))
	{
		err->AddMessage_(L"����������� ������ ������������ �� ����������", msError,
			L"����� ������ � ���������", i,
			L"����������� ����� ������", ver);
		return 0;
	}
	i = ind->get_numrec(i + ver - 1);

	rec = new char[table_versions->get_recordlen()];
	table_versions->getrecord(i, rec);
	s = fld->get_presentation(rec, true);
	delete[] rec;
	v = s.ToIntDef(0);
	if(!v)
	{
		if(err) err->AddMessage_(L"�� ������� �������� �������� ����� ������ ����������� ������������.", msError,
			L"����������� ����� ������", ver);
		return 0;
	}

	return v;
}

field* T_1CD::get_field(table* tab, String fieldname)
{
	int j;
	field* fld;
	String s;

	for(j = 0; j < tab->num_fields; j++)
	{
		fld = tab->fields[j];
		if(fld->getname().CompareIC(fieldname) == 0) return fld;
	}
	if(err)
	{
		s =L"� ������� ";
		s += tab->name;
		s += L" �� ������� ���� ";
		s += fieldname;
		s += L".";
		err->AddError(s);
	}
	return NULL;
}

index* T_1CD::get_index(table* tab, String indexname)
{
	int j;
	index* ind;
	String s;

	for(j = 0; j < tab->num_indexes; j++)
	{
		ind = tab->indexes[j];
		if(ind->getname().CompareIC(indexname) == 0) return ind;
	}
	if(err)
	{
		s =L"� ������� ";
		s += tab->name;
		s += L" �� ������ ������ ";
		s += indexname;
		s += L".";
		err->AddError(s);
	}
	return NULL;
}

//---------------------------------------------------------------------------
// ���������� ������������ � ���� �� ��������� ������������
// ver - ����� ������ ����������� ������������
// ver > 0 - ������������ ���������� ����� ������
// ver <= 0 - ����� ������ �� ��������� ������������. 0 - ��������� ������������, -1 - ������������� � �.�., �.�. ����� ������ ������������ ��� ����� ��������� + ver
bool __fastcall T_1CD::save_depot_config(const String& _filename, int ver)
{
	char* rec;
	char* frec;
	//field* fld;
	field* fldd_depotver;
	field* fldd_rootobjid;

	field* fldv_vernum;
	field* fldv_cversion;
	field* fldv_snapshotcrc;
	field* fldv_snapshotmaker;

	field* fldh_objid;
	field* fldh_vernum;
	field* fldh_objverid;
	field* fldh_removed;
	field* fldh_datapacked;
	field* fldh_objdata;
	field* fldh_datahash;
	index* indh;
	char* rech1;
	char* rech2;

	char rootobj[16];
	char curobj[16];
	unsigned int ih, nh;
	int countr;

	field* flde_objid;
	field* flde_vernum;
	field* flde_extname;
	field* flde_extverid;
	field* flde_datapacked;
	field* flde_extdata;
	field* flde_datahash;
	index* inde;
	char* rece;
	DynamicArray<char*> reces;
	DynamicArray<String> extnames;
	int nreces;
	unsigned int ie, ne;
	int countv;

	bool ok;
	bool lastremoved;
	bool removed;
	bool datapacked;
	bool deletesobj;
	char emptyimage[8];
	unsigned int i, k, _crc;
	int v, j, res, lastver, n;
	String s, ss, sp, sn;
	depot_ver depotVer;
	unsigned int configVerMajor, configVerMinor;
	//char VerDate[7];
	TStream* in;
	TStream* out;
	TStream* sobj;
	TStreamWriter* sw;
	std::vector<_packdata> packdates;
	TSearchRec srec;
	_packdata pd;
	_packdata* pdr;
	__int64 packlen;
	v8catalog* cat;
	v8catalog* cath;
	bool unziph;
	v8file* f;
	tree* t;
	tree* tc;
	tree* tv; // ������ ������ ����� versions
	tree* tvc; // ���. ������� ������ ����� versions
	tree* tr; // ������ ������ ����� root
	tree* trc; // ���. ������� ������ ����� root
	tree* th; // ���. ������� ������ ����� ��� ���������� ������ �������� �� ������� HISTORY (root ��� versions � ����������� �� ������ ������������)
	tree* tcountv; // ����, ���������� ������� � ����� versions
	tree* tcountr; // ����, ���������� ������� � ����� root
	String __filename;

	union
	{
		GUID guid;
		unsigned char uid[16];
	};

	union
	{
		char cv_b[2];
		unsigned short cv_s;
	};


	if(!is_open()) return false;

	if(!is_depot)
	{
		if(err) err->AddError(L"���� �� �������� ���������� ������������.");
		return false;
	}

	// �������� ������ ���������
	if(!table_depot)
	{
		if(err) err->AddError(L"� ���� ��������� ����������� ������� DEPOT.");
		return false;
	}

	fldd_depotver = get_field(table_depot, L"DEPOTVER");
	if(!fldd_depotver) return false;
	fldd_rootobjid = get_field(table_depot, L"ROOTOBJID");
	if(!fldd_rootobjid) return false;

	rec = new char[table_depot->get_recordlen()];
	ok = false;
	for(i = 0; i < table_depot->get_phys_numrecords(); i++)
	{
		table_depot->getrecord(i, rec);
		if(!*rec)
		{
			ok = true;
			break;
		}
	}
	if(!ok)
	{
		if(err) err->AddError(L"�� ������� ��������� ������ � ������� DEPOT.");
		delete[] rec;
		return false;
	}

	s = fldd_depotver->get_presentation(rec, true);

	if(s.CompareIC(L"0300000000000000") == 0) depotVer = depotVer3;
	else if(s.CompareIC(L"0500000000000000") == 0) depotVer = depotVer5;
	else if(s.CompareIC(L"0600000000000000") == 0) depotVer = depotVer6;
	else if(s.CompareIC(L"0700000000000000") == 0) depotVer = depotVer7;
	else
	{
		err->AddMessage_(L"����������� ������ ���������", msError,
			L"������ ���������", s);
		delete[] rec;
		return false;
	}

	memcpy(rootobj, rec + fldd_rootobjid->offset, 16);
	delete[] rec;

	// "�����������" ������ ������������
	ver = get_ver_depot_config(ver);

	// ���� ������ � ������� ������ ������������
	if(!table_versions)
	{
		if(err) err->AddError(L"� ���� ��������� ����������� ������� VERSIONS.");
		return false;
	}

	fldv_vernum = get_field(table_versions, L"VERNUM");
	if(!fldv_vernum) return false;
	if(depotVer >= depotVer5)
	{
		fldv_cversion = get_field(table_versions, L"CVERSION");
		if(!fldv_cversion) return false;
	}
	fldv_snapshotcrc = get_field(table_versions, L"SNAPSHOTCRC");
	if(!fldv_snapshotcrc) return false;
	fldv_snapshotmaker = get_field(table_versions, L"SNAPSHOTMAKER");
	if(!fldv_snapshotmaker) return false;

	rec = new char[table_versions->get_recordlen()];
	ok = false;
	for(i = 0; i < table_versions->get_phys_numrecords(); i++)
	{
		table_versions->getrecord(i, rec);
		if(*rec) continue;
		s = fldv_vernum->get_presentation(rec, true);
		v = s.ToIntDef(0);
		if(v == ver)
		{
			ok = true;
			break;
		}
	}

	if(!ok)
	{
		if(err) err->AddMessage_(L"� ��������� �� ������� ������ ������������", msError,
			L"��������� ������", ver);
		delete[] rec;
		return false;
	}

	__filename = System::Ioutils::TPath::GetFullPath(_filename);
/*
	// ���������, ��� �� �������� ������ ������ ������������
	if(*(rec + fldv_snapshotcrc->offset)) if(*(rec + fldv_snapshotmaker->offset)) if(memcmp(rootobj, rec + fldv_snapshotmaker->offset + 1, 16) == 0)
	{
		_crc = *(unsigned int*)(rec + fldv_snapshotcrc->offset + 1);

		//s = System.IOUtils.TPath::GetDirectoryName(filename);
		s = filename.SubString(1, filename.LastDelimiter(L"\\"));
		s += L"cache\\ddb";
		ss = L"00000";
		ss += ver;
		s += ss.SubString(ss.Length() - 4, 5);
		s += L".snp";
		if(FileExists(s))
		{
			try
			{
				in = new TFileStream(s, fmOpenRead | fmShareDenyNone);
			}
			catch(...)
			{
				if(err) err->AddMessage_(L"�� ������� ������� ���� ��������", msWarning,
					L"��� �����", s,
					L"��������� ������", ver);
				in = NULL;
			}
			try
			{
				//if(FileExists(__filename)) DeleteFile(__filename);
				out = new TFileStream(__filename, fmCreate | fmShareDenyWrite);
			}
			catch(...)
			{
				if(err) err->AddMessage_(L"�� ������� ������� ���� ������������", msWarning,
					L"��� �����", __filename);
				delete[] rec;
				return false;
			}
			if(in)
			{
				try
				{
					InflateStream(in, out);
				}
				catch(...)
				{
					if(err) err->AddMessage_(L"�� ������� ����������� ���� ��������", msWarning,
						L"��� �����", s,
						L"��������� ������", ver);
					delete out;
					out = NULL;
				}
				delete in;
				in = NULL;
				if(out)
				{
					k = _crc32(out);
					if(k == _crc)
					{
						delete out;
						delete[] rec;
						return true;
					}
					if(err) err->AddMessage_(L"���� �������� �������� (�� ������� ����������� �����)", msWarning,
						L"��� �����", s,
						L"��������� ������", ver,
						L"������ ���� CRC32", tohex(_crc),
						L"��������� CRC32", tohex(k));
					delete out;
				}
			}
		}
		else
		{
			if(err) err->AddMessage_(L"�� ������ ���� ��������", msWarning,
				L"��� �����", s,
				L"��������� ������", ver);
		}
	}
*/
	// ���������� ������ ��������� ������������ (��� ����� version)
	if(depotVer >= depotVer5)
	{
		frec = rec + fldv_cversion->offset;
		cv_b[0] = frec[1];
		cv_b[1] = frec[0];
		configVerMajor = cv_s;
		frec += 2;
		cv_b[0] = frec[1];
		cv_b[1] = frec[0];
		configVerMinor = cv_s;
	}
	else
	{
		configVerMinor = 0;
		if(version == ver8_0_3_0 || version == ver8_0_5_0) configVerMajor = 6;
		else if(version == ver8_1_0_0) configVerMajor = 106;
		else configVerMajor = 216;
	}

	delete[] rec;

	// �������������� ������� HISTORY � EXTERNALS
	if(!table_history)
	{
		if(err) err->AddError(L"� ���� ��������� ����������� ������� HISTORY.");
		return false;
	}

	if(!table_externals)
	{
		if(err) err->AddError(L"� ���� ��������� ����������� ������� EXTERNALS.");
		return false;
	}

	fldh_objid = get_field(table_history, L"OBJID");
	if(!fldh_objid) return false;
	fldh_vernum = get_field(table_history, L"VERNUM");
	if(!fldh_vernum) return false;
	fldh_objverid = get_field(table_history, L"OBJVERID");
	if(!fldh_objverid) return false;
	fldh_removed = get_field(table_history, L"REMOVED");
	if(!fldh_removed) return false;
	fldh_datapacked = get_field(table_history, L"DATAPACKED");
	if(!fldh_datapacked) return false;
	fldh_objdata = get_field(table_history, L"OBJDATA");
	if(!fldh_objdata) return false;

	flde_objid = get_field(table_externals, L"OBJID");
	if(!flde_objid) return false;
	flde_vernum = get_field(table_externals, L"VERNUM");
	if(!flde_vernum) return false;
	flde_extname = get_field(table_externals, L"EXTNAME");
	if(!flde_extname) return false;
	flde_extverid = get_field(table_externals, L"EXTVERID");
	if(!flde_extverid) return false;
	flde_datapacked = get_field(table_externals, L"DATAPACKED");
	if(!flde_datapacked) return false;
	flde_extdata = get_field(table_externals, L"EXTDATA");
	if(!flde_extdata) return false;

	if(depotVer >= depotVer6)
	{
		fldh_datahash = get_field(table_history, L"DATAHASH");
		if(!fldh_datahash) return false;
		flde_datahash = get_field(table_externals, L"DATAHASH");
		if(!flde_datahash) return false;

		sp = filename.SubString(1, filename.LastDelimiter(L"\\")) + L"data\\pack\\";
		s = sp + L"pack-*.ind";
		if(FindFirst(s, 0, srec) == 0)
		{
			do
			{
				try
				{
					in = new TFileStream(sp + srec.Name, fmOpenRead | fmShareDenyNone);
				}
				catch(...)
				{
					if(err) err->AddMessage_(L"������ �������� �����", msError,
						L"����", srec.Name);
					FindClose(srec);
					return false;
				}
				in->Seek(8, soFromBeginning);
				in->Read(&i, 4);
				pd.datahashes = new _datahash[i];
				in->Read(pd.datahashes, i * sizeof(_datahash));
				delete in;
				pd.count = i;
				try
				{
					s = sp + srec.Name.SubString(1, srec.Name.Length() - 3) + L"pck";
					pd.pack = new TFileStream(s, fmOpenRead | fmShareDenyNone);
				}
				catch(...)
				{
					if(err) err->AddMessage_(L"������ �������� �����", msError,
						L"����", s);
					FindClose(srec);
					return false;
				}
				packdates.push_back(pd);
			} while(FindNext(srec) == 0);
			FindClose(srec);
		}

		sp = filename.SubString(1, filename.LastDelimiter(L"\\")) + L"data\\objects\\";
	}
	else
	{
		fldh_datahash = NULL;
		flde_datahash = NULL;
	}

	indh = get_index(table_history, L"PK");
	if(!indh) return 0;
	inde = get_index(table_externals, L"PK");
	if(!inde) return 0;

	rech1 = new char[table_history->get_recordlen()];
	rech2 = new char[table_history->get_recordlen()];
	rece = new char[table_externals->get_recordlen()];
	memset(rece, 0, table_externals->get_recordlen());
	nreces = 0;
	reces.Length = 0;

	nh = indh->get_numrecords();
	ne = inde->get_numrecords();
	memset(curobj, 0, 16);

	if(FileExists(__filename)) DeleteFile(__filename);
	cat = new v8catalog(__filename, false);

	// ������� � ���������� ���� version
	t = new tree(L"", nd_list, NULL);
	tc = new tree(L"", nd_list, t);
	tc = new tree(L"", nd_list, tc);
	s = configVerMajor;
	tc->add_child(s, nd_number);
	s = configVerMinor;
	tc->add_child(s, nd_number);
	s = outtext(t);
	delete t;

	in = new TMemoryStream;
	in->Write(TEncoding::UTF8->GetPreamble(), TEncoding::UTF8->GetPreamble().Length);
	sw = new TStreamWriter(in, TEncoding::UTF8, 1024);
	sw->Write(s);
	delete sw;
	out = new TMemoryStream;
	in->Seek(0, soFromBeginning);
	DeflateStream(in, out);
	f = cat->createFile(L"version");
	f->WriteAndClose(out);
	delete in;
	delete out;

	// root, versions
	tv = new tree(L"", nd_list, NULL); // ������ ������ ����� versions
	tvc = new tree(L"", nd_list, tv); // ���. ������� ������ ����� versions
	tr = new tree(L"", nd_list, NULL); // ������ ������ ����� root
	trc = new tree(L"", nd_list, tr); // ���. ������� ������ ����� root

	tvc->add_child(L"1", nd_number);
	tcountv = tvc->add_child(L"0", nd_number); // ����, ���������� ������� � ����� versions
	countv = 0;

	tvc->add_child(L"", nd_string);
	CreateGUID(guid);
	tvc->add_child(GUIDasMS(uid), nd_guid);
	countv++;

	tvc->add_child(L"version", nd_string);
	CreateGUID(guid);
	tvc->add_child(GUIDasMS(uid), nd_guid);
	countv++;

	tvc->add_child(L"versions", nd_string);
	CreateGUID(guid);
	tvc->add_child(GUIDasMS(uid), nd_guid);
	countv++;

	if(configVerMajor < 100)
	{
		trc->add_child(L"1", nd_number);
		trc->add_child(GUIDasMS((unsigned char*)rootobj), nd_guid);
		tcountr = trc->add_child(L"0", nd_number); // ����, ���������� ������� � ����� root
		th = trc;
		cath = cat->CreateCatalog(L"metadata", true);
		unziph = true;
	}
	else
	{
		trc->add_child(L"2", nd_number);
		trc->add_child(GUIDasMS((unsigned char*)rootobj), nd_guid);
		th = tvc;
		tcountr = NULL;
		cath = cat;
		unziph = false;
		tvc->add_child(L"root", nd_string);
		CreateGUID(guid);
		tvc->add_child(GUIDasMS(uid), nd_guid);
		countv++;
	}
	countr = 0;

	lastver = -1;
	lastremoved = true;
	memset(emptyimage, 0, 8);

	in = new TMemoryStream;
	out = new TMemoryStream;
	for(ih = 0, ie = 0; ih <= nh; ih++)
	{
		if(ih < nh)
		{
			i = indh->get_numrec(ih);
			table_history->getrecord(i, rech2);
		}

		if(memcmp(curobj, rech2 + fldh_objid->offset, 16) != 0 || ih == nh)
		{ // ��� ����� ������ ��� ����� �������
			if(!lastremoved)
			{
				ok = false;
				deletesobj = false;
				packlen = 0;
				rec = rech1 + fldh_objdata->offset + 1;
				if(*(rech1 + fldh_objdata->offset) && memcmp(emptyimage, rec, 8))
				{
					table_history->readBlob(in, *(unsigned int*)rec, *(unsigned int*)(rec + 4));
					if(unziph)
					{
						out->Size = 0;
						in->Seek(0, soFromBeginning);
						InflateStream(in, out);
						sobj = out;
					}
					else sobj = in;
					ok = true;
				}
				else if(depotVer >= depotVer6)
				{
					rec = rech1 + fldh_datahash->offset + (fldh_datahash->null_exists ? 1 : 0);
					for(i = 0; i < packdates.size(); i++)
					{
						pdr = &packdates[i];
						for(k = 0; k < pdr->count; k++) if(memcmp(rec, pdr->datahashes[k].datahash, 20) == 0)
						{
							sobj = pdr->pack;
							sobj->Seek(pdr->datahashes[k].offset, soBeginning);
							sobj->Read(&packlen, 8);
							ok = true;
							break;
						}
						if(ok) break;
					}

					if(!ok)
					{
						ss = fldh_datahash->get_presentation(rech1, true);
						s = sp + ss.SubString(1, 2) + L'\\' + ss.SubString(3, ss.Length() - 2);
						if(FileExists(s))
						{
							try
							{
								sobj = new TFileStream(s, fmOpenRead | fmShareDenyNone);
								deletesobj = true;
								ok = true;
							}
							catch(...)
							{
								if(err) err->AddMessage_(L"������ �������� �����", msError,
									L"����", s,
									L"�������", L"HISTORY",
									L"������", fldh_objid->get_presentation(rech1, false, L'.', true),
									L"������", fldh_vernum->get_presentation(rech1, false));
							}
						}
						else
						{
							if(err) err->AddMessage_(L"�� ������ ����", msError,
								L"����", s,
								L"�������", L"HISTORY",
								L"������", fldh_objid->get_presentation(rech1, false, L'.', true),
								L"������", fldh_vernum->get_presentation(rech1, false));
						}
					}
				}
				s = fldh_objid->get_presentation(rech1, false, L'.', true);
				if(!ok)
				{
					if(err) err->AddMessage_(L"������ ������ ������� ������������", msError,
						L"�������", L"HISTORY",
						L"������", s,
						L"������", fldh_vernum->get_presentation(rech1, false));
				}
				else
				{
					f = cath->createFile(s);
					if(packlen) f->WriteAndClose(sobj, packlen);
					else f->WriteAndClose(sobj);
					if(deletesobj) delete sobj;
					th->add_child(s, nd_string);
					th->add_child(GUIDasMS((unsigned char*)rech1 + fldh_objverid->offset), nd_guid);
					countr++;

					// ��� ��� ���� �� EXTERNALS
					while(true)
					{
						if(ie > ne) break;
						res = memcmp(rece + flde_objid->offset, curobj, 16);
						if(res > 0) break;
						if(!res)
						{
							s = flde_vernum->get_presentation(rece, false);
							v = s.ToIntDef(MaxInt);
							s = flde_extname->get_presentation(rece);
							if(v <= ver) if(*(rece + flde_datapacked->offset))
							{
								for(j = 0; j < nreces; j++) if(s.CompareIC(flde_extname->get_presentation(reces[j])) == 0) break;
								if(j == reces.Length){
									reces.Length++;
									reces[j] = new char[table_externals->get_recordlen()];
								}
								if(j == nreces) nreces++;
								memcpy(reces[j], rece, table_externals->get_recordlen());
							}
							if(v == lastver)
							{
								extnames.Length++;
								extnames[extnames.Length - 1] = s;
							}
						}
						if(ie == ne)
						{
							ie++;
							break;
						}
						i = inde->get_numrec(ie++);
						table_externals->getrecord(i, rece);
					}
					for(j = 0; j < nreces; j++)
					{
						rec = reces[j];
						sn = flde_extname->get_presentation(rec);
						ok = false;
						for(n = 0; n < extnames.Length; n++ ) if(sn.CompareIC(extnames[n]) == 0)
						{
							ok = true;
							break;
						}
						if(!ok) continue;

						ok = false;
						packlen = 0;
						deletesobj = false;
						frec = rec + flde_extdata->offset;
						if(memcmp(emptyimage, frec, 8))
						{
							table_externals->readBlob(in, *(unsigned int*)frec, *(unsigned int*)(frec + 4));
							sobj = in;
							ok = true;
						}
						else if(depotVer >= depotVer6)
						{
							frec = rec + flde_datahash->offset + (flde_datahash->null_exists ? 1 : 0);
							for(i = 0; i < packdates.size(); i++)
							{
								pdr = &packdates[i];
								for(k = 0; k < pdr->count; k++) if(memcmp(frec, pdr->datahashes[k].datahash, 20) == 0)
								{
									sobj = pdr->pack;
									sobj->Seek(pdr->datahashes[k].offset, soBeginning);
									sobj->Read(&packlen, 8);
									ok = true;
									break;
								}
								if(ok) break;
							}

							if(!ok)
							{
								ss = flde_datahash->get_presentation(rec, true);
								s = sp + ss.SubString(1, 2) + L'\\' + ss.SubString(3, ss.Length() - 2);
								if(FileExists(s))
								{
									try
									{
										sobj = new TFileStream(s, fmOpenRead | fmShareDenyNone);
										deletesobj = true;
										ok = true;
									}
									catch(...)
									{
										if(err) err->AddMessage_(L"������ �������� �����", msError,
											L"����", s,
											L"�������", L"EXTERNALS",
											L"������", flde_extname->get_presentation(rec),
											L"������", flde_vernum->get_presentation(rec));
									}
								}
								else
								{
									if(err) err->AddMessage_(L"�� ������ ����", msError,
										L"����", s,
										L"�������", L"EXTERNALS",
										L"������", flde_extname->get_presentation(rec),
										L"������", flde_vernum->get_presentation(rec));
								}
							}
						}
						if(!ok)
						{
							if(err) err->AddMessage_(L"������ ������ ������� ������������", msError,
								L"�������", L"EXTERNALS",
								L"������", sn,
								L"������", flde_vernum->get_presentation(rec));
						}
						else
						{
							f = cath->createFile(sn);
							if(packlen) f->WriteAndClose(sobj, packlen);
							else f->WriteAndClose(sobj);
							if(deletesobj) delete sobj;
							tvc->add_child(sn, nd_string);
							tvc->add_child(GUIDasMS((unsigned char*)rec + flde_extverid->offset), nd_guid);
							countv++;
						}

					}
					nreces = 0;
					extnames.Length = 0;
				}
			}

			memcpy(curobj, rech2 + fldh_objid->offset, 16);
			lastremoved = true;
		}

		if(ih < nh)
		{
			s = fldh_vernum->get_presentation(rech2, false);
			v = s.ToIntDef(MaxInt);
			if(v <= ver)
			{
				removed = *(rech2 + fldh_removed->offset);
				if(removed)
				{
					lastremoved = true;
				}
				else
				{
					datapacked = false;
					if(*(rech2 + fldh_datapacked->offset)) if(*(rech2 + fldh_datapacked->offset + 1)) datapacked = true;
					if(datapacked)
					{
						memcpy(rech1, rech2, table_history->get_recordlen());
						lastremoved = false;
						lastver = v;
					}
				}
			}
		}
	}

	delete[] rech1;
	delete[] rech2;
	delete[] rece;
	for(j = 0; j < nreces; j++) delete[] reces[j];

	if(unziph)
	{
		tcountv->set_value(countv, nd_number);
		tcountr->set_value(countr, nd_number);
	}
	else tcountv->set_value(countv + countr, nd_number);

	// ������ root
	s = outtext(tr);
	delete tr;
	in->Size = 0;
	in->Write(TEncoding::UTF8->GetPreamble(), TEncoding::UTF8->GetPreamble().Length);
	sw = new TStreamWriter(in, TEncoding::UTF8, 1024);
	sw->Write(s);
	delete sw;
	in->Seek(0, soFromBeginning);
	f = cath->createFile(L"root");
	if(unziph)
	{
		f->WriteAndClose(in);
	}
	else
	{
		out->Size = 0;
		DeflateStream(in, out);
		f->WriteAndClose(out);
	}

	// ������ versions
	s = outtext(tv);
	delete tv;
	in->Size = 0;
	in->Write(TEncoding::UTF8->GetPreamble(), TEncoding::UTF8->GetPreamble().Length);
	sw = new TStreamWriter(in, TEncoding::UTF8, 1024);
	sw->Write(s);
	delete sw;
	out->Size = 0;
	in->Seek(0, soFromBeginning);
	DeflateStream(in, out);
	f = cat->createFile(L"versions");
	f->WriteAndClose(out);

	delete in;
	delete out;

	for(i = 0; i < packdates.size(); i++)
	{
		delete packdates[i].pack;
		delete[] packdates[i].datahashes;
	}

	delete cat;
	return true;
}

//---------------------------------------------------------------------------
// ���������� ������ ������������ � ������� �� ��������� ������������
// ver_begin - ��������� ����� ��������� ������ ����������� ������ ������������
// ver_end - �������� ����� ��������� ������ ����������� ������ ������������
// ver_begin > 0, ver_end > 0 - ������������ ���������� ����� ������
// ver_begin <= 0, ver_end <= 0 - ����� ������ �� ��������� ������������. 0 - ��������� ������������, -1 - ������������� � �.�., �.�. ����� ������ ������������ ��� ����� ��������� + ver
bool __fastcall T_1CD::save_part_depot_config(const String& _filename, int ver_begin, int ver_end)
{
	char* rec;
	char* frec;
	field* fldd_depotver;
	//field* fldd_rootobjid;

	field* fldv_vernum;
	field* fldv_cversion;
	field* fldv_snapshotcrc;
	field* fldv_snapshotmaker;

	field* fldh_objid;
	field* fldh_vernum;
	field* fldh_objverid;
	field* fldh_removed;
	field* fldh_datapacked;
	field* fldh_objdata;
	field* fldh_datahash;
	index* indh;
	char* rech; // ������� ������ HISTORY
	char* rech1; // ������ � ������� < ver_begin
	bool hasrech1;
	char* rech2; // ������ � ������� <= ver_end
	bool hasrech2;

	char rootobj[16];
	char curobj[16];
	unsigned int ih, nh;

	field* flde_objid;
	field* flde_vernum;
	field* flde_extname;
	field* flde_extverid;
	field* flde_datapacked;
	field* flde_extdata;
	field* flde_datahash;
	index* inde;
	char* rece;
	unsigned int ie, ne, je;

	bool ok;
	bool removed;
	bool datapacked;
	bool deletesobj;
	bool iscatalog;
	bool changed;
	bool inreaded;
	bool hasext;
	char emptyimage[8];
	char verid[16];
	unsigned int i, k, _crc;
	int v, j, res, lastver, n;
	String s, ss, sp, sn, se;
	depot_ver depotVer;
	unsigned int configVerMajor, configVerMinor;
	//char VerDate[7];
	TMemoryStream* in;
	TMemoryStream* out;
	TStream* sobj;
	TStreamWriter* sw;
	TMemoryStream* sd;
	bool hasdeleted;
	std::vector<_packdata> packdates;
	TSearchRec srec;
	_packdata pd;
	_packdata* pdr;
	__int64 packlen;
	v8catalog* cat;
	String cath;
	TFileStream* f;
	String __filename;

	union
	{
		GUID guid;
		unsigned char uid[16];
	};

	union
	{
		char cv_b[2];
		unsigned short cv_s;
	};


	if(!is_open()) return false;

	if(!is_depot)
	{
		if(err) err->AddError(L"���� �� �������� ���������� ������������.");
		return false;
	}

	// �������� ������ ���������
	if(!table_depot)
	{
		if(err) err->AddError(L"� ���� ��������� ����������� ������� DEPOT.");
		return false;
	}

	fldd_depotver = get_field(table_depot, L"DEPOTVER");
	if(!fldd_depotver) return false;

	rec = new char[table_depot->get_recordlen()];
	ok = false;
	for(i = 0; i < table_depot->get_phys_numrecords(); i++)
	{
		table_depot->getrecord(i, rec);
		if(!*rec)
		{
			ok = true;
			break;
		}
	}
	if(!ok)
	{
		if(err) err->AddError(L"�� ������� ��������� ������ � ������� DEPOT.");
		delete[] rec;
		return false;
	}

	s = fldd_depotver->get_presentation(rec, true);

	if(s.CompareIC(L"0300000000000000") == 0) depotVer = depotVer3;
	else if(s.CompareIC(L"0500000000000000") == 0) depotVer = depotVer5;
	else if(s.CompareIC(L"0600000000000000") == 0) depotVer = depotVer6;
	else
	{
		err->AddMessage_(L"����������� ������ ���������", msError,
			L"������ ���������", s);
		delete[] rec;
		return false;
	}

	delete[] rec;

	// "�����������" ������ ������������
	ver_begin = get_ver_depot_config(ver_begin);
	ver_end = get_ver_depot_config(ver_end);
	if(ver_end < ver_begin)
	{
		v = ver_begin;
		ver_begin = ver_end;
		ver_end = v;
	}

	// ���� ������ � ������� ������ ������������
	if(!table_versions)
	{
		if(err) err->AddError(L"� ���� ��������� ����������� ������� VERSIONS.");
		return false;
	}

	fldv_vernum = get_field(table_versions, L"VERNUM");
	if(!fldv_vernum) return false;
	if(depotVer >= depotVer5)
	{
		fldv_cversion = get_field(table_versions, L"CVERSION");
		if(!fldv_cversion) return false;
	}
	fldv_snapshotcrc = get_field(table_versions, L"SNAPSHOTCRC");
	if(!fldv_snapshotcrc) return false;
	fldv_snapshotmaker = get_field(table_versions, L"SNAPSHOTMAKER");
	if(!fldv_snapshotmaker) return false;

	rec = new char[table_versions->get_recordlen()];
	n = 0;
	for(i = 0; i < table_versions->get_phys_numrecords(); i++)
	{
		table_versions->getrecord(i, rec);
		if(*rec) continue;
		s = fldv_vernum->get_presentation(rec, true);
		v = s.ToIntDef(0);
		if(v == ver_begin) n++;
		if(v == ver_end) n++;
		if(n >= 2) break;
	}

	if(n < 2)
	{
		if(err) err->AddMessage_(L"� ��������� �� ������� ����������� ������ ������������", msError
			, L"������ �", ver_begin
			, L"������ ��", ver_end);
		delete[] rec;
		return false;
	}

	__filename = System::Ioutils::TPath::GetFullPath(_filename);

	// ���������� ������ ��������� ������������ (��� ����� version)
	if(depotVer >= depotVer5)
	{
		frec = rec + fldv_cversion->offset;
		cv_b[0] = frec[1];
		cv_b[1] = frec[0];
		configVerMajor = cv_s;
		frec += 2;
		cv_b[0] = frec[1];
		cv_b[1] = frec[0];
		configVerMinor = cv_s;
	}
	else
	{
		configVerMinor = 0;
		if(version == ver8_0_3_0 || version == ver8_0_5_0) configVerMajor = 6;
		else if(version == ver8_1_0_0) configVerMajor = 106;
		else configVerMajor = 216;
	}

	delete[] rec;

	// �������������� ������� HISTORY � EXTERNALS
	if(!table_history)
	{
		if(err) err->AddError(L"� ���� ��������� ����������� ������� HISTORY.");
		return false;
	}

	if(!table_externals)
	{
		if(err) err->AddError(L"� ���� ��������� ����������� ������� EXTERNALS.");
		return false;
	}

	fldh_objid = get_field(table_history, L"OBJID");
	if(!fldh_objid) return false;
	fldh_vernum = get_field(table_history, L"VERNUM");
	if(!fldh_vernum) return false;
	fldh_objverid = get_field(table_history, L"OBJVERID");
	if(!fldh_objverid) return false;
	fldh_removed = get_field(table_history, L"REMOVED");
	if(!fldh_removed) return false;
	fldh_datapacked = get_field(table_history, L"DATAPACKED");
	if(!fldh_datapacked) return false;
	fldh_objdata = get_field(table_history, L"OBJDATA");
	if(!fldh_objdata) return false;

	flde_objid = get_field(table_externals, L"OBJID");
	if(!flde_objid) return false;
	flde_vernum = get_field(table_externals, L"VERNUM");
	if(!flde_vernum) return false;
	flde_extname = get_field(table_externals, L"EXTNAME");
	if(!flde_extname) return false;
	flde_extverid = get_field(table_externals, L"EXTVERID");
	if(!flde_extverid) return false;
	flde_datapacked = get_field(table_externals, L"DATAPACKED");
	if(!flde_datapacked) return false;
	flde_extdata = get_field(table_externals, L"EXTDATA");
	if(!flde_extdata) return false;

	if(depotVer >= depotVer6)
	{
		fldh_datahash = get_field(table_history, L"DATAHASH");
		if(!fldh_datahash) return false;
		flde_datahash = get_field(table_externals, L"DATAHASH");
		if(!flde_datahash) return false;

		sp = filename.SubString(1, filename.LastDelimiter(L"\\")) + L"data\\pack\\";
		s = sp + L"pack-*.ind";
		if(FindFirst(s, 0, srec) == 0)
		{
			do
			{
				try
				{
					f = new TFileStream(sp + srec.Name, fmOpenRead | fmShareDenyNone);
				}
				catch(...)
				{
					if(err) err->AddMessage_(L"������ �������� �����", msError,
						L"����", srec.Name);
					FindClose(srec);
					return false;
				}
				f->Seek(8, soFromBeginning);
				f->Read(&i, 4);
				pd.datahashes = new _datahash[i];
				f->Read(pd.datahashes, i * sizeof(_datahash));
				delete f;
				pd.count = i;
				try
				{
					s = sp + srec.Name.SubString(1, srec.Name.Length() - 3) + L"pck";
					pd.pack = new TFileStream(s, fmOpenRead | fmShareDenyNone);
				}
				catch(...)
				{
					if(err) err->AddMessage_(L"������ �������� �����", msError,
						L"����", s);
					FindClose(srec);
					return false;
				}
				packdates.push_back(pd);
			} while(FindNext(srec) == 0);
			FindClose(srec);
		}

		sp = filename.SubString(1, filename.LastDelimiter(L"\\")) + L"data\\objects\\";
	}
	else
	{
		fldh_datahash = NULL;
		flde_datahash = NULL;
	}

	indh = get_index(table_history, L"PK");
	if(!indh) return 0;
	inde = get_index(table_externals, L"PK");
	if(!inde) return 0;

	rech = new char[table_history->get_recordlen()];
	rech1 = new char[table_history->get_recordlen()];
	rech2 = new char[table_history->get_recordlen()];
	rece = new char[table_externals->get_recordlen()];
	memset(rece, 0, table_externals->get_recordlen());

	nh = indh->get_numrecords();
	ne = inde->get_numrecords();
	memset(curobj, 0, 16);


	if(configVerMajor < 100)
	{
		cath = __filename + L"\\metadata\\";
	}
	else
	{
		cath = __filename + L"\\";
	}

	lastver = -1;
	memset(emptyimage, 0, 8);

	sd = new TMemoryStream;
	sw = new TStreamWriter(sd, TEncoding::UTF8, 4096);
	//sd->Write(TEncoding::UTF8->GetPreamble(), TEncoding::UTF8->GetPreamble().Length);
	hasdeleted = false;

	in = new TMemoryStream;
	out = new TMemoryStream;
	cat = NULL;
	hasrech1 = false;
	hasrech2 = false;
	for(ih = 0, ie = 0; ih <= nh; ih++)
	{
		if(ih < nh)
		{
			i = indh->get_numrec(ih);
			table_history->getrecord(i, rech);
		}

		if(memcmp(curobj, rech + fldh_objid->offset, 16) != 0 || ih == nh)
		{ // ��� ����� ������ ��� ����� �������
			if(ih) if(hasrech2)
			{
				s = fldh_vernum->get_presentation(rech2, false);
				lastver = s.ToIntDef(MaxInt);
				sn = fldh_objid->get_presentation(rech2, false, L'.', true);

				hasext = true;
				removed = *(rech2 + fldh_removed->offset);
				if(removed)
				{
					if(hasrech1)
					{
						sw->Write(sn + L"\r\n");
						hasdeleted = true;
					}
					else hasext = false;
				}
				else
				{
					datapacked = false;
					if(*(rech2 + fldh_datapacked->offset)) if(*(rech2 + fldh_datapacked->offset + 1)) datapacked = true;

					if(datapacked)
					{
						changed = true;
						inreaded = false;
						if(hasrech1)
						{
							datapacked = false;
							if(*(rech1 + fldh_datapacked->offset)) if(*(rech1 + fldh_datapacked->offset + 1)) datapacked = true;
							if(datapacked)
							{
								rec = rech1 + fldh_objdata->offset + 1;
								if(*(rech1 + fldh_objdata->offset) && memcmp(emptyimage, rec, 8))
								{
									table_history->readBlob(out, *(unsigned int*)rec, *(unsigned int*)(rec + 4));
									rec = rech2 + fldh_objdata->offset + 1;
									if(*(rech2 + fldh_objdata->offset) && memcmp(emptyimage, rec, 8))
									{
										table_history->readBlob(in, *(unsigned int*)rec, *(unsigned int*)(rec + 4));
										inreaded = true;
										if(in->Size == out->Size) if(memcmp(in->Memory, out->Memory, in->Size) == 0) changed = false;
									}
								}
								else if(depotVer >= depotVer6)
								{
									if(memcmp(rech2 + fldh_datahash->offset + (fldh_datahash->null_exists ? 1 : 0)
											, rech1 + fldh_datahash->offset + (fldh_datahash->null_exists ? 1 : 0)
											, fldh_datahash->length) == 0) changed = false;
								}
							}
						}

						if(changed)
						{
							ok = false;
							deletesobj = false;
							packlen = 0;
							rec = rech2 + fldh_objdata->offset + 1;
							if(inreaded)
							{
								sobj = in;
								ok = true;
							}
							else if(*(rech2 + fldh_objdata->offset) && memcmp(emptyimage, rec, 8))
							{
								table_history->readBlob(in, *(unsigned int*)rec, *(unsigned int*)(rec + 4));
								sobj = in;
								ok = true;
							}
							else if(depotVer >= depotVer6)
							{
								rec = rech2 + fldh_datahash->offset + (fldh_datahash->null_exists ? 1 : 0);
								for(i = 0; i < packdates.size(); i++)
								{
									pdr = &packdates[i];
									for(k = 0; k < pdr->count; k++) if(memcmp(rec, pdr->datahashes[k].datahash, 20) == 0)
									{
										sobj = pdr->pack;
										sobj->Seek(pdr->datahashes[k].offset, soBeginning);
										sobj->Read(&packlen, 8);
										in->Size = 0;
										in->CopyFrom(sobj, packlen);
										sobj = in;
										ok = true;
										break;
									}
									if(ok) break;
								}

								if(!ok)
								{
									ss = fldh_datahash->get_presentation(rech2, true);
									s = sp + ss.SubString(1, 2) + L'\\' + ss.SubString(3, ss.Length() - 2);
									if(FileExists(s))
									{
										try
										{
											sobj = new TFileStream(s, fmOpenRead | fmShareDenyNone);
											deletesobj = true;
											ok = true;
										}
										catch(...)
										{
											if(err) err->AddMessage_(L"������ �������� �����", msError,
												L"����", s,
												L"�������", L"HISTORY",
												L"������", sn,
												L"������", lastver);
										}
									}
									else
									{
										if(err) err->AddMessage_(L"�� ������ ����", msError,
											L"����", s,
											L"�������", L"HISTORY",
											L"������", sn,
											L"������", lastver);
									}
								}
							}

							if(!ok)
							{
								if(err) err->AddMessage_(L"������ ������ ������� ������������", msError,
									L"�������", L"HISTORY",
									L"������", sn,
									L"������", lastver);
							}
							else
							{
								f = new TFileStream(cath + sn, fmCreate);
								sobj->Seek(0, soFromBeginning);
								InflateStream(sobj, f);
								delete f;
								if(deletesobj) delete sobj;

							}
						}
					}
				}

				// ��� ��� ���� �� EXTERNALS
				while(hasext)
				{
					if(ie > ne) break;
					res = memcmp(rece + flde_objid->offset, curobj, 16);
					if(res > 0) break;
					if(!res)
					{
						s = flde_vernum->get_presentation(rece, false);
						v = s.ToIntDef(MaxInt);
						if(v == lastver)
						{
							se = flde_extname->get_presentation(rece);
							if(removed)
							{
								sw->Write(se + L"\r\n");
								hasdeleted = true;
							}
							else
							{
								datapacked = *(rece + flde_datapacked->offset);

								// ==> ����� ������ � �����
								// � ������ ���������� ������ (datapacked = false) � ���� ������ �������� ����� ���������� ������ � �������
								// (� ��� �� objid, extname � extverid), �� � �������� ver_begin <= vernum < lastver
								memcpy(verid, rece + flde_extverid->offset, 16);
								je = ie;
								while(!datapacked && v > ver_begin && je)
								{
									i = inde->get_numrec(--je);
									table_externals->getrecord(i, rece);
									res = memcmp(rece + flde_objid->offset, curobj, 16);
									if(res) break;
									s = flde_extname->get_presentation(rece);
									if(s.CompareIC(se)) continue;
									if(memcmp(verid, rece + flde_extverid->offset, 16) == 0)
									{
										s = flde_vernum->get_presentation(rece, false);
										v = s.ToIntDef(MaxInt);
										if(v < ver_begin) break;
										datapacked = *(rece + flde_datapacked->offset);
									}
								}
								// <== ����� ������ � �����

								if(datapacked)
								{
									ok = false;
									packlen = 0;
									deletesobj = false;
									frec = rece + flde_extdata->offset;
									if(memcmp(emptyimage, frec, 8))
									{
										table_externals->readBlob(in, *(unsigned int*)frec, *(unsigned int*)(frec + 4));
										sobj = in;
										ok = true;
									}
									else if(depotVer >= depotVer6)
									{
										frec = rece + flde_datahash->offset + (flde_datahash->null_exists ? 1 : 0);
										for(i = 0; i < packdates.size(); i++)
										{
											pdr = &packdates[i];
											for(k = 0; k < pdr->count; k++) if(memcmp(frec, pdr->datahashes[k].datahash, 20) == 0)
											{
												sobj = pdr->pack;
												sobj->Seek(pdr->datahashes[k].offset, soBeginning);
												sobj->Read(&packlen, 8);
												in->Size = 0;
												in->CopyFrom(sobj, packlen);
												sobj = in;
												ok = true;
												break;
											}
											if(ok) break;
										}

										if(!ok)
										{
											ss = flde_datahash->get_presentation(rece, true);
											s = sp + ss.SubString(1, 2) + L'\\' + ss.SubString(3, ss.Length() - 2);
											if(FileExists(s))
											{
												try
												{
													sobj = new TFileStream(s, fmOpenRead | fmShareDenyNone);
													deletesobj = true;
													ok = true;
												}
												catch(...)
												{
													if(err) err->AddMessage_(L"������ �������� �����", msError,
														L"����", s,
														L"�������", L"EXTERNALS",
														L"������", sn,
														L"���� ������������", se,
														L"������", v);
												}
											}
											else
											{
												if(err) err->AddMessage_(L"�� ������ ����", msError,
													L"����", s,
													L"�������", L"EXTERNALS",
													L"������", sn,
													L"���� ������������", se,
													L"������", v);
											}
										}
									}
									if(!ok)
									{
										if(err) err->AddMessage_(L"������ ������ ������� ������������", msError,
											L"�������", L"EXTERNALS",
											L"������", sn,
											L"���� ������������", se,
											L"������", v);
									}
									else
									{
										out->Size = 0;
										sobj->Seek(0, soFromBeginning);
										InflateStream(sobj, out);
										iscatalog = false;
										if(out->Size > 0)
										{
											cat = new v8catalog(out, false, true);
											iscatalog = cat->IsCatalog();
										}
										if(iscatalog) cat->SaveToDir(cath + se);
										else
										{
											f = new TFileStream(cath + se, fmCreate);
											//f->Write(out);
											f->CopyFrom(out, 0);
											delete f;
										}
										delete cat;
										cat = NULL;
										if(deletesobj) delete sobj;

									}

								}

							}
						}

					}
					if(ie == ne)
					{
						ie++;
						break;
					}
					i = inde->get_numrec(ie++);
					table_externals->getrecord(i, rece);
				}
			}

			memcpy(curobj, rech + fldh_objid->offset, 16);
			hasrech1 = false;
			hasrech2 = false;
		}

		if(ih < nh)
		{
			s = fldh_vernum->get_presentation(rech, false);
			v = s.ToIntDef(MaxInt);
			if(v < ver_begin)
			{
				memcpy(rech1, rech, table_history->get_recordlen());
				hasrech1 = true;
			}
			else if(v <= ver_end)
			{
				memcpy(rech2, rech, table_history->get_recordlen());
				hasrech2 = true;
			}
		}
	}

	delete sw;
	if(hasdeleted)
	{
		f = new TFileStream(__filename + L"\\deleted", fmCreate);
		f->CopyFrom(sd, 0);
		delete f;
	}
	delete sd;

	delete[] rech;
	delete[] rech1;
	delete[] rech2;
	delete[] rece;

	for(i = 0; i < packdates.size(); i++)
	{
		delete packdates[i].pack;
		delete[] packdates[i].datahashes;
	}



	return true;
}


#ifndef PublicRelease
//---------------------------------------------------------------------------
// �������� � �������������� ������� ���������� ����� DATA ���������� �������
// �������� ������� ���������� �� ��������� �������, ���������� �� �������� �����
// ����������� �������� ����� DATA, ���� �������� �� ��������, ������������ ����� ���������� �������� � �����.
void __fastcall T_1CD::restore_DATA_allocation_table(table* tab)
{
	char* rectt;
	unsigned int block;
	v8ob* rootobj;
	objtab* ca;
	char* cd;
	unsigned int i, l, cl, a, d, r;
	int j, k, m, n, rl;
	bool ok;
	std::vector<unsigned int> bk;
	String s;

	block = tab->get_file_data()->get_block_number();

	if(block < 5 || block >= length)
	{
		if(err) err->AddMessage_(L"����� ��������� ����� ����� DATA ������������", msError
			,L"�������", tab->getname()
			,L"����� �����", block
		);
		return;
	}

	rootobj = (v8ob*)getblock_for_write(block, true);

	if(memcmp(rootobj->sig, SIG_OBJ, 8))
	{
		//memcpy(rootobj->sig, SIG_OBJ, 8);
		//if(err) err->AddMessage_(L"��������� ��������� ����� ����� DATA ������������. �������� ����� ���������.", msError
		if(err) err->AddMessage_(L"��������� ��������� ����� ����� DATA ������������.", msError
			,L"�������", tab->getname()
			,L"����� ����� (dec)", block
			,L"����� ����� (hex)", tohex(block)
		);
		return;
	}

	l = rootobj->len;
	rl = tab->get_recordlen();
	if(l / rl * rl != l)
	{
		if(err) err->AddMessage_(L"����� ����� DATA �� ������ ����� ����� ������.", msError
			,L"�������", tab->getname()
			,L"����� ����� (dec)", block
			,L"����� ����� (hex)", tohex(block)
			,L"����� �����", l
			,L"����� ������", tab->get_recordlen()
		);
		return;
	}

	rectt = tab->get_record_template_test();

	for(i = 0, j = 0, k = 0; l; ++i)
	{
		cl = l > 0x1000 ? 0x1000 : l;

		if(!j)
		{
			a = rootobj->blocks[k];
			if(a < 5 || a >= length)
			{
				if(err) err->AddMessage_(L"������������ ����� ����� ������� ���������� ����� DATA. ������� ����� �������� ����������", msWarning
					,L"�������", tab->getname()
					,L"������ ��������", k
					,L"����� �����", a
				);
				a = length;
				ca = (objtab*)getblock_for_write(a, false);
			}
			else ca = (objtab*)getblock_for_write(a, true);

			n = ((unsigned __int64)l + 0xfff) >> 12;
			if(n > 1023) n = 1023;
			m = ca->numblocks;

			if(n != m)
			{
				if(err) err->AddMessage_(L"������������ ����� ������ �� �������� ���������� ����� DATA. ����������.", msWarning
					,L"�������", tab->getname()
					,L"����� �����", a
					,L"������ ��������", k
					,L"�������� ���������� ������", m
					,L"������ ���������� ������", n
				);
				ca->numblocks = n;
			}

			++k;
		}

		d = ca->blocks[j];
		ok = true;
		if(d < 5 || d >= length)
		{
			if(err) err->AddMessage_(L"������������ ����� �������� ������ ����� DATA.", msWarning
				,L"�������", tab->getname()
				,L"����� �����", a
				,L"������ �������� ����������", k - 1
				,L"������ ����� �� ��������", j
				,L"�������� ����� ��������", d
			);
			ok = false;
		}
		if(ok)
		{
			ok = test_block_by_template(d, rectt, i, rl, cl);
			if(!ok)
			{
			if(err) err->AddMessage_(L"C������� ������ ����� DATA �� �������� �� �������.", msWarning
				,L"�������", tab->getname()
				,L"����� �����", d
				,L"������ �������� ����������", k - 1
				,L"������ ����� �� ��������", j
				,L"������ �������� � ����� DATA", i
			);
			}
		}

		if(!ok)
		{
			bk.clear();
			for(r = 5; r < length; ++r)
			{
				if(test_block_by_template(r, rectt, i, rl, cl)) bk.push_back(r);
			}
			if(bk.size() == 0)
			{
				if(err) err->AddMessage_(L"�� ������� ����� ���������� �������� ������ ����� DATA �� �������.", msError
					,L"�������", tab->getname()
					,L"������ �������� ����������", k - 1
					,L"������ ����� �� ��������", j
					,L"������ �������� � ����� DATA", i
				);
			}
			else if(bk.size() == 1)
			{
				d = bk[0];
				ca->blocks[j] = d;
				if(err) err->AddMessage_(L"������� ���������� �������� ������ ����� DATA. �������� �������������", msInfo
					,L"�������", tab->getname()
					,L"����� �����", d
					,L"������ �������� ����������", k - 1
					,L"������ ����� �� ��������", j
					,L"������ �������� � ����� DATA", i
				);

			}
			else
			{
				s = L"";
				for(d = 0; d < bk.size(); ++d)
				{
					if(d > 0) s += L", ";
					s += tohex(bk[d]);
				}
				if(err) err->AddMessage_(L"������� ��������� ���������� ������� ������ ����� DATA.", msHint
					,L"�������", tab->getname()
					,L"������ ���������� ������", s
					,L"������ �������� ����������", k - 1
					,L"������ ����� �� ��������", j
					,L"������ �������� � ����� DATA", i
				);
			}
		}

		if(++j >= 1023)
		{
			j = 0;
			++a;
		}
		if(l < 0x1000) l = 0;
		else l-=0x1000;
	}


	delete[] rectt;

	flush();
}

//---------------------------------------------------------------------------
// �������� ����� ������� �� �������
bool __fastcall T_1CD::test_block_by_template(unsigned int testblock, char* tt, unsigned int num, int rlen, int len)
{
	unsigned char b[0x1000];
	bool ok;
	int i, j;

	ok = true;
	fs->Seek((__int64)testblock << 12, (TSeekOrigin)soFromBeginning);
	fs->ReadBuffer(b, len);

	if(!num)
	{
		if(b[0] != 1) return false;
		for(i = 1; i < 5; ++i) if(b[i] != 0) return false;
		//for(i = 5; i < rlen; ++i) if(b[i] != 0 && b[i] != 0x10 && b[i] != 0x20) return false;
		j = 0;
		i = rlen;
	}
	else
	{
		i = (num << 12) / rlen;
		j = (num << 12) - rlen * i;
		i = 0;
	}
	for(; i < len; ++i)
	{
		if(tt[(j << 8) + b[i]] == 0) return false;
		if(++j >= rlen) j = 0;
	}
	for(i = len; i < 0x1000; ++i) if(b[i]) return false;
	return true;
}

#endif //#ifdef PublicRelease

//---------------------------------------------------------------------------
TableFiles* T_1CD::get_files_config()
{
	if(!_files_config) _files_config = new TableFiles(table_config);
	return _files_config;
}

//---------------------------------------------------------------------------
TableFiles* T_1CD::get_files_configsave()
{
	if(!_files_configsave) _files_configsave = new TableFiles(table_configsave);
	return _files_configsave;
}

//---------------------------------------------------------------------------
TableFiles* T_1CD::get_files_params()
{
	if(!_files_params) _files_params = new TableFiles(table_params);
	return _files_params;
}

//---------------------------------------------------------------------------
TableFiles* T_1CD::get_files_files()
{
	if(!_files_files) _files_files = new TableFiles(table_files);
	return _files_files;
}

//---------------------------------------------------------------------------
TableFiles* T_1CD::get_files_configcas()
{
	if(!_files_configcas) _files_configcas = new TableFiles(table_configcas);
	return _files_configcas;
}

//---------------------------------------------------------------------------
TableFiles* T_1CD::get_files_configcassave()
{
	if(!_files_configcassave) _files_configcassave = new TableFiles(table_configcassave);
	return _files_configcassave;
}

//---------------------------------------------------------------------------
bool __fastcall T_1CD::save_config_ext(const String& _filename, const TGUID& uid, const String& hashname)
{
	ConfigStorageTableConfigCasSave* cs;
	bool res;

	cs = new ConfigStorageTableConfigCasSave(files_configcas, files_configcassave, uid, hashname);
	if(!cs->getready()) res = false;
	else res = cs->save_config(_filename);
	delete cs;
	return res;
}

//---------------------------------------------------------------------------
bool __fastcall T_1CD::save_config_ext_db(const String& _filename, const String& hashname)
{
	ConfigStorageTableConfigCas* cs;
	bool res;

	cs = new ConfigStorageTableConfigCas(files_configcas, hashname);
	if(!cs->getready()) res = false;
	else res = cs->save_config(_filename);
	delete cs;
	return res;

}


//---------------------------------------------------------------------------

